<?php
/**
 * Set image dimensions
 */
return [
    'cxs' => [
        'width' => 100,
        'height' => 100
    ],
    'xs' => [
        'width' => 200,
        'height' => 200
    ],
    'sm' => [
        'width' => 400,
        'height' => 400
    ],
    'md' => [
        'width' => 800,
        'height' => 800
    ],
    'lg' => [
        'width' => 1200,
        'height' => 1200
    ],
    'default' => [
        'width' => 2000,
        'height' => 2000
    ],
    'autocomplete' => [
        'width' => 36,
        'height' => 36
    ],
    'fe-account' => [
        'width' => 80,
        'height' => 35
    ],
    'fe-article' => [
        'width' => 780,
        'height' => 210
    ],
    'facebook' => [
        'width' => 600,
        'height' => 315
    ],
    'twitter' => [
        'width' => 435,
        'height' => 375
    ],
    // shopping cart
    'shopping-cart' => [
        'width' => 100,
        'height' => 100,
        'quality' => 92
    ],
    'ret-shopping-cart' => [
        'width' => 200,
        'height' => 200,
        'quality' => 92
    ],
    // project formats
];
