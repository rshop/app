<?php
/**
 * Use to override RSHOP configuration
 */
return [
    'Admin' => [
        'images' => [
            // 'banners' => [
            //     1 => [
            //         'desktop' => ''
            //     ]
            // ],
            // 'categories' => [
            //     'main' => '',
            // ],
            // 'articles' => [
            //     'main' => '16:9 (aspoň 1000px na dlhú stranu)',
            //     'thumbnail' => '5:3 (aspoň 500px na dlhú stranu)'
            // ],
        ],
    ],
    'TinyMCE.configs.extraSimple.height' => 250,
    'TinyMCE.configs.openings' => [
        'plugins' => 'code fullscreen paste table',
        'menubar' => false,
        'height' => 250,
        'resize' => true,
        'toolbar1' => 'undo redo | table | removeformat | fullscreen code'
    ]
];
