<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'social' => '
        <a href="{{url}}" title="{{title}}" class="c-btn c-btn--{{name}} {{classes}}" {{attrs}}>
            <i class="{{icon}}" aria-hidden="true"></i>
            <span>{{text}}</span>
        </a>'
];
