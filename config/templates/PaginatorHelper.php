<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'wrapper' => '<nav class="t_center">{{content}}</nav>',
    'list' => '<ul class="{{class}} pagination right hidden-xs hidden-sm">{{content}}</ul>',
    'number' => '<li class="pager__item"><a href="{{url}}">{{text}}</a></li>',
    'first' => '<li class="pager__item"><a href="{{url}}">{{text}}</a></li>',
    'last' => '<li class="pager__item"><a href="{{url}}">{{text}}</a></li>',
    'nextActive' => '<li class="pager__item next pager__item--last"><a rel="next" href="{{url}}">&gt;</a></li>',
    'nextDisabled' => '<li class="pager__item next pager__item--last disabled"><a rel="next" href="{{url}}">&gt;</a></li>',
    'prevActive' => '<li class="pager__item prev pager__item--first"><a rel="prev" href="{{url}}">&lt;</a></li>',
    'prevDisabled' => '<li class="pager__item prev pager__item--first disabled"><a rel="prev" href="{{url}}">&lt;</a></li>',
    'current' => '<li class="active pager__item"><a href="{{url}}">{{text}}</a></li>',
    'ellipsis' => '<li class="pager__item pager__item--dots">...</li>',
    'loadMore' => '<button class="btn btn-default pager__load" type="button" data-url="{{url}}" data-page="{{page}}" data-max="{{pageCount}}" title="' . __('Zobraziť {{nextText}} {{perPage}} {{itemText}}') . '" {{attrs}}>' . __('Zobraziť {{nextText}} {{perPage}} {{itemText}}') . '</button>'
];
