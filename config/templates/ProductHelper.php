<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'cart' => '
        <div class="{{class}}" {{attrs}}>
            {{formStart}}
                <div class="products__quantity">
                    {{quantity}}
                </div>
                <button class="c-btn c-btn--cart is-sm products__buy" type="submit" data-ec-id="{{productId}}" data-ec-item="Products" data-ec-action="addProductToCart" {{disabled}}>
                    <i class="ico ico--minicart left"></i>
                    <span>
                        {{text}}
                    </span>
                </button>

                <input type="hidden" value="{{productId}}" name="id" />
                <input type="hidden" value="{{cartItemType}}" name="cartItemType" />
            {{formEnd}}
        </div>
    ',
    'variation' => '{{input}}',
    'variationsContainer' => '{{content}}',
    'compare' => '
        <div class="{{class}}" {{attrs}}>
            {{formStart}}
                <button class="btn btn-default products__compare" type="submit" data-ec-id="{{productId}}" data-ec-item="Products" data-ec-action="addProductToSearch" {{disabled}}>
                    <i class="ico ico--minicart left"></i>
                    <span>
                        {{text}}
                    </span>
                </button>
                <input type="hidden" value="{{productId}}" name="id" />
            {{formEnd}}
        </div>
    ',
    'cartUnauthenticated' => '{{content}}',
    'cartNotSaleable' => '',
    'ribbon' => '<div class="products__ribbon products__ribbon--{{type}}">{{text}}</div>',
    'tagsContainer' => '<div class="products__tags">{{prepend}}<ul class="list"></ul>{{append}}</div>',
    'tag' => '<li><a href="{{url}}" title="{{url}}">{{name}}</a></li>',
    'download' => '<a href="{{url}}" title="{{title}}" class="product__download" target="_blank">{{name}}</a>',
    'backLink' => '<a href="{{url}} title="{{title}}" {{attrs}}>{{name}}</a>'
];
