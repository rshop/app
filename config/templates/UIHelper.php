<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'dropdownWrapper' => '<div class="c-dropdown c-dropdown--{{type}} js-dropdown">{{content}}</div>',
    'dropdownContent' => '{{button}} {{list}}',
    'dropdownButton' => '
        <button class="c-dropdown__btn c-btn {{classes}}">
            <span>{{name}}</span>
            <i class="{{icon}} is-on-right" aria-hidden="true"></i>
        </button>
    ',
    'dropdownList' => '<ul class="c-dropdown__list">{{items}}</ul>',
    'dropdownItem' => '
        <li class="c-dropdown__item">
            <a href="{{url}}" title="{{title}}" class="c-dropdown__link c-link has-arrow {{classes}}" {{attr}}>
                <i class="fa fa-caret-right" aria-hidden="true"></i>
                <span>{{name}}</span>
            </a>
        </li>
    '
];
