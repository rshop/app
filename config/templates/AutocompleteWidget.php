<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

use Cake\Core\Configure;

return [
    'default' => '<a href="{{item.link}}" title="{{item.value}}">{{item.name}}</a>',
    'heading' => '<h3>{{item.name}}</h3>',
    'all' => '<a href="{{item.link}}" title="{{item.name}}">{{item.name}}</a>',
    'noresults' => '<h3>{{item.name}}</h3>',
    'Products' => '
        <a href="{{item.link}}" title="{{item.value}}" data-id="{{item.id}}">
            <table>
                <tr>
                    <td class="autocomplete__img">{{item.image}}</td>
                    <td class="autocomplete__name">{{item.name}}</td>
                    <td class="autocomplete__price">
                        <strong>' . (!Configure::read('Rshop.pricesWithVat') ? '{{item.price}}' : '{{item.price_vat}}') . '</strong>
                    </td>
                </tr>
            </table>
        </a>
    ',
    'Categories' => '
        <a href="{{item.link}}" title="{{item.value}}" data-id="{{item.id}}">
            {{item.name}}
            <span class="autocomplete__parent">{{item.parent}}</span>
        </a>
    ',
    'Manufacturers' => '<a href="{{item.link}}" title="{{item.value}}" data-id="{{item.id}}">{{item.name}}</a>',
    'Articles' => '<a href="{{item.link}}" title="{{item.value}}" data-id="{{item.id}}">{{item.name}}</a>'
];
