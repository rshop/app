<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'trigger' => '
        <div class="header__minicart">
            <a href="{{url}}" class="btn btn-primary" title="{{name}}" data-toggle="aside" data-target="#cartAside" {{attrs}}>
                {{prepend}}<span class="hidden-cxs cart__totals__total">{{content}}</span>{{append}}
                <span class="badge cart__totals__quantity">{{count}}</span>
            </a>
        </div>
    ',
    'serviceGroup' => '<strong>{{name}}</strong><br /><ul>{{content}}</ul>',
    'service' => '<li>{{input}}{{name}}</li>',
    'moduleInput' => '
        <input type="{{type}}" class="{{class}}" id="{{moduleType}}-{{id}}" name="{{moduleType}}_id" value="{{id}}" data-name="{{name}}" {{available}} {{checked}} {{disabled}}>
    '
];
