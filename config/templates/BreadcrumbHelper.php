<?php

/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '{{content}}',
    'list' => '<div class="c-breadcrumb__list">{{content}}</div>',
    'home' => '
        <div class="c-breadcrumb__item is-home">
            <a href="/" title="{{name}}" class="c-link">
                ' . __('Domov') . '
            </a>
        </div>
    ',
    'delimiter' => '',
    'item' => '
        <div class="c-breadcrumb__item">
            <div class="sc-delimiter">
                <i class="ico ico-chevron-right" aria-hidden="true"></i>
            </div>

            <a href="{{url}}" title="{{name}}" class="c-link">
                <span>{{name}}</span>
            </a>
        </div>
    ',
    'itemList' => '
        <div class="c-breadcrumb__item">
            <div class="sc-delimiter">
                <i class="ico ico-chevron-right" aria-hidden="true"></i>
            </div>

            <a href="{{url}}" title="{{name}}" class="c-link">
                <span>{{name}}</span>
            </a>
        </div>
    ',
    'subitem' => '
        <li class="subbread__item">
            <a href="{{url}}" title="{{name}}">{{name}}</a>
        </li>
    ',
    'last' => '
        <div class="c-breadcrumb__item">
            <div class="sc-delimiter">
                <i class="ico ico-chevron-right" aria-hidden="true"></i>
            </div>

            <span class="sc-last">{{name}}</span>
        </div>
    ',
];
