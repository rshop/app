<?php
return [
    'button' => '<button{{attrs}}>{{text}}</button>',
    'checkbox' => '<input type="checkbox" {{inputAttrs}} name="{{name}}" value="{{value}}" class="c-form__input {{inputClass}}" data-custom="{{custom}}" {{attrs}}>',
    'checkboxFormGroup' => '{{label}}',
    'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
    'checkboxContainer' => '<div class="c-form__item c-form__item--checkbox {{cols}}">{{content}}</div>',
    'dateWidget' => '{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}',
    'error' => '<span class="help-block">{{content}}</span>',
    'errorList' => '<ul>{{content}}</ul>',
    'errorItem' => '<li>{{text}}</li>',
    'file' => '<input type="file" {{inputAttrs}} name="{{name}}" class="c-form__input {{inputClass}}" {{attrs}}>',
    'fieldset' => '<fieldset{{attrs}}>{{content}}</fieldset>',
    'formStart' => '<form{{attrs}}>',
    'formEnd' => '</form>',
    'textFormGroup' => '{{label}}{{prepend}}{{input}}{{append}}',
    'formGroup' => '{{label}}{{prepend}}{{input}}{{append}}',
    'hiddenBlock' => '<div style="display:none;">{{content}}</div>',
    'input' => '<input type="{{type}}" name="{{name}}" class="c-form__input {{class}} {{inputClass}}" {{inputAttrs}} {{attrs}} />',
    'inputSubmit' => '<input type="{{type}}"{{attrs}}>',
    'inputContainer' => '
        <div class="c-form__item c-form__item--{{type}} {{holder}} {{cols}}" {{holderClass}}>
            {{content}}
            <div class="sc-note">{{inputNote}}</div>
        </div>',
    'inputContainerError' => '
        <div class="c-form__item c-form__item--{{type}} {{holder}} {{cols}}" {{holderClass}}>
            {{content}}
            <label class="error">{{error}}</label>
            <div class="sc-note">{{inputNote}}</div>
        </div>',
    'label' => '
        <label class="c-form__label {{labelClass}}" {{attrs}}>
            <span class="sc-text">{{text}}</span>
            <span class="sc-label-addon">{{labelAddon}}</span>
            <span class="sc-input-addon">{{inputAddon}}</span>
        </label>',
    'nestingLabel' => '
        {{hidden}}{{input}}
        <label class="c-form__label"{{attrs}}>
            <span class="sc-toggle"></span>
            <span class="sc-text">{{text}}</span>
        </label>',
    'legend' => '<legend>{{text}}</legend>',
    'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
    'optgroup' => '<optgroup label="{{label}}"{{attrs}}>{{content}}</optgroup>',
    'select' => '
        <div class="c-form__item c-form__item--select {{holder}} {{cols}} {{required}}" {{holderClass}}>
            <label class="c-form__label" for="{{name}}">
                <span class="sc-text">{{labelText}}</span>
            </label>
            <select name="{{name}}" class="c-form__input {{inputClass}} {{class}}" {{inputAttrs}} {{attrs}}>
                {{content}}
            </select>
        </div>',
    'selectMultiple' => '<select name="{{name}}" multiple="multiple" class="c-form__input {{attrs.class}} {{inputClass}}" {{attrs}}>{{content}}</select>',
    'radio' => '<input type="radio" {{inputAttrs}} name="{{name}}" value="{{value}}" class="c-form__input {{inputClass}}" data-custom="{{custom}}" {{attrs}}>',
    'radioWrapper' => '<div class="c-form-radio">{{label}}</div>',
    'radioContainer' => '<div class="c-form__item c-form__item--radio {{isRequired}}">{{content}}</div>',
    'radioGroupContainer' => '<div class="btn-group" data-toggle="buttons">{{content}}</div>',
    'textarea' => '<textarea name="{{name}}" class="c-form__input c-form__input--textarea {{attrs.class}} {{inputClass}}" {{attrs}}>{{value}}</textarea>',
    'submitContainer' => '<div class="form-group">{{content}}</div>',
    'autocomplete' => '<input type="text" name="{{name}}" {{attrs}} class="c-form__input {{class}}" />{{append}}',
    'spinners' => '
        <div class="spinners input-group-btn-vertical">
            <button class="btn btn-default btn-xs" data-value="1">
                <span class="icon glyphicon glyphicon-triangle-top"></span>
            </button>
            <button class="btn btn-default btn-xs" data-value="-1">
                <span class="icon glyphicon glyphicon-triangle-bottom"></span>
            </button>
        </div>
    ',
    'newSpinner' => '
        <r-spinner class="spinner-wrapper c-form__quantity {{wrapperClass}}" {{wrapperAttrs}}>
            <button type="button" class="is-decrement c-btn c-btn--cart {{decrementClass}}" decrement>
                <i class="ico ico-minus"></i>
            </button>

            <input type="text" class="c-form__input {{inputClass}}" value="{{value}}" {{attrs}} autocomplete="off" inputmode="decimal" />

            <button type="button" class="is-increment c-btn c-btn--cart {{incrementClass}}" increment>
                <i class="ico ico-plus"></i>
            </button>
        </r-spinner>
    ',
    'range' => '
        <div class="range">
            <div class="form-group">
                <input type="text" class="c-form__input range__from" id="{{id}}__from" name="{{name}}[from]" data-min="{{min}}"  placeholder="{{minPlaceholder}}" value="{{from}}">
            </div>
            <div id="{{id}}" class="range__slider" data-currency="{{currency}}"></div>
            <div class="form-group">
                <input type="text" class="c-form__input range__to" id="{{id}}-range-to" name="{{name}}[to]" data-max="{{max}}"  placeholder="{{maxPlaceholder}}" value="{{to}}">
            </div>
            <div class="breaker"></div>
        </div>
    ',
];
