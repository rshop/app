<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'bubblesContainer' => '<div class="filter__bubbles">{{content}}</div>',
    'bubble' => '<a href="{{url}}" class="filter__bubbles__item {{class}}" data-filter-key="{{key}}" data-filter-value="{{value}}">
        {{content}}<span class="glyphicon glyphicon-remove"></span>
    </a>',
    'bubbleReset' => '<a href="#" class="filter__bubbles__item {{class}}" title="' . __('Zrušiť všetky filtre') . '" type="reset">' . __('Zrušiť všetky filtre') . '</a>',
    'histogramContainer' => '<div class="filter__histogram">{{content}}</div>',
    'histogramItem' => '
        <div class="filter__histogram__col">
            <a href="#" class="filter__histogram__content" data-count="{{count}}" data-from="{{from}}" data-to="{{to}}" title="{{title}}">
                <span style="height:{{percentage}}"></span>
            </a>
        </div>
    '
];
