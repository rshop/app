<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'textFormGroup' => '{{label}}{{prepend}}{{input}}{{append}}',
    'inputContainer' => '<div class="form-group {{holder}} {{cols}} {{type}}{{required}}" {{attrs}}>{{content}}</div>',
    'checkboxContainer' => '<div class="form-group {{cols}} {{class}}"><div class="checkbox">{{content}}</div></div>'
];
