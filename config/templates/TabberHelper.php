<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '<div {{attrs}}>{{navigation}}<div class="tab-content">{{content}}</div></div>',
    'navigation' => '<ul class="nav nav-tabs {{attrs.class}}" {{attrs}} role="tablist">{{content}}</ul>',
    'navigationItem' => '<li class="{{active}}" {{attrs}} role="presentation"><a href="#{{id}}" title="{{name}}"  data-toggle="tab" role="tab">{{name}}</a></li>',
    'item' => '<div id="{{id}}" class="tab-pane {{active}}" {{attrs}} role="tabpanel">{{content}}</div>'
];
