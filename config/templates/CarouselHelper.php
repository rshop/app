<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '
        <div id="{{id}}" class="carousel slide {{attrs.class}}" data-ride="carousel" {{attrs}}>
            {{indicators}}
            <div class="carousel-inner">
                {{content}}
            </div>
            {{arrowleft}}{{arrowright}}
        </div>
    ',
    'item' => '<div class="item {{active}} {{attrs.class}}" {{attrs}}>{{content}}</div>',
    'arrowLeft' => '<a href="#{{id}}" data-slide="prev" class="left carousel-control {{attrs.class}}" role="button" {{attrs}}>{{content}}</a>',
    'arrowRight' => '<a href="#{{id}}" data-slide="next" class="right carousel-control {{attrs.class}}" role="button" {{attrs}}>{{content}}</a>',
    'indicatorContainer' => '<ol class="carousel-indicators {{active}} {{attrs.class}}" {{attrs}}>{{content}}</ol>',
    'indicator' => '<li data-target="#{{id}}" data-slide-to="{{index}}" {{attrs}}></li>'
];
