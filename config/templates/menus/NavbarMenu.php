<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '
        <nav id="{{id}}_holder" class="{{holder}} navbar navigation container" role="navigation">
            {{prepend}}
            {{content}}
            {{append}}
        </nav>',
    'navigation' => '
        <div id="{{id}}" class="{{holder}} navigation__menu">
            {{prepend}}
            <ul class="nav navbar-nav {{class}} level0">{{content}}</ul>
            {{append}}
        </div>
    ',
    'navigation_level1' => '<ul class="dropdown-menu level{{level}}">{{content}}</ul>',
    'item' => '
        <li class="navigation__menu__item {{class}}">
            <a href="{{url}}" title="{{name}}" {{linkOptions}}>{{prepend}}{{content}}</a>{{append}}
        </li>
    '
];
