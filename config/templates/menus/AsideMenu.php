<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '
        <aside class="aside aside--nav">
            <div class="aside__header">
                <div class="aside__title">' . __('Navigácia') . '</div>
                <a href="#" title="' . __('Zavrieť') . '" class="aside__close">
                    ' . __('Zavrieť') . '
                    <span class="glyphicon glyphicon-remove-circle"></span>
                </a>
            </div>
            {{prepend}}{{content}}{{append}}
        </aside>
    ',
    'navigation' => '
        <div class="aside__content">
            {{prepend}}{{content}}{{append}}
        </div>
    ',
    'item' => '
        <div class="aside--nav__item">
            {{prepend}}
            <a href="{{url}}" title="{{name}}">
                {{content}}
            </a>
            {{append}}
        </div>
    ',
    'navigation_level1' => '
        <div class="aside--subnav">
            <a href="#" title="' . __('Hlavné kategórie') . '" class="aside--subnav__back">
                ' . __('Hlavné kategórie') . '
            </a>
            <ul class="aside--subnav__list">
                {{content}}
            </ul>
            <a href="#" title="' . __('Hlavné kategórie') . '" class="aside--subnav__back">
                ' . __('Hlavné kategórie') . '
            </a>
        </div>
    ',
    'item_level1' => '
        <li class="aside--subnav__item">
            <a href="{{url}}" title="{{name}}">
                <table>
                    <tr>
                        <td class="aside--subnav__img">
                            {{image}}
                        </td>
                        <td class="aside--subnav__name">
                            {{content}}
                        </td>
                    </tr>
                </table>
            </a>
        </li>
    '
];
