<?php
/**
 * This file is part of rshop/frontend package.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '
        <nav id="{{id}}_holder" class="{{holder}} navigation" role="navigation">
            {{prepend}}
            {{content}}
            {{append}}
        </nav>',
    'navigation' => '
        <div id="{{id}}" class="{{holder}} navigation__menu">
            {{prepend}}
            <ul class="navigation__list {{class}} level0">{{content}}</ul>
            {{append}}
        </div>
    ',
    'navigation_level1' => '<ul class="dropdown-menu level{{level}}">{{content}}</ul>',
    'item' => '
        <li class="navigation__item {{class}}">
            <a href="{{url}}" title="{{name}}" {{linkOptions}}>{{prepend}}{{content}}</a>{{append}}
        </li>
    '
];
