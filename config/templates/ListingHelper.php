<?php

/**
 * This file is part of rshop/biutli project.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '<div id="{{id}}" class="c-listing-products {{class}}">{{content}}{{fetch}}{{pager}}</div>',
    'holder' => '<div class="c-listing-products__holder listing__holder">{{content}}</div>',
    'item' => '<div class="c-listing-products__item {{class}}">{{content}}</div>',
    'emptyHolder' => '<div class="c-msg c-msg--blank is-empty h-text-center">{{content}}</div>',
    'empty' => __('Tento zoznam neobsahuje žiadne položky'),
    'fetch' => '<div class="listing-fetch"></div>',
    'banner' => '<div class="listing__baner>{{content}}</div>',
];
