<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

return [
    'container' => '
        <div id="{{id}}" class="modal fade c-modal {{containerClass}}" tabindex="-1" role="dialog" {{attrs}}>
            <div class="c-modal__loader">
                <span class="js-modal-loader-text">{{loaderText}}</span>
            </div>
            <div class="modal-dialog {{dialogClass}}">
                <div class="modal-content {{contentClass}}">
                    {{header}}

                    {{content}}
                </div>
            </div>
        </div>
    ',
    'header' => '
        <div class="c-modal__header">
            <div class="c-modal__name">{{name}}</div>

            <button class="c-btn c-btn--transparent is-sm" type="button" data-dismiss="modal">
                <i class="ico ico-cross" aria-hidden="true"></i>
            </button>
        </div>
    '
];
