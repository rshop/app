<?php
/**
 * Use to override RSHOP FRONTEND configuration
 */

use Rshop\FrontendUtility\Routing\Router;

$blogId = 1;
$logoUrl = Router::url('/img/logos/rshop.svg', ['full' => true]);

return [
    'Eshop' => [
        'internetExplorerSupported' => false,
        'buildAllResourcesInRoot' => true,
        'noImage' => '/img/static/noimage.svg',
        'Project' => [
            'name' => 'rshop',
            'blogId' => $blogId
        ],
        'Logs' => [
            'Sentry' => [
                'active' => true,
                'sampleRate' => '1',
            ]
        ],
        'registrationPasswordComparison' => false,
    ],
    'Product' => [
        'available_status_color' => '#418769',
        'unvailable_status_color' => '#000',
        'newSpinner' => true,
        'views' => [
            'default' => 'grid',
            'grid' => 'Rshop/Frontend./Common/Product/thumb',
            'row' => 'Rshop/Frontend./Common/Product/thumb'
        ]
    ],
    'Cart' => [
        'sameAddressCheckboxName' => 'billing_same_as_shipping',
        'restoreItemsFromBackend' => true,
        'DiscountSelector' => 'voucher,points,discount,discount_discount,quantity_discount,cheapest_free_discount',
        'ProductItemsSelector' => 'product,service,present,voucher,configurator_discount,discount_discount,quantity_discount,cheapest_free_discount',
        'FreeShipping' => [
            'price' => __('Nakúpte ešte za {0} a dopravu máte zadarmo', ':freeShippingAmount'),
            'free' => __('K tomuto nákupu máte dopravu zdarma'),
        ],
        'Modules' => [
            'upsell' => false
        ],
        'Steps' => [
            'cart' => [
                'next' => ['controller' => 'Shopping', 'action' => 'form']
            ],
            'form' => [
                'next' => ['controller' => 'Shopping', 'action' => 'modules'],
                'step' => 1
            ],
            'modules' => [
                'next' => ['controller' => 'Shopping', 'action' => 'success'],
                'step' => 2
            ]
        ],
        'Redirects' => [
            'Success' => [
                'register' => ['controller' => 'Shopping', 'action' => 'modules']
            ]
        ],
        'Processes' => [
            'finalize' => [
                'steps' => [
                    'modules' => true
                ]
            ]
        ],
        'Validations' => [
            'modules' => [
                'before' => [
                    'form' => false,
                    'register' => false
                ]
            ]
        ]
    ],
    'Messages' => [
        'Shopping' => [
            'addedProduct' => __('Produkt bol úspešne pridaný {0}do košíka{1}', '<a href="' . Router::url(['urlKey' => 'Shopping.cart']) . '" title="' . __('Nákupný košík') . '">', '</a>'),
        ],
        'Validation' => [
            'skVatNum' => __('Zadajte IČ DPH v správnom tvare.'),
        ]
    ],
    'Url' => [
        'Articles' => [
            'ObchPodmienky' => ['plugin' => 'Rshop/Frontend', '_name' => 'Articles', 'name' => 'obchodne-podmienky', 'id' => 1],
        ],
        'Sections' => [
            'Blog' => ['plugin' => 'Rshop/Frontend', '_name' => 'Sections', 'name' => 'blogove-clanky', 'id' => $blogId]
        ],
        'External' => [
            'Facebook' => '',
            'Instagram' => '',
            'Youtube' => '',
        ],
    ],
    'Search' => [
        'tables' => ['Categories', 'Products', 'Articles'],
        'items' => [
            'Items' => [
                'Categories' => [
                    'limit' => 5,
                ],
                'Products' => [
                    'limit' => 5,
                ],
                'Articles' => [
                    'active' => true,
                    'limit' => 3
                ],
            ]
        ],
    ],
    'Listing' => [
        'paginator' => [
            'options' => [
                'numbers' => [
                    'maximum' => 3
                ]
            ],
            'Catalog' => [
                'search' => [
                    'limit' => 12
                ],
                'listing' => [
                    'limit' => 12
                ]
            ],
            'Content' => [
                'listing' => [
                    'limit' => 12
                ]
            ],
            'Marks' => [
                'view' => [
                    'limit' => 12
                ],
                'listing' => [
                    'limit' => 1000
                ]
            ]
        ]
    ],
    'Filter' => [
        'version' => '2',
        'sorts' => [
            'name' => [
                'active' => false
            ],
            'name:d' => [
                'active' => false
            ],
            'price' => [
                'label' => __('Najlacnejšie'),
                'sort' => 1
            ],
            'price:d' => [
                'label' => __('Najdrahšie'),
                'sort' => 2
            ],
        ],
    ],
    'Users' => [
        'Social' => [
            'login' => true,
            'authenticator' => 'Social'
        ],
    ],
    'Auth.authenticate' => [
        'all' => [
            'scope' => []
        ],
        'Form' => [
            'fields' => [
                'username' => 'login',
                'password' => 'password'
            ],
            'userModel' => 'Rshop/Admin.CustomerLogins',
            'finder' => 'auth'
        ],
        // totok prec ak nepouzivame social login
        'Rshop/Frontend.Social' => [
            'fields' => [
                'username' => 'login',
            ],
            'userModel' => 'Rshop/Admin.CustomerLogins',
            'finder' => 'auth'
        ],
    ],
    'Api' => [
        'GoogleCaptcha' => [
            'enabled' => true,
            'v3' => [
                'enabled' => true
            ]
        ]
    ],
    'Seo' => [
        'logoUrl' => $logoUrl,
        'meta' => [
            'facebook' => [
                'image' => $logoUrl
            ],
            'twitter' => [
                'site' => '@rshop',
                'image' => $logoUrl
            ]
        ]
    ],
];
