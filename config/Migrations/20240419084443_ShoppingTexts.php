<?php
use Migrations\AbstractMigration;

class ShoppingTexts extends AbstractMigration
{
    protected $_messagesTextConfigurations = [
        [
            'configuration_key' => 'text.shopping.infoPhone',
            'name' => 'Hlavička - Tel. č.',
            'value' => '+421 2 555 65 726',
        ],
        [
            'configuration_key' => 'text.shopping.infoPhoneText',
            'name' => 'Hlavička - Text pri tel. č.',
            'value' => 'Pon-Pia  8:00 - 16:00',
        ],
    ];

    /**
     * Up Method.
     */
    public function up()
    {
        $configurationGroupId = $this->fetchRow("SELECT id FROM rshop_configuration_groups WHERE identifier = 'text_shopping'")['id'];
        $sort = \intval($this->fetchRow("SELECT MAX(sort) AS largestSort FROM rshop_configurations WHERE configuration_group_id = '" . $configurationGroupId . "'")['largestSort']);

        $configurations = [];

        foreach ($this->_messagesTextConfigurations as $key => $configuration) {
            $configurations[$key] = \array_merge([
                'configuration_group_id' => $configurationGroupId,
                'type' => 'text',
                'public' => 1,
                'language_dependent' => 1,
                'shop_dependent' => 0,
                'sort' => ++$sort
            ], $configuration);
        }

        $this->table('rshop_configurations')
            ->insert($configurations)
            ->save();
    }

    /**
     * Down Method.
     */
    public function down()
    {
        $configKeys = \array_map(function ($configuration) {
            return "'" . $configuration['configuration_key'] . "'";
        }, $this->_messagesTextConfigurations);

        $this->execute('DELETE FROM rshop_configurations where configuration_key IN (' . \implode(',', $configKeys) . ')');
    }
}
