<?php
use Migrations\AbstractMigration;

class FavoriteProducts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('rshop_custom_favorite_products')
            ->addColumn('cookie', 'char', [
                'limit' => 40,
                'null' => false,
                'default' => '',
            ])
            ->addColumn('customer_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('product_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('product_variation_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'default' => null,
            ])
            ->addForeignKey('customer_id', 'rshop_customers', 'id', ['delete' => 'SET NULL', 'update' => 'CASCADE'])
            ->addForeignKey('product_id', 'rshop_products', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->addForeignKey('product_variation_id', 'rshop_product_variations', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->addIndex(['created'])
            ->addIndex(['cookie'])
            ->create();

        $this->table('rshop_products')
            ->addColumn('custom_favorite_sum', 'integer', [
                'after' => 'rating_sum',
                'limit' => 11,
                'null' => true,
                'default' => null
            ])
            ->update();
    }
}
