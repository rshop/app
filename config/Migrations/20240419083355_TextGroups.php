<?php
use Migrations\AbstractMigration;

class TextGroups extends AbstractMigration
{
    /**
     * Up Method.
     */
    public function up()
    {
        $largestSort = \intval($this->fetchRow('SELECT MAX(sort) AS largestSort FROM rshop_configuration_groups WHERE is_text = 1')['largestSort']);

        $this->table('rshop_configuration_groups')
            ->insert([
                [
                    'name' => 'Všeobecné',
                    'identifier' => 'text_general',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Hlavička / Pätička',
                    'identifier' => 'text_header_footer',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Index',
                    'identifier' => 'text_index',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Listing',
                    'identifier' => 'text_listing',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Detail produktu',
                    'identifier' => 'text_detail',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Môj účet',
                    'identifier' => 'text_account',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Nákupný proces',
                    'identifier' => 'text_shopping',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Kontakt',
                    'identifier' => 'text_contact',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Modálne okná',
                    'identifier' => 'text_modal',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->insert([
                [
                    'name' => 'Správy',
                    'identifier' => 'text_messages',
                    'public' => 1,
                    'sort' => ++$largestSort,
                    'is_text' => 1
                ]
            ])
            ->save();
    }

    /**
     * Down Method.
     */
    public function down()
    {
        $this->execute("DELETE FROM rshop_configuration_groups where identifier IN ('text_general', 'text_header_footer', 'text_index', 'text_listing', 'text_detail', 'text_account', 'text_shopping', 'text_contact', 'text_modal', 'text_messages')");
    }
}
