<?php
use Migrations\AbstractMigration;

class DefaultBoxes extends AbstractMigration
{
    protected $_boxes = [
        [
            'name' => 'Nákup - Odkazy v pätičke',
            'type' => 'custom',
            'active' => 1,
            'box_key' => 'shopping-links',
        ],
        [
            'name' => 'Nákup - Výhody registrovaného zákazníka',
            'type' => 'custom',
            'active' => 1,
            'box_key' => 'shopping-benefits-registred',
        ],
    ];

    /**
     * Up Method.
     */
    public function up()
    {
        $largestSort = \intval($this->fetchRow('SELECT MAX(sort) AS largestSort FROM rshop_boxes')['largestSort']);
        $largestId = \intval($this->fetchRow('SELECT MAX(id) AS largestId FROM rshop_boxes')['largestId']);

        $boxes = [];

        foreach ($this->_boxes as $box) {
            $boxes[] = \array_merge([
                'id' => ++$largestId,
                'sort' => ++$largestSort,
                'language_active' => 1,
            ], $box);
        }



        $this->table('rshop_boxes')
            ->insert($boxes)
            ->save();
    }

    /**
     * Down Method.
     */
    public function down()
    {
        $configBoxKeys = \array_map(function ($configuration) {
            return "'" . $configuration['box_key'] . "'";
        }, $this->_boxes);

        $this->execute('DELETE FROM rshop_boxes where box_key IN (' . \implode(',', $configBoxKeys) . ')');
    }
}
