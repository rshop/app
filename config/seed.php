<?php
/**
 * This file is part of rshop/cbelektro project.
 *
 * (c) RIESENIA.com
 */

namespace App\Config\BasicSeed;

$data = [
    'Rshop/Admin.BannerPlaces' => [
        '_defaults' => [],
    ],
    'Rshop/Admin.Boxes' => [
        '_defaults' => [],
    ]
];

$this->importTables($data);
