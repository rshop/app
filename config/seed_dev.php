<?php
namespace App\Config\BasicSeed;

$data = [];

/* options */
$options = [
    'manufacturers' => false,
    'properties' => false,
    'categories' => false, // Dependencies: properties
    'products' => false, // Dependencies: categories, manufacturers, properties
    'branches' => false,
    'benefits' => false,
    'sections' => false,
    'articles' => false, // Dependencies: sections
    'boxesData' => false
];

/* data */
$manufacturers = [
    'Manufacturer 1', 'Manufacturer 2',
];

$productTypes = [
    [
        'name' => 'Produkty',
        'groups' => [
            [
                'name' => 'Parametre',
            ]
        ],
    ],
];

$productTypeUnits = [
    'mm', 'kg', '°C'
];

$properties = [
    [
        'name' => 'Farba',
        'type' => 'select'
    ],
    [
        'name' => 'Wifi',
        'type' => 'boolean'
    ],
];

$productTypeOptions = [
    [
        'property_id' => 1,
        'value' => 'Biela'
    ],
    [
        'property_id' => 1,
        'value' => 'Oranžová'
    ],
    [
        'property_id' => 1,
        'value' => 'Čierna'
    ]
];

$categories = [
    [
        'name' => 'Hlavná kategória 1',
        'menu_name' => 'Kat. 1',
        'sub' => [
            [
                'name' => 'Subkategória 1',
                'menu_name' => 'Subkat. 1',
                'sub' => [
                    [
                        'name' => 'Subsubkat. 1'
                    ],
                    [
                        'name' => 'Subsubkat. 2'
                    ],
                ]
            ],
            [
                'name' => 'Subkategória 2',
                'menu_name' => 'Subkat. 2',
                'sub' => [
                    [
                        'name' => 'Subsubkat. 3'
                    ],
                    [
                        'name' => 'Subsubkat. 4'
                    ],
                ]
            ],
            [
                'name' => 'Subkategória 3'
            ],
        ]
    ],
    [
        'name' => 'Hlavná kategória 2',
        'menu_name' => 'Kat. 2',
    ],
];

$benefits = [
    1 => [
        'name' => 'Jednoduché vrátenie',
        'description' => '<p>Nepoužitý produkt výmeníme kus za ruk a rýchlo vybavíme reklamáciu</p>',
    ],
];

$branches = [
    [
        'name' => 'Dunajská Streda',
        'city' => 'Dunajská Streda',
        'street' => 'Galantská cesta 360/12',
        'post_code' => '92901',
        'email' => 'info@aktaon.com',
        'phone' => '+421 905 901 604',
        'description' => '<p>R1 výjazd Dunajská streda.<br> Na mieste je možné platiť v hotovosti aj kreditnou kartou.<br> Pokiaľ platíte na firmu, vystavíme vám faktúru.</p>',
        'lunch_break' => 'od 12:00 - 12:30 obedná pauza',
        'map_link' => 'https://goo.gl/maps/GBjYTD9RPT7BcKRCA',
        'video' => 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d170884.30559943273!2d17.628502!3d47.993087!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae9e106f9be6abd8!2sAktaon%20s.r.o.!5e0!3m2!1sen!2sus!4v1663157139333!5m2!1sen!2sus',
    ],
];

$sections = [
    1 => [
        'name' => 'Rshop'
    ],
    2 => [
        'name' => 'Blog'
    ],
    3 => [
        'name' => 'Sub 1',
        'parent_id' => 2
    ],
    4 => [
        'name' => 'Sub 2',
        'parent_id' => 2
    ],
    5 => [
        'name' => 'Sub 3',
        'parent_id' => 2
    ],
];

$articles = [
    1 => [
        'name' => 'Obchodné podmienky',
        'section_id' => 1,
        'short_description' => 'short todo',
        'description' => 'long todo'
    ],
    2 => [
        'name' => 'Reklamačné podmienky',
        'section_id' => 1,
        'short_description' => 'short todo',
        'description' => 'long todo'
    ],
    3 => [
        'name' => 'Doprava a platba',
        'section_id' => 1,
        'short_description' => 'short todo',
        'description' => 'long todo'
    ],
    4 => [
        'name' => 'Clanok 1',
        'section_id' => 3,
        'short_description' => '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>',
        'description' => '<p>Lorem ipsum dolor <strong>sit amet</strong>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut <a href="#">labore et dolore magna</a> aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
    ],
    4 => [
        'name' => 'Clanok 2',
        'section_id' => 3,
        'short_description' => '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>',
        'description' => '<p>Lorem ipsum dolor <strong>sit amet</strong>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut <a href="#">labore et dolore magna</a> aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
    ],
    4 => [
        'name' => 'Clanok 3',
        'section_id' => 3,
        'short_description' => '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>',
        'description' => '<p>Lorem ipsum dolor <strong>sit amet</strong>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut <a href="#">labore et dolore magna</a> aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
    ],
];

$boxes = [
    1 => [
        [
            'name' => 'Akcie',
            'url' => '/akciove-produkty',
        ],
        [
            'name' => 'Novinky',
            'url' => '/novinky',
        ],
    ],
    2 => [
        [
            'name' => 'Kategórie produktov',
            'subitems_type' => 'custom',
            'subitems' => [
                ['name' => 'Akcie', 'url' => '/akciove-produkty'],
                ['name' => 'Novinky', 'url' => '/novinky'],
                ['name' => 'Dom a el. prístroje', 'url' => '#'],
                ['name' => 'Elektroinštalačný materiál', 'url' => '#'],
                ['name' => 'Montážny materiál', 'url' => '#'],
                ['name' => 'Nástroje a dieľňa', 'url' => '#'],
                ['name' => 'Smart riešenia', 'url' => '#'],
            ],
        ],
    ]
];

/* execution */
if ($options['manufacturers'] && \count($manufacturers) > 0) {
    $tmpCount = 0;

    $data['Rshop/Admin.Manufacturers'] = [
        '_defaults' => [],
    ];

    foreach ($manufacturers as $name) {
        $data['Rshop/Admin.Manufacturers'][] = [
            'id' => ++$tmpCount,
            'name' => $name,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra posuere leo, at interdum dui congue bibendum. Pellentesque non turpis consectetur, hendrerit ante semper, dapibus arcu. Nunc sem tortor, eleifend in odio at, porttitor ullamcorper velit. Proin quis accumsan tortor. Mauris sed leo sed diam cursus lobortis volutpat in erat. Aenean luctus ac nunc eu sollicitudin. Cras et eleifend lectus. In lobortis ex ac rutrum rutrum. Proin facilisis tempus accumsan. Aliquam ullamcorper molestie dictum. Cras iaculis erat et mi varius porttitor. Praesent eu turpis ut urna vestibulum tristique vitae ullamcorper urna.'
        ];
    }
}

if ($options['properties']) {
    if (\count($productTypeUnits) > 0) {
        $tmpCount = 0;

        $data['Rshop/Admin.ProductTypeUnits'] = [
            '_defaults' => [],
        ];

        foreach ($productTypeUnits as $name) {
            $data['Rshop/Admin.ProductTypeUnits'][] = [
                'id' => ++$tmpCount,
                'unit' => $name,
                'format' => 'after'
            ];
        }
    }

    if (\count($properties) > 0) {
        $tmpCount = 0;

        $data['Rshop/Admin.Properties'] = [
            '_defaults' => [],
        ];

        foreach ($properties as $property) {
            $data['Rshop/Admin.Properties'][] = [
                'id' => ++$tmpCount,
                'name' => $property['name'],
                'type' => $property['type'],
                'product_type_unit_group_id' => $property['product_type_unit_group_id'] ?? null,
            ];
        }
    }

    if (\count($productTypes) > 0) {
        $tmpCount = 0;
        $tmpGroupCount = 0;
        $tmpPropertyCount = 0;

        $data['Rshop/Admin.ProductTypes'] = [
            '_defaults' => [],
        ];
        $data['Rshop/Admin.ProductTypeGroups'] = [
            '_defaults' => [],
        ];
        $data['Rshop/Admin.ProductTypeProperties'] = [
            '_defaults' => [],
        ];
        $data['Rshop/Admin.ProductPropertyValues'] = [
            '_defaults' => [],
        ];

        foreach ($productTypes as $type) {
            $data['Rshop/Admin.ProductTypes'][] = [
                'id' => ++$tmpCount,
                'name' => $type['name'],
            ];

            if ($type['groups']) {
                $typeId = $tmpCount;

                foreach ($type['groups'] as $group) {
                    $data['Rshop/Admin.ProductTypeGroups'][] = [
                        'id' => ++$tmpGroupCount,
                        'product_type_id' => $tmpCount,
                        'name' => $group['name'],
                        'sort' => $tmpGroupCount
                    ];
                }
            }
        }

        foreach ($productTypeOptions as $typeOption) {
            $data['Rshop/Admin.ProductTypeOptions'][] = [
                'id' => ++$tmpCount,
                'property_id' => $typeOption['property_id'],
                'value' => $typeOption['value'],
                'description' => $typeOption['description'] ?? '',
            ];
        }
    }
}

if ($options['categories'] && \count($categories) > 0) {
    $tmpCount = 0;

    $data['Rshop/Admin.Categories'] = [
        '_defaults' => [],
    ];

    foreach ($categories as $category) {
        $data['Rshop/Admin.Categories'][] = [
            'id' => ++$tmpCount,
            'name' => $category['name'],
            'menu_name' => $category['menu_name'] ?? null,
            'parent_id' => null,
            'display' => $category['display'] ?? 1,
            'show_in_main_menu' => $category['show_in_main_menu'] ?? 1,
            'show_subcategories' => $category['show_subcategories'] ?? 1,
            'active' => $category['active'] ?? 1,
            'has_active_path' => $category['has_active_path'] ?? 1,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra posuere leo, at interdum dui congue bibendum. Pellentesque non turpis consectetur, hendrerit ante semper, dapibus arcu. Nunc sem tortor, eleifend in odio at, porttitor ullamcorper velit. Proin quis accumsan tortor. Mauris sed leo sed diam cursus lobortis volutpat in erat. Aenean luctus ac nunc eu sollicitudin. Cras et eleifend lectus. In lobortis ex ac rutrum rutrum. Proin facilisis tempus accumsan. Aliquam ullamcorper molestie dictum. Cras iaculis erat et mi varius porttitor. Praesent eu turpis ut urna vestibulum tristique vitae ullamcorper urna.',
            'product_type_id' => $category['product_type_id'] ?? 1,
            'sort' => $tmpCount
        ];

        if ($category['sub']) {
            $parentId = $tmpCount;

            foreach ($category['sub'] as $subCategory) {
                $data['Rshop/Admin.Categories'][] = [
                    'id' => ++$tmpCount,
                    'name' => $subCategory['name'],
                    'menu_name' => $subCategory['menu_name'] ?? null,
                    'parent_id' => $parentId,
                    'display' => $subCategory['display'] ?? 1,
                    'show_in_main_menu' => $subCategory['show_in_main_menu'] ?? 1,
                    'active' => $subCategory['active'] ?? 1,
                    'has_active_path' => $subCategory['has_active_path'] ?? 1,
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra posuere leo, at interdum dui congue bibendum. Pellentesque non turpis consectetur, hendrerit ante semper, dapibus arcu. Nunc sem tortor, eleifend in odio at, porttitor ullamcorper velit. Proin quis accumsan tortor. Mauris sed leo sed diam cursus lobortis volutpat in erat. Aenean luctus ac nunc eu sollicitudin. Cras et eleifend lectus. In lobortis ex ac rutrum rutrum. Proin facilisis tempus accumsan. Aliquam ullamcorper molestie dictum. Cras iaculis erat et mi varius porttitor. Praesent eu turpis ut urna vestibulum tristique vitae ullamcorper urna.'
                ];

                if ($subCategory['sub']) {
                    $subParentId = $tmpCount;

                    foreach ($subCategory['sub'] as $subSubCategory) {
                        $data['Rshop/Admin.Categories'][] = [
                            'id' => ++$tmpCount,
                            'name' => $subSubCategory['name'],
                            'menu_name' => $subSubCategory['menu_name'] ?? null,
                            'parent_id' => $subParentId,
                            'display' => $subSubCategory['display'] ?? 1,
                            'show_in_main_menu' => $subSubCategory['show_in_main_menu'] ?? 1,
                            'active' => $subSubCategory['active'] ?? 1,
                            'has_active_path' => $subSubCategory['has_active_path'] ?? 1,
                            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra posuere leo, at interdum dui congue bibendum. Pellentesque non turpis consectetur, hendrerit ante semper, dapibus arcu. Nunc sem tortor, eleifend in odio at, porttitor ullamcorper velit. Proin quis accumsan tortor. Mauris sed leo sed diam cursus lobortis volutpat in erat. Aenean luctus ac nunc eu sollicitudin. Cras et eleifend lectus. In lobortis ex ac rutrum rutrum. Proin facilisis tempus accumsan. Aliquam ullamcorper molestie dictum. Cras iaculis erat et mi varius porttitor. Praesent eu turpis ut urna vestibulum tristique vitae ullamcorper urna.'
                        ];
                    }
                }
            }
        }

        $categoriesCount = $tmpCount;
    }
}

if ($options['products']) {
    $tmpAdditional1 = ['Vykurovací', 'Izbový', 'Výhrevný', 'Tepelnoizolačný', 'Flexibilný', 'Priemyselný', 'Nástenný', 'Univerzálny spínací', 'Interiérový 2-tlačidlový'];
    $tmpNames = ['vypínač', 'ovládač', 'štartér', 'termostat', 'konvektor', 'kábel', 'panel', 'ventilátor', 'detektor pohybu'];
    $tmpAdditional2 = ['s automatickou žalúziou, časovým spínačom a guličkovým ložiskom', 'so sieťkou, biely 140x140 airRoxy', 'eneRgy 125 TS (ST) - s časovačom', 'SALUS čidlo 3m, FS300', 'do zásuvky', 's nastavitelným oneskorením vypnutia', 'LMDT-810', 'pod rohože', 's pohybujúcim sa plameňovým efektom', 'na myši a hlodavce, ultrazvukový', ' do bazéna', 's projektorom'];
    $tmpPropertyId = 0;
    $tmpShortDesc = ['Cupcake ipsum dolor sit amet. Halvah cake I love I love pie dragée cotton candy cupcake. Marshmallow cake topping candy canes dessert chocolate pie. Cotton candy fruitcake marshmallow candy lemon drops. Sweet candy macaroon jujubes tiramisu danish croissant sweet. Fruitcake I love dragée I love candy canes dessert. Liquorice apple pie topping brownie sugar plum I love.'];
    $tmpVolume = ['180', '250', '350', '420', '500', '750', '1250'];
    $tmpPriceId = 0;

    $data['Rshop/Admin.Products'] = [
        '_defaults' => [],
    ];

    $data['Rshop/Admin.ProductPrices'] = [
        '_defaults' => [],
    ];

    // az od 5-ky aby sme si omylom neprepisali produkty kede sme vsetko povyplnali
    for ($i = 5; $i < 150; ++$i) {
        $data['Rshop/Admin.Products'][] = [
            'id' => $i + 1,
            'name' => $tmpAdditional1[\array_rand($tmpAdditional1)] . ' ' . $tmpNames[\array_rand($tmpNames)] . ' ' . $tmpAdditional2[\array_rand($tmpAdditional2)],
            'model' => \rand(1000000, 9000000) . $i,
            'ean' => 'ean-' . $i . '-' . $i,
            'stock' => (\rand(1, 2) == 1 ? \rand(0, 4) : \rand(17, 44)),
            'short_description' => $tmpShortDesc[\array_rand($tmpShortDesc)],
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pharetra posuere leo, at interdum dui congue bibendum. Pellentesque non turpis consectetur, hendrerit ante semper, dapibus arcu. Nunc sem tortor, eleifend in odio at, porttitor ullamcorper velit. Proin quis accumsan tortor. Mauris sed leo sed diam cursus lobortis volutpat in erat. Aenean luctus ac nunc eu sollicitudin. Cras et eleifend lectus. In lobortis ex ac rutrum rutrum. Proin facilisis tempus accumsan. Aliquam ullamcorper molestie dictum. Cras iaculis erat et mi varius porttitor. Praesent eu turpis ut urna vestibulum tristique vitae ullamcorper urna.',
            'manufacturer_id' => \rand(1, \count($manufacturers)),
            'tax_class_id' => 1,
            'active' => 1,
            'product_type_id' => \rand(1, \count($productTypes)),
            'volume' => $tmpVolume[\array_rand($tmpVolume)]
        ];
        $data['Rshop/Admin.CategoriesProducts'][] = [
            'id' => $i + 1,
            'product_id' => $i + 1,
            'category_id' => \rand(1, $categoriesCount)
        ];
        $data['Rshop/Admin.ProductPrices'][] = [
            'id' => ++$tmpPriceId,
            'product_id' => $i + 1,
            'price_type_id' => 1,
            'price' => ($tmpPrice = (\rand(1, 6) == 1 ? \rand(200, 12000) : \rand(4, 199))),
            'price_vat' => $tmpPrice * 1.2
        ];
        if ($tmpPriceId % 5 == 0) {
            $data['Rshop/Admin.ProductPrices'][] = [
                'id' => ++$tmpPriceId,
                'product_id' => $i + 1,
                'price_type_id' => 2,
                'price' => ($tmpDiscountedPrice = $tmpPrice - rand(1, $tmpPrice - 3)),
                'price_vat' => $tmpDiscountedPrice * 1.2
            ];
        }

        /*$data['Rshop/Admin.ProductPropertyValues'][] = [
            'id' => ++$tmpPropertyId,
            'product_id' => $i + 1,
            'product_type_property_id' => 1,
            'decimal_value' => \rand(7, 35)
        ];

        $data['Rshop/Admin.ProductPropertyValues'][] = [
            'id' => ++$tmpPropertyId,
            'product_id' => $i + 1,
            'product_type_property_id' => 2,
            'decimal_value' => \rand(2, 80)
        ];*/
    }
}

if ($options['benefits'] && $benefits) {
    $tmpCount = 0;

    $data['Rshop/Admin.Benefits'] = [
        '_defaults' => [
            'icon' => '',
            'active' => 1
        ],
    ];

    foreach ($benefits as $benefitId => $benefit) {
        $data['Rshop/Admin.Benefits'][] = [
            'id' => $benefitId,
            'name' => $benefit['name'] ?? '',
            'description' => $benefit['description'] ?? '',
            'url' => $benefit['url'] ?? '#',
            'image' => $benefit['image'] ?? '',
            'sort' => ++$tmpCount,
        ];
    }
}

if ($options['branches'] && \count($branches) > 0) {
    $tmpCount = 0;

    $data['Rshop/Admin.Branches'] = [
        '_defaults' => [],
    ];

    foreach ($branches as $branch) {
        $data['Rshop/Admin.Branches'][] = [
            'id' => ++$tmpCount,
            'name' => $branch['name'] ?? '-',
            'street' => $branch['street'] ?? '-',
            'street2' => $branch['street2'] ?? null,
            'city' => $branch['city'] ?? '-',
            'post_code' => $branch['post_code'] ?? '-',
            'country_id' => $branch['country_id'] ?? null,
            'state_id' => $branch['state_id'] ?? null,
            'description' => $branch['description'] ?? null,
            'email' => $branch['email'] ?? 'rshop@rshop.sk',
            'phone' => $branch['phone'] ?? null,
            'phone2' => $branch['phone2'] ?? null,
            'longitude' => $branch['longitude'] ?? null,
            'latitude' => $branch['latitude'] ?? null,
            'heureka_id' => $branch['heureka_id'] ?? null,
            'parking' => $branch['parking'] ?? null,
            'payment' => $branch['payment'] ?? null,
            'video' => $branch['video'] ?? null,
            'lunch_break' => $branch['lunch_break'] ?? null,
            'map_link' => $branch['map_link'] ?? null,
            'map_image' => $branch['map_image'] ?? null,
            'display' => $branch['display'] ?? 1,
            'is_active' => $branch['is_active'] ?? 1,
            'is_pickup' => $branch['is_pickup'] ?? 0,
            'sort' => $tmpCount,
        ];
    }
}

if ($options['sections'] && $sections) {
    $tmpCount = 0;

    $data['Rshop/Admin.Sections'] = [
        '_defaults' => [],
    ];

    foreach ($sections as $sectionId => $section) {
        $data['Rshop/Admin.Sections'][] = [
            'id' => $sectionId,
            'name' => $section['name'] ?? '',
            'parent_id' => $section['parent_id'] ?? null,
            'active' => $section['active'] ?? 1,
            'display' => $section['display'] ?? 1,
            'sort' => ++$tmpCount,
        ];
    }
}

if ($options['articles'] && $articles) {
    $tmpCount = 0;

    $data['Rshop/Admin.Articles'] = [
        '_defaults' => [],
    ];

    foreach ($articles as $articleId => $article) {
        $data['Rshop/Admin.Articles'][] = [
            'id' => $articleId,
            'name' => $article['name'] ?? '',
            'section_id' => $article['section_id'] ?? null,
            'active' => $article['active'] ?? 1,
            'short_description' => $article['short_description'] ?? '',
            'description' => $article['description'] ?? '',
            'sort' => ++$tmpCount,
        ];
    }
}

if ($options['boxesData'] && \count($boxes) > 0) {
    $tmpCount = 0;
    $tmpSubCount = 0;

    $data['Rshop/Admin.BoxItems'] = [
        '_defaults' => [],
    ];
    $data['Rshop/Admin.BoxSubitems'] = [
        '_defaults' => [],
    ];

    foreach ($boxes as $boxId => $boxItems) {
        foreach ($boxItems as $item) {
            $data['Rshop/Admin.BoxItems'][] = [
                'id' => ++$tmpCount,
                'box_id' => $boxId,
                'name' => $item['name'] ?? '',
                'url' => $item['url'] ?? null,
                'has_subitems' => isset($item['subitems']) ? 1 : 0,
                'subitems_type' => $item['subitems_type'] ?? null,
                'product_id' => $item['product_id'] ?? null,
                'category_id' => $item['category_id'] ?? null,
                'article_id' => $item['article_id'] ?? null,
                'section_id' => $item['section_id'] ?? null,
                'manufacturer_id' => $item['manufacturer_id'] ?? null,
                'brand_id' => $item['brand_id'] ?? null,
                'campaign_id' => $item['campaign_id'] ?? null,
                'benefit_id' => $item['benefit_id'] ?? null,
                'present_id' => $item['present_id'] ?? null,
                'tag_id' => $item['tag_id'] ?? null,
                'testimonial_id' => $item['testimonial_id'] ?? null,
                'sort' => $tmpCount,
                'active' => 1,
            ];

            if (isset($item['subitems']) && $item['subitems']) {
                foreach ($item['subitems'] as $subItem) {
                    $data['Rshop/Admin.BoxSubitems'][] = [
                        'id' => ++$tmpSubCount,
                        'box_item_id' => $tmpCount,
                        'name' => $subItem['name'] ?? '',
                        'url' => $subItem['url'] ?? null,
                        'product_id' => $subItem['product_id'] ?? null,
                        'category_id' => $subItem['category_id'] ?? null,
                        'article_id' => $subItem['article_id'] ?? null,
                        'section_id' => $subItem['section_id'] ?? null,
                        'manufacturer_id' => $subItem['manufacturer_id'] ?? null,
                        'brand_id' => $subItem['brand_id'] ?? null,
                        'campaign_id' => $subItem['campaign_id'] ?? null,
                        'benefit_id' => $subItem['benefit_id'] ?? null,
                        'present_id' => $subItem['present_id'] ?? null,
                        'tag_id' => $subItem['tag_id'] ?? null,
                        'testimonial_id' => $subItem['testimonial_id'] ?? null,
                        'sort' => $tmpSubCount,
                        'active' => 1,
                    ];
                }
            }
        }
    }
}

$this->importTables($data);