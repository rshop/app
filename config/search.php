<?php
/**
 * This file is part of rshop/toplac project.
 *
 * (c) RIESENIA.com
 */

$config = require __DIR__ . '/../vendor/rshop/rshop/config/search.php';

return $config;