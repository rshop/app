# RSHOP Application Skeleton

A skeleton for creating RSHOP applications.

## Installation

Run `composer create-project --prefer-dist rshop/app [app_name]`.
