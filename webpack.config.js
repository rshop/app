const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const ASSET_PATH = '/js/';

module.exports = (env, argv) => ({
    entry: {
        'p-default': './resources/js/pages/default.js',
        'p-index': './resources/js/pages/index.js',
        'p-catalog': './resources/js/pages/catalog/catalog.js',
        'p-product': './resources/js/pages/product.js',
        'p-brands': './resources/js/pages/product.js',
        'p-contact': './resources/js/pages/contact.js',
        'p-account': './resources/js/pages/account.js',
        'p-articles': './resources/js/pages/articles.js',
        'p-article': './resources/js/pages/article.js',
        'p-compare': './resources/js/pages/compare.js',
        'p-404': './resources/js/pages/404.js',
        'p-shopping-cart': './resources/js/pages/shopping-cart/shopping-cart.js',
        'p-shopping-form': './resources/js/pages/shopping-form/shopping-form.js',
        'p-shopping-modules': './resources/js/pages/shopping-modules/shopping-modules.js',
        'p-shopping-success': './resources/js/pages/shopping/shopping-success.js',
    },
    output: {
        publicPath: ASSET_PATH,
        filename: '[name].min.js',
        chunkFilename: '[name].[contenthash].min.js',
        path: path.resolve(__dirname, './webroot/js')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                resourceQuery: /blockType=i18n/,
                type: 'javascript/auto',
                loader: '@kazupon/vue-i18n-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new webpack.SourceMapDevToolPlugin({
            publicPath: ASSET_PATH,
            filename: '[name].min.js.map'
        }),
        new CleanWebpackPlugin({
            verbose: true,
            cleanOnceBeforeBuildPatterns: ['*.*.min.js'],
        }),
    ],
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            vue: 'vue/dist/vue.js',
            Layouts: path.resolve(__dirname, 'resources/js/layouts'),
            Common: path.resolve(__dirname, 'resources/js/common'),
            Components: path.resolve(__dirname, 'resources/js/components'),
            VueComponents: path.resolve(__dirname, 'resources/js/components/vue'),
            Pages: path.resolve(__dirname, 'resources/js/pages'),
            Ui: path.resolve(__dirname, 'resources/js/ui'),
            Functions: path.resolve(__dirname, 'resources/js/functions'),
            Lib: path.resolve(__dirname, 'resources/js/lib'),
            Config: path.resolve(__dirname, 'resources/js/config'),
            Locale: path.resolve(__dirname, 'resources/js/locale'),
            Frontend: path.resolve(__dirname, 'vendor/rshop/frontend'),
            FrontendJsAssets: 'Frontend/assets/js',
            FrontendComponents: 'FrontendJsAssets/components',
        },
        modules: ['node_modules']
    },
    externals: {
        jquery: 'jQuery'
    },
    watch: argv.mode === 'development',
    mode: argv.mode === 'development' ? 'development' : 'production',
})
