import gracefulFs from 'graceful-fs';
import mapStream from 'map-stream';

export const updateTimestamp = function (options) {
    return mapStream(function (file, cb) {
        if (file.isNull()) {
            return cb(null, file);
        }

        // Update file modification and access time
        return gracefulFs.utimes(file.path, new Date(), new Date(), cb.bind(null, null, file));
    });
};
