import { src, dest, parallel, lastRun } from 'gulp';
import { dirs, config, DEV, PAGE } from './_config';
import { updateTimestamp } from './_helpers';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import livereload from 'gulp-livereload';
import dependents from 'gulp-dependents';
import gulpif from 'gulp-if';

const sass = gulpSass( dartSass );

// css for third party libraries
function lib(done) {
    if (config.lib.css && config.lib.css.length > 0) {
        src(config.lib.css, {
            sourcemaps: true,
            allowEmpty: true
        })
            .pipe(concat('styles-lib.css'))
            .pipe(postcss([
                autoprefixer(),
                cssnano()
            ]))
            .pipe(dest(dirs.webroot.css, { sourcemaps: '.' }))
            .pipe(updateTimestamp())
            .pipe(gulpif(DEV, livereload()));
    }

    done();
}

// css for base/common styles
function common(done) {
    src([`${dirs.resources.css}common/import.scss`], {
        sourcemaps: true,
    })
        .pipe(gulpif(DEV, dependents()))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer(),
            cssnano()
        ]))
        .pipe(rename('styles-common.css'))
        .pipe(dest(dirs.webroot.css, { sourcemaps: '.' }))
        .pipe(updateTimestamp())
        .pipe(gulpif(DEV, livereload()));

    done();
}

// css for project's layouts
function layouts(done) {
    const cssLayouts = config.layouts.map(layout => {
        function tmpCssLayouts() {
            return src([`${dirs.resources.css}layouts/${layout}/import.scss`], {
                sourcemaps: true,
                allowEmpty: true,
            })
                .pipe(gulpif(DEV, dependents()))
                .pipe(sass.sync().on('error', sass.logError))
                .pipe(postcss([
                    autoprefixer(),
                    cssnano()
                ]))
                .pipe(rename(`styles-layout-${layout}.css`))
                .pipe(dest(dirs.webroot.css, { sourcemaps: '.' }))
                .pipe(updateTimestamp())
                .pipe(gulpif(DEV, livereload()));
        }

        tmpCssLayouts.displayName = `css-${layout}-layout`;
        return tmpCssLayouts;
    });

    return parallel(...cssLayouts, (parallelDone) => {
        parallelDone();
        done();
    })();
}

// css per page + page dependencies
function pages(done) {
    let cssDependenciesArray = config.tasks.filter(task => task.hasOwnProperty('cssDependencies') && task.cssDependencies.length > 0 && task.active);

    if (PAGE && PAGE != '') {
        cssDependenciesArray = cssDependenciesArray.filter(task => task.name.includes(PAGE));
    }

    const cssDependencies = cssDependenciesArray.map(task => {
        function tmpCssDependencies() {
            return src(task.cssDependencies, {
                sourcemaps: true,
                allowEmpty: true,
            })
                .pipe(gulpif(DEV, dependents()))
                .pipe(concat(`styles-${task.name}-dependencies.css`))
                .pipe(postcss([
                    autoprefixer(),
                    cssnano()
                ]))
                .pipe(dest(dirs.webroot.css, { sourcemaps: '.' }))
                .pipe(updateTimestamp())
                .pipe(gulpif(DEV, livereload()));
        }

        tmpCssDependencies.displayName = `css-${task.name}-dependencies`;
        return tmpCssDependencies;
    });

    let cssTasksArray = config.tasks.filter(task => task.active);

    if (PAGE && PAGE != '') {
        cssTasksArray = cssTasksArray.filter(task => task.name.includes(PAGE));
    }

    const cssTasks = cssTasksArray.map(task => {
        function tmpCss() {
            return src(`${dirs.resources.css}pages/${task.name}/import.scss`, {
                sourcemaps: true,
            })
                .pipe(gulpif(DEV, dependents()))
                .pipe(sass.sync().on('error', sass.logError))
                .pipe(postcss([
                    autoprefixer(),
                    cssnano()
                ]))
                .pipe(rename(`styles-${task.name}.css`))
                .pipe(dest(dirs.webroot.css, { sourcemaps: '.' }))
                .pipe(updateTimestamp())
                .pipe(gulpif(DEV, livereload()));
        }

        tmpCss.displayName = `css-${task.name}`;
        return tmpCss;
    });

    return parallel(...cssDependencies, ...cssTasks, (parallelDone) => {
        parallelDone();
        done();
    })();
}

export { lib, common, layouts, pages };
