import { watch, series, parallel } from 'gulp';
import { dirs } from './_config';

import livereload from 'gulp-livereload';

import * as styles from './_styles';
import { copy } from './_copy';
import * as scripts from './_scripts';

const css = parallel(styles.lib, styles.common, styles.layouts, styles.pages);
const js = parallel(scripts.lib, scripts.frontend, scripts.build);
const build = series(copy, js, css);

const watchTask = series(() => {
    livereload.listen();

    // lib
    watch([dirs.lib.css], series(styles.lib));

    // common
    watch([`${dirs.resources.css}common/**/*.scss`], series(styles.common));

    // layouts
    watch([
        `${dirs.resources.css}layouts/**/*.scss`,
        `${dirs.resources.css}plugins/**/*.scss`
    ], series(styles.layouts));

    // pages
    watch([`${dirs.resources.css}pages/**/*.scss`], series(styles.pages));

    // init + components
    watch([
        `${dirs.resources.css}init/**/*.scss`,
        `${dirs.resources.css}components/**/*.scss`
    ], series(styles.common, styles.layouts, styles.pages));

    watch([
        `${dirs.resources.js}**/*-jquery.js`,
        `${dirs.resources.js}frontend/*.js`,
    ], js);
});

export default watchTask;
export { build, css, js, copy };
export const update = build;
export const install = build;