import { src, dest, series } from 'gulp';
import { dirs, DEV } from './_config';
import { updateTimestamp } from './_helpers';

import concat from 'gulp-concat';
import replace from 'gulp-replace';
import rename from 'gulp-rename';
import header from 'gulp-header';

const icomoon1 = () => {
    return src([`${dirs.resources.fonts}icomoon/variables.scss`])
        .pipe(replace(/\"\\(.*)"/g, (match, p1, offset, string) => {
            return match.replace(/"/g, '');
        }))
        .pipe(replace('$icomoon-font-family: "icomoon" !default;', ''))
        .pipe(replace('$icomoon-font-path: "fonts" !default;', ''))
        .pipe(rename('_icomoon.scss'))
        .pipe(dest(`${dirs.resources.css}init/`))
        .pipe(updateTimestamp());
};
const icomoon2 = () => {
    return src([`${dirs.resources.fonts}icomoon/variables.scss`, `${dirs.resources.fonts}icomoon/style.scss`])
        .pipe(concat('_font_icomoon.scss'))
        .pipe(header('@use "../init" as *;'))
        .pipe(replace('@import "variables";', ''))
        .pipe(replace(/content: (.*);/g, (match, p1, offset, string) => {
            return `content: unicode(${p1});`;
        }))
        .pipe(replace(/\"\\(.*)"/g, (match, p1, offset, string) => {
            return match.replace(/"/g, '');
        }))
        .pipe(dest(`${dirs.resources.css}common/`))
        .pipe(updateTimestamp());
}

const copyToWebroot = (done) => {
    src([`${dirs.resources.fonts}**/*`])
        .pipe(dest(dirs.webroot.fonts))
        .pipe(updateTimestamp());

    src([`${dirs.lib.js}**/*`])
        .pipe(dest(`${dirs.webroot.js}/lib`));

    src([`${dirs.lib.css}**/*`])
        .pipe(dest(`${dirs.webroot.css}/lib`));

    src([`${dirs.resources.images}**/*`])
        .pipe(dest(dirs.webroot.images));

    src([`${dirs.resources.root}*.ico`])
        .pipe(dest(dirs.webroot.root));

    src([`${dirs.resources.root}*.csv`])
        .pipe(dest(dirs.webroot.root));

    done();
};

export const copy = DEV ? series(icomoon1, icomoon2, copyToWebroot) : copyToWebroot;
