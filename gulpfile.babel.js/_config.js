import yargs from 'yargs';

export const DEV = yargs.argv.dev || yargs.argv.d;
export const PAGE = yargs.argv.page || yargs.argv.p;

export const dirs = {
    /**
     * paths to third party libraries
     */
    lib: {
        css: './resources/css/lib/',
        js: './resources/js/lib/'
    },
    /**
     * path to project's resources
     */
    resources: {
        root: './resources/',
        css: './resources/css/',
        js: './resources/js/',
        fonts: './resources/fonts/',
        images: './resources/img/',
        frontendAssets: './vendor/rshop/frontend/assets/',
        frontend: './vendor/rshop/frontend/resources/',
        frontendCart: './vendor/rshop/frontend-cart/resources/',
    },
    /**
     * path to project's webroot
     */
    webroot: {
        root: './webroot',
        css: './webroot/css',
        js: './webroot/js',
        fonts: './webroot/fonts',
        images: './webroot/img'
    }
}

export const config = {
    /**
     * list of third party libraries
     *
     * object of strings:
     * - css: array (optional)
     * - js: array (optional)
     */
    lib: {
        css: [
            `./node_modules/tippy.js/dist/tippy.css`,
            `./node_modules/tippy.js/animations/shift-away.css`,
        ],
        js: [
            `./node_modules/jquery/dist/jquery.js`,
            `./node_modules/jquery-validation/dist/jquery.validate.js`,
            `./node_modules/underscore/underscore.js`,
            // `${dirs.resources.js}lib/bootstrap/affix.js`,
            // `${dirs.resources.js}lib/bootstrap/alert.js`,
            // `${dirs.resources.js}lib/bootstrap/button.js`,
            // `${dirs.resources.js}lib/bootstrap/carousel.js`,
            // `${dirs.resources.js}lib/bootstrap/collapse.js`,
            // `${dirs.resources.js}lib/bootstrap/dropdown.js`,
            `${dirs.resources.js}lib/bootstrap/modal.js`,
            // `${dirs.resources.js}lib/bootstrap/popover.js`,
            // `${dirs.resources.js}lib/bootstrap/scrollspy.js`,
            // `${dirs.resources.js}lib/bootstrap/tab.js`,
            // `${dirs.resources.js}lib/bootstrap/tooltip.js`,
            `${dirs.resources.js}lib/bootstrap/transition.js`,
            `./node_modules/bootstrap-notify/bootstrap-notify.js`,
        ]
    },

    frontend: {
        css: [

        ],
        js: [
            // prebite veci z jadra
            `${dirs.resources.js}lib/frontend/eshop.js`,
            `${dirs.resources.js}lib/frontend/format.js`,

            // veci z jadra
            `${dirs.resources.frontend}js/event.js`,
            `${dirs.resources.frontend}js/cart.js`,
            `${dirs.resources.frontend}js/compare.js`,
            `${dirs.resources.frontend}js/tracking.js`,
            `${dirs.resources.frontend}js/default.js`,
            `${dirs.resources.frontend}js/form.js`,
            `${dirs.resources.frontend}js/url.js`,
            `${dirs.resources.frontend}js/validate.js`,
            `${dirs.resources.frontend}js/ui/ui_spinner.js`,

            // custom veci prebijania jadra
            `${dirs.resources.js}lib/frontend/general.js`,

            // `${dirs.resources.frontend}js/general/*.js`,
            // `${dirs.resources.frontend}js/ui/_init.js`,
            // `${dirs.resources.frontend}js/ui/ui_accordion.js`,
            // `${dirs.resources.frontend}js/ui/ui_dropdown.js`,
            // `${dirs.resources.frontend}js/ui/ui_tabs.js`,
            // `${dirs.resources.frontendCart}../frontend-cart/resources/components/**/*.js`,
        ]
    },
    /**
     * list of project's layouts
     *
     * array of strings
     */
    layouts: [
        'default',
        'shopping'
    ],
    /**
     * list of tasks for css/js build
     *
     * array of objects:
     * - name: string (required)
     * - cssDependencies: array (optional)
     * - jsFrontend: array (optional)
     * - jsDependencies: array (optional)
     */
    tasks: [
        {
            name: 'default',
            active: true
        },
        {
            name: 'index',
            active: true
        },
        {
            name: 'catalog',
            cssDependencies: [
                './node_modules/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
                './node_modules/nouislider/dist/nouislider.css'
            ],
            jsFrontend: [
                `${dirs.resources.frontend}js/listing.js`,
                `${dirs.resources.frontend}js/filter.js`,
            ],
            jsDependencies: [
                './node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
            ],
            active: true
        },
        {
            name: 'brands',
            active: true
        },
        {
            name: 'product',
            active: true
        },
        {
            name: 'contact',
            active: true
        },
        {
            name: 'shopping-cart',
            active: true
        },
        {
            name: 'shopping-form',
            active: true
        },
        {
            name: 'shopping-modules',
            active: true
        },
        {
            name: 'shopping-success',
            active: true
        },
        {
            name: 'account',
            jsFrontend: [
                `${dirs.resources.frontend}js/ui/ui_dropdown.js`,
                `${dirs.resources.frontend}js/listing.js`,
                `${dirs.resources.frontend}js/filter.js`,
            ],
            active: true
        },
        {
            name: 'articles',
            jsFrontend: [
                `${dirs.resources.frontend}js/listing.js`
            ],
            active: true
        },
        {
            name: 'article',
            active: true
        },
        {
            name: 'compare',
            jsDependencies: [
                `${dirs.lib.js}other/syncscroll.js`
            ],
            active: false
        },
        {
            name: '404',
            active: false
        },
        {
            name: 'admin',
            active: false
        }
    ]
};
