import { src, dest, parallel, lastRun } from 'gulp';
import { dirs, config, DEV } from './_config';
import { updateTimestamp } from './_helpers';

import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import babel from 'gulp-babel';
import livereload from 'gulp-livereload';
import dependents from 'gulp-dependents';
import gulpif from 'gulp-if';

export function lib(done) {
    if (config.lib.js && config.lib.js.length > 0) {
        src(config.lib.js, {
            sourcemaps: DEV,
            allowEmpty: true
        })
            .pipe(concat('scripts-lib.min.js'))
            .pipe(uglify())
            .pipe(dest(dirs.webroot.js, { sourcemaps: (DEV ? './source-maps/' : false) }))
            .pipe(updateTimestamp())
            .pipe(gulpif(DEV, livereload()));
    }

    done();
}

export function frontend(done) {
    if (config.frontend.js && config.frontend.js.length > 0) {
        src(config.frontend.js, {
            sourcemaps: DEV,
            allowEmpty: true
        })
            .pipe(concat('scripts-frontend.min.js'))
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
            .pipe(dest(dirs.webroot.js, { sourcemaps: (DEV ? './source-maps/' : false) }))
            .pipe(updateTimestamp())
            .pipe(gulpif(DEV, livereload()));
    }

    done()
}

export function build(done) {
    const jsDependenciesArray = config.tasks.filter(task => task.hasOwnProperty('jsDependencies'));
    const jsFrontendArray = config.tasks.filter(task => task.hasOwnProperty('jsFrontend'));

    const jsDependencies = jsDependenciesArray.map(task => {
        function tmpJsDependencies() {
            return src(task.jsDependencies, {
                sourcemaps: DEV,
                since: (DEV ? lastRun(build) : false)
            })
                .pipe(gulpif(DEV, dependents()))
                .pipe(concat(`scripts-${task.name}-dependencies.min.js`))
                .pipe(uglify())
                .pipe(dest(dirs.webroot.js, { sourcemaps: (DEV ? './source-maps/' : false) }))
                .pipe(updateTimestamp())
                .pipe(gulpif(DEV, livereload()));
        }

        tmpJsDependencies.displayName = `js-${task.name}-dependencies`;
        return tmpJsDependencies;
    });

    const jsFrontend = jsFrontendArray.map(task => {
        function tmpjsFrontend() {
            return src(task.jsFrontend, {
                sourcemaps: DEV,
                since: (DEV ? lastRun(build) : false)
            })
                .pipe(gulpif(DEV, dependents()))
                .pipe(concat(`scripts-${task.name}-frontend.min.js`))
                .pipe(babel({
                    presets: ['@babel/env']
                }))
                .pipe(uglify())
                .pipe(dest(dirs.webroot.js, { sourcemaps: (DEV ? './source-maps/' : false) }))
                .pipe(updateTimestamp())
                .pipe(gulpif(DEV, livereload()));
        }

        tmpjsFrontend.displayName = `js-${task.name}-frontend`;
        return tmpjsFrontend;
    });

    return parallel(...jsDependencies, ...jsFrontend, (parallelDone) => {
        parallelDone();
        done();
    })();
}
