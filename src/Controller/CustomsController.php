<?php
namespace App\Controller;

use Cake\Event\Event;
use Rshop\Frontend\Controller\AppController;
use Rshop\Frontend\Routing\RshopClassChecker;

/**
 * Project custom actions controller.
 */
class CustomsController extends AppController
{
    public function initialize()
    {
        $this->plugin = 'Rshop/Frontend';
        $this->request->params['plugin'] = 'Rshop/Frontend';

        $this->loadComponent(RshopClassChecker::component('Listing'), [
            'actions' => ['customerOrderedProducts']
        ]);

        parent::initialize();
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['seenProducts', 'modalWatchdog']);
        $this->Security->config('unlockedActions', ['seenProducts', 'customerOrderedProducts', 'modalWatchdog']);

        parent::beforeFilter($event);
    }

    /**
     * Get last seen products.
     */
    public function seenProducts()
    {
        if (!$this->request->is('ajax') || !$this->request->is(['post', 'put'])) {
            return $this->redirect('/404');
        }

        $productIds = $this->getRequest()->getData('product_ids');
        $products = [];

        if ($productIds) {
            $products = $this->loadModel('Products')->find()
                ->where(['Products.id IN' => $productIds])
                ->limit(3)
                ->toArray();
        }

        $this->viewBuilder()->setLayout(false);
        $this->viewBuilder()->setTemplate('/Customs/seen_products');
        $this->viewBuilder()->setClassName('\Rshop\Frontend\View\FrontendView');

        $this->set('products', $products);
    }

    public function customerOrderedProducts()
    {
        $this->loadModel('Products');
        $this->loadModel('OrderProducts');

        // load category products
        $products = $this->Products->find('active')
            ->autoFields(true)
            ->select(['order_document_number' => 'Orders.document_number'])
            ->join([
                'OrderProducts' => [
                    'type' => 'INNER',
                    'table' => $this->OrderProducts->table(),
                    'conditions' => ['OrderProducts.product_id = Products.id']
                ],
                'Orders' => [
                    'type' => 'INNER',
                    'table' => $this->OrderProducts->Orders->table(),
                    'conditions' => ['OrderProducts.order_id = Orders.id', 'Orders.customer_id' => $this->Auth->user('customer_id'), 'Orders.customer_login_id' => $this->Auth->user('id')]
                ]
            ])
            ->order(['Orders.created' => 'DESC'])
            ->group('Products.id');

        // listing add query
        $this->Listing->setQuery($products, [], ['disabledElastic' => true]);
    }

    public function modalWatchdog($id)
    {
        $this->viewBuilder()->layout(false);
        $this->set('product', $this->loadModel('Products')->get($id));
    }
}
