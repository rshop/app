<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

namespace App\Controller;

use Rshop\FrontendCart\Controller\CartController as RshopCartController;

class CartController extends RshopCartController
{
    public function initialize()
    {
        $this->plugin = 'Rshop/FrontendCart';
        $this->request->params['plugin'] = 'Rshop/FrontendCart';

        parent::initialize();
    }
}
