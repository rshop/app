<?php
/**
 * This file is part of rshop/toplac project.
 *
 * (c) RIESENIA.com
 */

namespace App\Controller;

use Rshop\Frontend\Controller\UsersController as RshopUsersController;

class UsersController extends RshopUsersController
{
    public function initialize()
    {
        $this->plugin = 'Rshop/Frontend';
        $this->request->params['plugin'] = 'Rshop/Frontend';

        parent::initialize();
    }
}
