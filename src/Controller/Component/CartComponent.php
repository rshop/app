<?php

namespace App\Controller\Component;

use Rshop\Frontend\Controller\Component\CartComponent as BaseCartComponent;

class CartComponent extends BaseCartComponent
{
    public function _fetchTotals()
    {
        $data = parent::_fetchTotals();

        $data['rounding'] = $this->cart()->getRoundingAmount('~')->asFloat();
        $data['voucher'] = $this->cart()->getTotal('voucher')->asFloat();
        $data['savings'] = $this->cart()->getSavings()->asFloat();

        return $data;
    }
}