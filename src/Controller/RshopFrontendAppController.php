<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Rshop\Admin\Routing\RshopClassChecker;

/**
 * Base Controller for Rshop/Frontend plugin
 */
class RshopFrontendAppController extends Controller
{
    public function initialize()
    {
        $isLocal = isset($_SERVER['SERVER_NAME']) && \in_array($_SERVER['SERVER_NAME'], ['dev-app.sk', '10.10.53.27', 'localhost']);
        $isOurIp = isset($_SERVER['REMOTE_ADDR']) && \in_array($_SERVER['REMOTE_ADDR'], ['82.119.114.18']);

        if (!$isLocal && !$isOurIp) {
            // $this->redirect('/admin');
        }
    }

    public function customAssignFavoriteProductsCount(): int
    {
        $count = $this->getFavoriteProductsCount() ?? 0;

        $this->getRequest()->getSession()->write('custom_favorite_products_count', $count);

        return $count;
    }

    protected function getFavoriteProductsCount(): int
    {
        $favoriteWhere = [];

        if ($this->Auth->user()) {
            $favoriteWhere['customer_id'] = $this->Auth->info('id');
        } elseif ($this->request->getCookie('favoriteProducts')) {
            $favoriteWhere['cookie'] = $this->request->getCookie('favoriteProducts');
        }

        if ($favoriteWhere) {
            $customFavoriteProductsCount = RshopClassChecker::getTable('CustomFavoriteProducts')->find()
                ->where($favoriteWhere)
                ->count();
        }

        return $customFavoriteProductsCount ?? 0;
    }
}
