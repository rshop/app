<?php
namespace App\Controller;

use Cake\Event\Event;
use Rshop\Frontend\Controller\AppController;
use Rshop\Frontend\Routing\RshopClassChecker;

class CustomFavoriteProductsController extends AppController
{
    public function initialize()
    {
        $this->loadComponent(RshopClassChecker::component('Listing'), [
            'actions' => ['listing'],
        ]);

        parent::initialize();
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['add', 'remove', 'listing']);
        $this->Security->config('unlockedActions', ['add', 'remove', 'listing']);

        parent::beforeFilter($event);
    }

    public function add($productId)
    {
        $this->RequestHandler->renderAs($this, 'json');

        $result = [
            'status' => false,
            'message' => __('Niekde nastala chyba')
        ];

        if (!$this->request->is(['post', 'put'])) {
            return $this->redirect('/404');
        }

        // pocet
        $count = $this->getFavoriteProductsCount();

        $favoriteItem = $this->loadModel('CustomFavoriteProducts')->newEntity();

        $data = [
            'product_id' => $productId
        ];

        if ($this->Auth->user()) {
            $data['customer_id'] = $this->Auth->info('id');
        } else {
            $cookie = $this->request->getCookie('favoriteProducts');

            // set cookie
            if (!$cookie) {
                $cookie = \bin2hex(\random_bytes(20));

                $this->setResponse($this->getResponse()->withCookie('favoriteProducts', ['value' => $cookie, 'expire' => \strtotime('+1 year')]));
            }

            $data['cookie'] = $cookie;
        }

        $favoriteItem = $this->CustomFavoriteProducts->patchEntity($favoriteItem, $data);

        if ($this->CustomFavoriteProducts->exists($data)) {
            $result = [
                'status' => false,
                'message' => __('Produkt sa už nachádza v obľúbených')
            ];
        } elseif ($this->CustomFavoriteProducts->save($favoriteItem)) {
            $this->getRequest()->getSession()->write('custom_favorite_products_count', ++$count);

            $result = [
                'status' => true,
                'message' => __('Produkt bol úspešne pridaný do obľúbených'),
                'count' => $count
            ];
        }

        $this->set([
            'data' => $result,
            '_serialize' => 'data'
        ]);
    }

    public function remove($productId)
    {
        $this->RequestHandler->renderAs($this, 'json');

        $result = [
            'status' => false,
            'message' => __('Niekde nastala chyba'),
        ];

        if (!$this->request->is(['post', 'put'])) {
            return $this->redirect('/404');
        }

        $where = [
            'product_id' => $productId
        ];

        if ($this->Auth->user()) {
            $where['customer_id'] = $this->Auth->info('id');
        } elseif ($this->request->getCookie('favoriteProducts')) {
            $where['cookie'] = $this->request->getCookie('favoriteProducts');
        } else {
            return $this->redirect('/404');
        }

        $favoriteItem = $this->loadModel('CustomFavoriteProducts')->find()
            ->where($where)
            ->first();

        if (!$favoriteItem) {
            $result = [
                'status' => false,
                'message' => __('Produkt sa nenachádza v obľúbených')
            ];
        } elseif ($this->CustomFavoriteProducts->delete($favoriteItem)) {
            $result = [
                'status' => true,
                'message' => __('Produkt bol úspešne odstránený z obľúbených'),
                'count' => $this->customAssignFavoriteProductsCount()
            ];
        }

        $this->set([
            'data' => $result,
            '_serialize' => 'data'
        ]);
    }

    /**
     * Listing products.
     *
     * @return void
     */
    public function listing()
    {
        $this->loadModel('Products');

        // pokryjeme halusku po prihlaseni cez google
        if ($this->Auth->user() && $this->request->getCookie('favoriteProducts')) {
            $this->loadModel('CustomFavoriteProducts')->updateAll(['customer_id' => $this->Auth->info('id'), 'cookie' => ''], ['cookie' => $this->request->getCookie('favoriteProducts')]);
            $this->customAssignFavoriteProductsCount();
            $this->setResponse($this->getResponse()->withCookie('favoriteProducts', ['value' => '', 'expire' => \strtotime('-1 day')]));
        }

        if ($this->Auth->user()) {
            $where = ['CustomFavoriteProducts.customer_id' => $this->Auth->info('id')];
        } elseif ($this->request->getCookie('favoriteProducts')) {
            $where = ['CustomFavoriteProducts.cookie' => $this->request->getCookie('favoriteProducts')];
        } else {
            $where = ['1 = 2'];
        }

        $query = $this->loadModel('Products')->find('active')
            ->autoFields(true)
            ->contain(['ProductTypes', 'ProductImages', 'TaxClasses', 'ProductStatuses'])
            ->join([
                'CustomFavoriteProducts' => [
                    'type' => 'INNER',
                    'table' => $this->loadModel('CustomFavoriteProducts')->table(),
                    'conditions' => 'CustomFavoriteProducts.product_id = Products.id'
                ]
            ])
            ->where($where)
            ->group('Products.id');

        $this->Listing->setQuery($query, ['sort' => 'name'], ['disabledElastic' => true]);
    }
}
