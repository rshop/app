<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

namespace App\Model\Entity;

use Litipk\BigNumbers\Decimal;
use Rshop\Admin\Model\Entity\Product as RshopProduct;

class Product extends RshopProduct
{
    /**
     * Get savings for product.
     *
     * @return Decimal
     */
    public function getSavings(): Decimal
    {
        if (!$this->is_action) {
            return Decimal::fromInteger(0);
        }

        return Decimal::fromFloat($this->price_vat - $this->old_price_vat);
    }

    public function setCombiDealParent(?Product $product)
    {
        $this->_combiDealParent = $product;
    }

    public function getCombiDealParent(): ?Product
    {
        return $this->_combiDealParent;
    }
}
