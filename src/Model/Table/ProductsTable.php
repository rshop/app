<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

namespace App\Model\Table;

use Rshop\Admin\Model\Table\ProductsTable as RshopProductsTable;

class ProductsTable extends RshopProductsTable
{
}
