<?php
namespace App\Model\Table;

use Rshop\Admin\Model\Table\AppTable;

/**
 * CustomFavoriteProducts Model.
 */
class CustomFavoriteProductsTable extends AppTable
{
    /**
     * Initialize method.
     *
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');

        // add CounterCache behavior
        $this->addBehavior('CounterCache', [
            'Products' => [
                'custom_favorite_sum' => function ($event, $entity, $table) {
                    $query = $this->find();
                    $favorite = $query
                        ->select(['favorite_sum' => $query->func()->count('CustomFavoriteProducts.id')])
                        ->where(['product_id' => $entity->product_id])
                        ->toArray();

                    return $favorite[0]->favorite_sum;
                }
            ]
        ]);

        $this->belongsTo('Products');
    }
}
