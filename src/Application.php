<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App;

use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Http\BaseApplication;
use Cake\Routing\Middleware\RoutingMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rshop\Consent\Middleware\ContentSecurityPolicyMiddleware;
use Rshop\Core\Error\Middleware\ErrorHandlerMiddleware;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication
{
    public function bootstrap()
    {
        // Call parent to load bootstrap from files.
        parent::bootstrap();

        // debug kit
        if (Configure::read('debug')) {
            Configure::write('DebugKit.forceEnable', true);
            $this->addPlugin(\DebugKit\Plugin::class);
        }

        // migrations
        if (PHP_SAPI === 'cli') {
            try {
                $this->addPlugin('Bake');
            } catch (MissingPluginException $e) {
                // Do not halt if the plugin is missing
            }
            $this->addPlugin('Migrations');
        }

        $this->addPlugin('Rshop', ['enable' => true]);

        Configure::load('rshop');
        Configure::load('frontend');
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware($middlewareQueue)
    {
        $middlewareQueue
            // catch any exceptions in the lower layers and make an error page/response
            ->add(ErrorHandlerMiddleware::class)

            // add routing middleware
            ->add(new RoutingMiddleware($this))

            // add content security policy
            ->add(ContentSecurityPolicyMiddleware::class);

        return $middlewareQueue;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        Configure::load('rshop');
        Configure::load('frontend');

        return parent::__invoke($request, $response, $next);
    }
}
