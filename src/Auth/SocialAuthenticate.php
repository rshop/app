<?php
/**
 * This file is part of rshop/biutli project.
 *
 * (c) RIESENIA.com
 */

namespace App\Auth;

use CakeDC\Users\Exception\UserNotActiveException;
use Rshop\Core\I18n\I18n;
use Rshop\Frontend\Auth\SocialAuthenticate as FrontendSocialAuthenticate;

class SocialAuthenticate extends FrontendSocialAuthenticate
{
    protected function _socialLogin($data)
    {
        $user = parent::_socialLogin($data);

        return $user;
    }
}
