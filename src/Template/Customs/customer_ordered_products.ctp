<?php
$this->extend('/Common/Layout/account');

$this->Listing->Paginator->templates([
    'wrapper' => '
        <nav class="pager">
            {{content}}
        </nav>
    ',
    'loadMore' => '
        <button class="c-btn c-btn--primary pager__load" title="' . __('Zobraziť ďalšie produkty') . '" data-url="{{url}}" data-page="{{page}}" data-max="{{pageCount}}" onclick="return Listing.fetchData(this);">
            <span>' . __('Zobraziť ďalšie produkty') . '</span>
        </button>
    ',
]);

// assign title
$this->assign('title', __('Zakúpený tovar'));
$this->Breadcrumb->addItem(__('Zakúpený tovar'));
?>

<div class="c-account-table">
    <div class="c-account-table__header xs-is-hidden cxs-is-hidden">
        <?= $this->element('Rshop/Frontend.Account/Tables/ordered_products_header'); ?>
    </div>

    <div class="c-account-table__body">
        <?= $this->Listing->render($listingData); ?>
    </div>
</div>