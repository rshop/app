<?php
if ($products) {
    ?>
    <div class="l-container">
        <div class="c-seen-products">
            <div class="c-seen-products__heading">
                <div class="c-seen-products__title">
                    <?= __('Už ste si prezerali'); ?>
                </div>
            </div>

            <div class="c-seen-products__wrapper">
                <?php
                foreach ($products as $product) {
                    if (!$product->active) {
                        continue;
                    } ?>
                <div class="c-seen-products__item">
                    <?= $this->element('Rshop/Frontend.Product/thumb_seen', [
                        'data' => $product
                    ]); ?>
                </div>
                <?php
                } ?>
            </div>
        </div>
    </div>
<?php
}
