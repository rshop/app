<?php
/**
 * This file is part of rshop/biutli project.
 *
 * (c) RIESENIA.com
 */

$this->extend('Rshop/Frontend.Common/Layout/listing');

// data
$this->set('dataset', [
    'title' => __('Obľúbené produkty'),
    'filter' => false,
]);

// settings
$this->set('settings', [
    'showSorts' => false,
    'showLimit' => false,
]);

echo $this->Listing->render($listingData);
