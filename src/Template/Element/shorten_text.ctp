<?php
/**
 * pouzitelne variabilne.
 *
 * $limit (int)
 * $fullText (string)
 * $showMoreText (string)
 * $showLessText (string)
 */

use Cake\Utility\Text;

if (!$fullText) {
    return '';
}

$shortText = null;
$showMoreText = $showMoreText ?? __('zobraziť viac');
$showLessText = $showLessText ?? __('zobraziť menej');

if (\mb_strlen($fullText) > $limit + 30) {
    $shortText = Text::truncate($fullText, $limit, [
        'exact' => false,
        'html' => true
    ]);
}
?>

<r-shorten class="is-shorten" data-more-text="<?= $showMoreText; ?>" data-less-text="<?= $showLessText; ?>">
    <?php
    if ($shortText) {
        ?>
    <div class="is-text" r-shorten-short>
        <?= $shortText; ?>
    </div>

    <div class="is-text hidden" r-shorten-full>
        <?= $fullText; ?>
    </div>

    <button class="c-link c-link--secondary" r-shorten-trigger>
        <span r-shorten-trigger-text><?= $showMoreText; ?></span>
        <i class="ico ico-chevron-down-bold" aria-hidden="true"></i>
    </button>
    <?php
    } else {
        ?>
    <div class="is-text">
        <?= $fullText; ?>
    </div>
    <?php
    }
    ?>
</r-shorten>