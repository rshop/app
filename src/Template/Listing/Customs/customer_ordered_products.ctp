<?php
$view = 'orderedRow';
$url = ['controller' => 'Customs', 'action' => 'customerOrderedProducts', 'plugin' => null, '?' => $this->request->query];

$this->Listing->Paginator->templates([
    'loadMore' => '
        <button class="c-btn c-btn--primary pager__load" title="' . __('Zobraziť ďalšie produkty') . '" data-url="{{url}}" data-page="{{page}}" data-max="{{pageCount}}" onclick="return Listing.fetchData(this);">
            <span>' . __('Zobraziť ďalšie produkty') . '</span>
        </button>
    ',
]);

$this->Listing->create('products', [
    'emptyMessage' => __('Ešte ste nekúpili žiaden tovar')
])
    ->setTemplate($view, $this->Configure->read('Product.views.' . $view))
    ->setLocalData($listingData)
    ->setAjaxUrl($this->Url->build($url));

echo $this->Listing;
