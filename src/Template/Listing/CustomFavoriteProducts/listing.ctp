<?php
/**
 * This file is part of rshop/biutli project.
 *
 * (c) RIESENIA.com
 */

$url = ['controller' => 'CustomFavoriteProducts', 'action' => 'listing', 'plugin' => null, '?' => $this->request->query];

$this->Listing->create('products', [
    'emptyMessage' => __('V obľúbených nemáte žiadny produkt')
])
    ->setLocalData($listingData)
    ->setAjaxUrl($this->Url->build($url));

echo $this->Listing;
