<?php
$this->assign('title', __('Zoznam značiek'));
$this->Breadcrumb->addItem(__('Značky'));

$hasFilter = $this->Configure->read('Marks.hasFilter.Manufacturers');
?>

<div class="l-container">
    <div class="c-brands-listing">
        <div class="c-brands-listing__header" id="brands-holder">
            <h1 class="c-brands-listing__title"><?= __('Zoznam značiek'); ?></h1>
            <?php
            if ($hasFilter) {
                ?>
            <div class="c-brands-listing__alphabet">
                <?php
                $firstLetterOld = null;

                foreach ($listingData as $manufacturer) {
                    $firstLetter = $this->Format->getFirstLetter($manufacturer->name);

                    if ($firstLetter !== $firstLetterOld) {
                        echo $this->Html->tag('a', $firstLetter, [
                            'href' => '#brand-' . $firstLetter,
                            'class' => 'c-brands-listing__link c-link',
                            'r-anchor' => 'brand-' . $firstLetter,
                            'escape' => false
                        ]);
                    }
                    $firstLetterOld = $firstLetter;
                } ?>
            </div>
            <?php
            }
            ?>
        </div>

        <?php
        $firstLetterOld = null;

        foreach ($listingData as $id => $manufacturer) {
            $firstLetter = $this->Format->getFirstLetter($manufacturer->name);

            if ($id == 0 || \strcasecmp($firstLetter, $firstLetterOld) != 0) {
                if ($hasFilter) {
                    if ($id != 0) {
                        echo '</div><div class="breaker"></div>';
                    } ?>
                    <div id="brand-<?= $firstLetter; ?>" class="c-brands-box has-filter">
                        <div class="c-brands-box__header">
                            <h2 class="c-brands-box__title"><?= $firstLetter; ?></h2>
                            <a href="#brands-holder" class="c-brands-box__anchor c-link" r-anchor="brands-holder">
                                <i class="ico ico-caret-up-thick" aria-hidden='true'></i>
                                <span><?= __('Hore'); ?></span>
                            </a>
                        </div>
                <?php
                } elseif ($id == 0) {
                    echo '<div class="c-brands-box">';
                }

                $firstLetterOld = $firstLetter;
            } ?>
            <div class="c-brands-box__item">
                <a class="c-link" href="<?= $manufacturer->full_link; ?>" title="<?= $manufacturer->name; ?>">
                    <div class="c-brands-box__item-img">
                        <?= $this->Html->lazyThumbnail($manufacturer->image, [
                            'alt' => h($manufacturer->name),
                            'format' => 'sm',
                            'retina' => 'md'
                        ]); ?>
                    </div>
                    <h3 class="c-brands-box__item-name"><?= $manufacturer->name; ?></h3>
                </a>
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>
