<?php
$this->extend('Rshop/Frontend.Common/Layout/listing');

// title
$this->assign('title', $manufacturer->name);

$this->assign('subcategories', $this->cell('Rshop/Frontend.Mark::categories', [$manufacturer->id]));

$this->set('settings', [
    'showManufacturersFilter' => false
]);

echo $this->Listing->render($listingData);
