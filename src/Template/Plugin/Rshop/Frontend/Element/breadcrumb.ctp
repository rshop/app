<div class="c-breadcrumb">
    <?php
    echo $this->Breadcrumb->create('breadcrumb', [
        'class' => 'breadcrumb',
    ])
        ->addItems($crumbs);
    ?>
</div>