<?php
$isBlog = $isBlog ?? false;
$isLarge = $isLarge ?? false;
$customThumbClass = $customThumbClass ?? '';

if (!$isBlog) {
    $customThumbClass .= ' has-border';
}

if ($isLarge) {
    $customThumbClass .= ' is-large';
}
?>

<div class="c-article-thumb <?= $customThumbClass; ?>">
    <?= $data->name; ?>
</div>