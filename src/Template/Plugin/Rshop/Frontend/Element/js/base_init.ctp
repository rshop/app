<?php
$defaultJsFile = 'default';
$urlKey = $this->Url->urlKey();
$urlKeyArray = \explode('.', $urlKey);

$this->start('head');
echo $this->Html->charset() . "\n";
echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">' . "\n";
echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">' . "\n";
echo $this->Seo;
echo $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) . "\n";

echo $this->fetch('assets_before');
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('libsCss');
echo $this->fetch('layoutCss');
echo $this->fetch('preload');
echo $this->fetch('pageCss');
echo $this->fetch('assets');
echo '<noscript><style>.noscript__hide { display: none; }</style></noscript>';

echo $this->fetch('headScripts');
$this->end();

echo $this->Html->scriptBlock('
    !function(d) {
        if(!d.currentScript){
            var s = d.createElement("script");
            s.src = "' . $this->Url->script('Rshop/Frontend.default-ie.js') . '";
            d.head.appendChild(s);
        }
    }(document)
', ['block' => null]);

echo $this->Html->scriptBlock('
    var _debug = ' . ($this->Configure->read('debug') ? 'true' : 'false') . ';
', ['block' => null]);

echo $this->fetch('libJs');

// set currency
$currency = $this->Format->currency();

if ($currency) {
    foreach ($currency as $key => $value) {
        if ($value === null) {
            $value = '';
        }
        $currency[$key] = $value;
    }
}

$countries = $this->modelHelper('Misc')->countries(false);
echo $this->Html->scriptBlock('
    Eshop.options.debug = _debug;
    Eshop.options.baseUrl = "' . $this->Url->build('/') . '";
    Eshop.options.fullUrl = "' . $this->Url->build('/', true) . '";
    Eshop.options.pricesWithVat = ' . ($this->Configure->read('Rshop.pricesWithVat') ? 'true' : 'false') . ';
    Eshop.options.currency = ' . \json_encode($currency ?: []) . ';
    Eshop.options.imageSrc = "' . $this->Html->thumbnailSrc('%s', ['format' => '%format', 'fullBase' => true]) . '";
    Eshop.options.countries = ' . \json_encode(\Cake\Utility\Hash::combine($countries ?: [], '{n}.id', '{n}')) . ';
', ['block' => null]);

$this->Components->generateTemplates();

echo $this->Html->scriptBlock('
    jQuery.validator.addMethod("exactlength", function(value, element, param) {
        return this.optional(element) || value.length == param;
    }, jQuery.validator.format("Please enter exactly {0} characters."));

    jQuery.validator.addMethod("phone", function(value, element) {
        return /^[0-9+][0-9 ]{7,17}$/.test(value) || value == "";
    }, jQuery.validator.format("Please enter valid phone number."));

    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.indexOf(" ") < 0;
    }, jQuery.validator.format("Space are not allowed"));

    jQuery.validator.addMethod("skVatNum", function(value, element) {
        return /^(SK)[0-9]{10}$/.test(value) || value == "";
    }, jQuery.validator.format("Please enter valid VAT number."));

    jQuery.validator.addMethod("addressNumber", function (value, element) {
        return /[^0-9 ].\d/.test(value.trim()) || value == "";
    }, jQuery.validator.format("Please enter address with house number."));

    jQuery.extend(jQuery.validator.messages, {
        required: "' . $this->Configure->read('Messages.Validation.required') . '",
        remote: "' . $this->Configure->read('Messages.Validation.remote') . '",
        email: "' . $this->Configure->read('Messages.Validation.email') . '",
        phone: "' . $this->Configure->read('Messages.Validation.phone') . '",
        url: "' . $this->Configure->read('Messages.Validation.url') . '",
        date: "' . $this->Configure->read('Messages.Validation.date') . '",
        dateISO: "' . $this->Configure->read('Messages.Validation.dateISO') . '",
        number: "' . $this->Configure->read('Messages.Validation.number') . '",
        digits: "' . $this->Configure->read('Messages.Validation.digits') . '",
        creditcard: "' . $this->Configure->read('Messages.Validation.creditcard') . '",
        equalTo: "' . $this->Configure->read('Messages.Validation.equalTo') . '",
        accept: "' . $this->Configure->read('Messages.Validation.accept') . '",
        maxlength: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.maxlength') . '"),
        minlength: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.minlength') . '"),
        rangelength: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.rangelength') . '"),
        range: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.range') . '"),
        max: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.max') . '"),
        min: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.min') . '"),
        exactlength: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.exactlength') . '"),
        noSpace: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.noSpace') . '"),
        digitsWithSpaces: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.digitsWithSpaces') . '"),
        skVatNum: jQuery.validator.format("' . $this->Configure->read('Messages.Validation.skVatNum') . '"),
        addressNumber: jQuery.validator.format("' . __('Vyplňte prosím názov a číslo ulice.') . '"),
    });
', ['ready' => true, 'data-cookieconsent' => 'ignore']);

if ($this->Configure->read('Api.GoogleCaptcha.enabled')) {
    $scriptUrl = 'https://www.google.com/recaptcha/api.js';

    if ($this->Configure->read('Api.GoogleCaptcha.v3.enabled')) {
        $scriptUrl .= '?render=' . $this->Configure->read('Api.GoogleCaptcha.v3.siteKey');

        $this->Html->scriptBlock('
        if ($("input[name=\"g-recaptcha-response\"]").size() > 0) {
            grecaptcha.ready(function() {
                Event.dispatch("Recaptcha.execute");
            });
        }

        Event.on("Recaptcha.execute", function() {
            $("input[name=\"g-recaptcha-response\"]").each(function() {
                var $input = $(this);

                grecaptcha.execute("' . $this->Configure->read('Api.GoogleCaptcha.v3.siteKey') . '", {action: "form"}).then(function(token) {
                    $input.val(token);
                });
            });
        });
        ', ['block' => true, 'ready' => true]);
    }

    echo $this->Html->script($scriptUrl, ['block' => false, 'defer']);
}
