<?php
$this->start('loginModalContent');
?>
<div class="c-modal__content">
    <?php
    echo $this->element('Rshop/Frontend.Form/login', ['isShopping' => $isShopping ?? false]);

    if (!($withoutRegistration ?? false)) {
    ?>
        <div class="login-register">
            <div class="login-register__title">
                <?= $this->Configure->text('modal.loginTitle'); ?>
            </div>

            <div class="login-register__text">
                <?= $this->Configure->text('modal.loginText'); ?>
            </div>

            <?= $this->Html->link(
                $this->Configure->text('modal.loginBtn'),
                ['urlKey' => 'Eshop.registration'],
                ['class' => 'c-btn']
            ); ?>

            <?php
            if ($this->Configure->read('Rshop.customers.clubDiscountEnabled') && $this->Configure->read('Custom.showClubDiscount')) {
            ?>
                <div class="login-register__savings">
                    <i class="ico ico-percentage"></i>
                    <span><?= __('A ušetrite {0} pri každom nákupe', '<span class="c-percent">' . $this->Format->percent($this->Configure->read('Rshop.customers.clubDiscountValue')) . '</span>'); ?></span>
                </div>
            <?php
            } ?>
        </div>
    <?php
    }
    ?>

    <div class="c-modal__success js-login-success">
        <i class="ico ico-tick"></i>
        <span class="sc-text"><?= __('Úspešne ste sa prihlásili.'); ?></span>
    </div>
</div>
<?php
$this->end();

echo $this->Modal->create('login-modal', [
    'name' => __('Prihláste sa'),
    'templateVars' => [
        'containerClass' => 'c-modal--login is-lg'
    ]
])
    ->setContent($this->fetch('loginModalContent'));
