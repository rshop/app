<?php
if ($this->Configure->read('Rshop.cookie.showMessage')) {
    ?>
<div class="c-cookie js-cookie-message">
    <div class="l-container">
        <div class="c-cookie__holder">
            <div class="is-text">
                <?= $this->Configure->read('Rshop.cookie.text') . ' ' . $this->Html->link($this->Configure->read('Rshop.cookie.linkText'), $this->Url->build(['urlKey' => 'Articles.ZoznamPouzivanychCookies']), ['class' => 'c-link']); ?>
            </div>

            <button class="c-btn c-btn--primary is-bold is-sm js-cookie-message-close">
                <?= $this->Configure->read('Rshop.cookie.buttonText'); ?>
            </button>
        </div>
    </div>
</div>
<?php
}
