<?php
use Cake\I18n\I18n;

$addressId = $addressId ?: 0;
$addressType = 'shipping';
$readOnly = false && I18n::getLocale() == 'en_US' && $this->request->params['controller'] == 'Steps' && $this->request->params['action'] == 'form';
?>

<div class="c-form__row">
    <?= $this->Form->input('customer_addresses.' . $addressId . '.first_name', [
        'placeholder' => false,
        'label' => __('Meno'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="first-name"',
        ],
    ]); ?>

    <?= $this->Form->input('customer_addresses.' . $addressId . '.last_name', [
        'placeholder' => false,
        'label' => __('Priezvisko'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="last-name"',
        ],
    ]); ?>
</div>

<div class="c-form__row">
    <?php
    echo $this->Form->input('customer_addresses.' . $addressId . '.company', [
        'placeholder' => false,
        'label' => __('Doplnenie adresy'),
        'templateVars' => [
            'holderClass' => 'data-type="company"',
            'inputNote' => '(' . __('názov firmy doručenia') . ')',
        ],
    ]);
    ?>

    <?= $this->Form->input('customer_addresses.' . $addressId . '.phone', [
        'placeholder' => false,
        'label' => __('Telefónne číslo (mobil)'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="phone"',
        ],
    ]); ?>
</div>

<?= $this->element('Rshop/Frontend.Form/address_autocomplete', [
    'type' => $addressType,
    'inputs' => [
        'address' => 'customer_addresses[' . $addressId . '][address]',
        'city' => 'customer_addresses[' . $addressId . '][city]',
        'post_code' => 'customer_addresses[' . $addressId . '][post_code]',
        'country_id' => 'customer_addresses[' . $addressId . '][country_id]',
    ],
]); ?>

<div class="c-form__row">
    <?= $this->Form->input('customer_addresses.' . $addressId . '.address', [
        'placeholder' => false,
        'label' => __('Názov a číslo ulice'),
        'required' => true,
        'readOnly' => $readOnly,
        'templateVars' => [
            'holderClass' => 'data-type="address"',
            'inputAttrs' => 'data-type="' . $addressType . '-address"',
        ],
    ]); ?>

    <?= $this->Form->input('customer_addresses.' . $addressId . '.post_code', [
        'placeholder' => '--- --',
        'label' => __('PSČ'),
        'required' => true,
        'readOnly' => $readOnly,
        'templateVars' => [
            'class' => $addressType . '-post-code',
            'inputAttrs' => 'data-type="' . $addressType . '-post-code"',
        ],
    ]); ?>
</div>

<div class="c-form__row">
    <?= $this->Form->input('customer_addresses.' . $addressId . '.city', [
        'placeholder' => false,
        'label' => __('Mesto'),
        'required' => true,
        'readOnly' => $readOnly,
        'templateVars' => [
            'holderClass' => 'data-type="city"',
            'inputAttrs' => 'data-type="' . $addressType . '-city"',
        ],
    ]); ?>

    <?= $this->Form->select('customer_addresses.' . $addressId . '.country_id', null, [
        'placeholder' => false,
        'label' => __('Krajina'),
        'required' => true,
        'autocomplete' => 'off',
        'value' => $this->request->getSession()->read('custom.deliveryCountryId') ?: null,
        'templateVars' => [
            'holderClass' => 'data-type="country"',
            'class' => 'js-country-input',
            'inputAttrs' => 'data-type="' . $addressType . '-country"',
        ],
    ]); ?>
</div>