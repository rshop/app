<?php
$entity = $entity ?? $customer;
$isOrderEntity = $entity instanceof \App\Model\Entity\Order;
$addressType = $isOrderEntity ? 'billing' : 'default';
$prefix = $isOrderEntity ? 'billing_' : '';
$showFinstatField = $showFinstatField ?? false;

if ($isOrderEntity) {
    $companyInputIsChecked = $entity->billing_org_num || $entity->billing_vat_num || $entity->billing_tax_num;
} else {
    $companyInputIsChecked = !isset($this->request->data['is_company']) ? $entity->is_company : @$this->request->data['is_company'];
}

$hideInputs = $this->Configure->read('Rshop.finstat.enabled') && !$companyInputIsChecked && $showFinstatField;
?>

<div class="c-form__box">
    <?= $this->Form->input('is_company', [
        'placeholder' => false,
        'label' => [
            'text' => __('Chcem nakúpiť na firmu/szčo') . ' <small>(' . __('získate benefity registrovanej firmy') . ')</small>',
            'escape' => false
        ],
        'data-box' => 'registration_user_box',
        'type' => 'checkbox',
        'checked' => $companyInputIsChecked,
        'default' => true,
        'custom' => true,
        'r-toggle-trigger' => 'company-group',
    ]); ?>
</div>

<r-toggle class="c-form__toggle <?= $companyInputIsChecked ? 'is-active' : ''; ?>" id="company-group">
    <?php
    if ($showFinstatField && $this->Configure->read('Rshop.finstat.enabled')) {
        echo $this->Form->input('custom_autocomplete_company', [
            'placeholder' => __('Začnite písať názov firmy, alebo IČO'),
            'required' => false,
            'label' => __('IČO, alebo názov firmy'),
            'data-autocomplete-field' => true
        ]);
    }
    ?>

    <div class="c-form__row js-b2b-reg-billing <?= $hideInputs ? 'is-hidden' : ''; ?>">
        <?= $this->Form->input($prefix . 'company', [
            'data-autocomplete-name' => 'company',
            'placeholder' => false,
            'autocomplete' => 'off',
            'label' => __('Názov firmy'),
            'templateVars' => [
                'holderClass' => 'data-type="company"',
            ],
        ]); ?>
    </div>

    <div class="c-form__row js-b2b-reg-billing <?= $hideInputs ? 'is-hidden' : ''; ?>">
        <?= $this->Form->input($prefix . 'org_num', [
            'data-autocomplete-name' => 'org_num',
            'required' => true,
            'placeholder' => false,
            'autocomplete' => 'off',
            'label' => __('IČO'),
            'templateVars' => [
                'holderClass' => 'data-type="num"',
            ],
        ]); ?>

        <?= $this->Form->input($prefix . 'tax_num', [
            'data-autocomplete-name' => 'tax_num',
            'required' => false,
            'placeholder' => false,
            'label' => __('DIČ'),
            'templateVars' => [
                'holderClass' => 'data-type="num"',
            ],
        ]); ?>

        <?= $this->Form->input($prefix . 'vat_num', [
            'data-autocomplete-name' => 'vat_num',
            'placeholder' => false,
            'label' => __('IČ DPH'),
            'templateVars' => [
                'holderClass' => 'data-type="num"',
            ],
        ]); ?>
    </div>
</r-toggle>
