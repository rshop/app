<?php

$isEdit = $isEdit ?? false;
$entity = $entity ?? $customer;
$isOrderEntity = $entity instanceof Rshop\Admin\Model\Entity\Order;
$addressType = $isOrderEntity ? 'billing' : 'default';
$showCompanyInputs = $showCompanyInputs ?? true;

if ($isEdit) {
?>
<div class="c-form__row">
    <?= $this->Form->input($isOrderEntity ? 'customer_email' : 'email', [
        'placeholder' => false,
        'required' => true,
        'label' => __('Emailová adresa'),
        'disabled' => true,
        'templateVars' => [
            'holderClass' => 'data-type="email"',
        ],
    ]); ?>
</div>
<?php
}
?>

<div class="c-form__row">
    <?= $this->Form->input('first_name', [
        'placeholder' => false,
        'label' => __('Meno'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="first-name"',
        ],
    ]); ?>

    <?= $this->Form->input('last_name', [
        'placeholder' => false,
        'label' => __('Priezvisko'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="last-name"',
        ],
    ]); ?>
</div>

<?php
echo $this->element('Rshop/Frontend.Form/address_autocomplete', [
    'type' => $addressType,
    'inputs' => [
        'address' => $isOrderEntity ? 'billing_address' : 'address',
        'city' => $isOrderEntity ? 'billing_city' : 'city',
        'post_code' => $isOrderEntity ? 'billing_post_code' : 'post_code',
        'country_id' => $isOrderEntity ? 'billing_country_id' : 'country_id',
    ],
]);
?>

<div class="c-form__row">
    <?= $this->Form->input($isOrderEntity ? 'billing_address' : 'address', [
        'data-autocomplete-name' => 'street',
        'placeholder' => false,
        'label' => __('Názov a číslo ulice'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="address"',
            'inputAttrs' => 'data-type="' . $addressType . '-address"',
        ],
    ]); ?>

    <?= $this->Form->input($isOrderEntity ? 'billing_post_code' : 'post_code', [
        'data-autocomplete-name' => 'post_code',
        'placeholder' => '--- --',
        'label' => __('PSČ'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="post-code"',
            'inputAttrs' => 'data-type="' . $addressType . '-post-code"',
        ],
    ]); ?>
</div>

<div class="c-form__row">
    <?= $this->Form->input($isOrderEntity ? 'billing_city' : 'city', [
        'data-autocomplete-name' => 'city',
        'placeholder' => false,
        'label' => __('Mesto'),
        'required' => true,
        'templateVars' => [
            'holderClass' => 'data-type="city"',
            'inputAttrs' => 'data-type="' . $addressType . '-city"',
        ],
    ]); ?>

    <?= $this->Form->select($isOrderEntity ? 'billing_country_id' : 'country_id', null, [
        'placeholder' => false,
        'label' => __('Krajina'),
        'required' => true,
        'autocomplete' => 'off',
        'value' => $this->request->getSession()->read('custom.deliveryCountryId') ?: null,
        'templateVars' => [
            'holderClass' => 'data-type="country"',
            'inputAttrs' => 'data-type="' . $addressType . '-country"',
        ],
    ]); ?>
</div>

<div class="c-form__row">
    <?= $this->Form->input($isOrderEntity ? 'customer_phone' : 'phone', [
        'placeholder' => false,
        'label' => __('Telefónne číslo (mobil)'),
        'required' => false,
        'templateVars' => [
            'holderClass' => 'data-type="phone"',
        ],
    ]); ?>
</div>

<?php
if ($showCompanyInputs) {
    echo $this->element('Rshop/Frontend.Form/company_inputs', ['showFinstatField' => false]);
}

echo $this->element('Rshop/Frontend.Form/finstat', ['data' => [
    'options' => [
        'autocomplete_field' => 'input[data-autocomplete-field]',
        'company_field' => 'input[data-autocomplete-name=company]',
        'org_num_field' => 'input[data-autocomplete-name=org_num]',
        'tax_num_field' => 'input[data-autocomplete-name=tax_num]',
        'vat_num_field' => 'input[data-autocomplete-name=vat_num]',
        'street_field' => 'input[data-autocomplete-name=street]',
        'city_field' => 'input[data-autocomplete-name=city]',
        'post_code_field' => 'input[data-autocomplete-name=post_code]'
    ]
]]);
?>
