<?php
$entity = $entity ?? $user;
$data = [];

switch ($type) {
    case 'shipping':
        $data['name'] = $entity->shipping_name;
        $data['phone'] = $entity->shipping_phone;
        $data['email'] = $entity->customer_email;

        $data['company'] = $entity->shipping_company;
        $data['address'] = $entity->shipping_address;
        $data['post_code'] = $entity->shipping_post_code;
        $data['city'] = $entity->shipping_city;
        $data['country'] = $entity->shipping_country->name;

        break;

    case 'billing':
        $data['name'] = $entity->billing_name;
        $data['phone'] = $entity->customer_phone;
        $data['email'] = $entity->customer_email;

        $data['address'] = $entity->billing_address;
        $data['post_code'] = $entity->billing_post_code;
        $data['city'] = $entity->billing_city;
        $data['country'] = $entity->billing_country->name;

        $data['org_num'] = $entity->billing_org_num;
        $data['tax_num'] = $entity->billing_tax_num;
        $data['vat_num'] = $entity->billing_vat_num;

        break;

    case 'address':
        $data['name'] = $entity->first_name . ' ' . $entity->last_name;
        $data['phone'] = $entity->phone;
        $data['email'] = $entity->email;

        $data['company'] = $entity->company;
        $data['address'] = $entity->address;
        $data['post_code'] = $entity->post_code;
        $data['city'] = $entity->city;
        $data['country'] = $entity->country->name;

        break;

    case 'account':
    default:
        $data['name'] = $entity->is_company && $entity->company ? $entity->company : ($entity->first_name . ' ' . $entity->last_name);
        $data['phone'] = $entity->phone;
        $data['email'] = $entity->email;

        $data['address'] = $entity->address;
        $data['post_code'] = $entity->post_code;
        $data['city'] = $entity->city;
        $data['country'] = $entity->country->name;

        $data['org_num'] = $entity->org_num;
        $data['tax_num'] = $entity->tax_num;
        $data['vat_num'] = $entity->vat_num;

        break;
}
?>

<div class="c-info-box">
    <div class="c-info-box__item">
        <?php
        echo '<div><strong data-type="name">' . $data['name'] . '</strong></div>';
        echo '<div><span data-type="phone">' . $data['phone'] . '</span></div>';
        echo '<div>' . $data['email'] . '</div>';
        ?>
    </div>

    <div class="c-info-box__item">
        <?php
        if (\in_array($type, ['shipping', 'address'])) {
            echo '<div><span data-type="company">' . $data['company'] . '</span></div>';
        }

        echo '<div><span data-type="address">' . $data['address'] . '</span></div>';
        echo '<div><span data-type="post_code">' . $data['post_code'] . '</span> <span data-type="city">' . $data['city'] . '</span></div>';
        echo '<div><span data-type="country">' . $data['country'] . '</span></div>';
        ?>
    </div>

    <?php
    if (\in_array($type, ['billing', 'account'])) {
    ?>
    <div class="c-info-box__item js-address-box-company <?= (!$data['org_num'] && !$data['tax_num'] && !$data['vat_num']) ? 'is-not-visible' : ''; ?>">
        <?php
        echo '<div>' . __('IČO') . ': <strong data-type="org_num">' . $data['org_num'] . '</strong></div>';
        echo '<div>' . __('DIČ') . ': <span data-type="tax_num">' . $data['tax_num'] . '</span></div>';
        echo '<div>' . __('IČ DPH') . ': <span data-type="vat_num">' . $data['vat_num'] . '</span></div>';
        ?>
    </div>
    <?php
    }
    ?>
</div>