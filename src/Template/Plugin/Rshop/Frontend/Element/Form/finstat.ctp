<?php
if ($this->Configure->read('Rshop.finstat.enabled')) {
    $this->Html->preload('/js/lib/jquery/jquery-ui.min.js', ['as' => 'script']);
    $this->Html->preload('/css/lib/jquery-ui.css', ['as' => 'style']);

    echo $this->Html->script('/js/lib/jquery/jquery-ui.min.js', ['block' => 'script', 'data-cookieconsent' => 'ignore']);
    echo $this->Html->css('/css/lib/jquery-ui.css', ['block' => 'libsCss', 'data-cookieconsent' => 'ignore']);
    echo $this->element('Rshop/Customers.finstat_autocomplete', ($data ?? []));
}
