<?php

use Cake\Core\Configure;

$isShopping = $isShopping ?? false;

$forgottenPasswordLink = Configure::check('Users.Email.required') ? $this->Html->link(
    '<span>' . __('Zabudli ste svoje heslo') . '?</span>',
    ['urlKey' => 'Eshop.lostPassword'],
    [
        'class' => 'c-link c-link--secondary',
        'escape' => false,
        'target' => ($isShopping ? '_blank' : '_self'),
        'tabindex' => '-1',
    ]
) : '';
?>

<div class="c-msg c-msg--error is-flash is-hidden js-login-error"></div>

<div class="c-form">
    <?php
    $formOptions = [
        'url' => $this->Url->build(['urlKey' => 'Eshop.userLogin'], true),
        'name' => 'form-login',
        'class' => 'js-form-login',
    ];

    if ($isShopping) {
        $formOptions['data-email-verification-url'] = $this->Url->build(['controller' => 'Users', 'action' => 'validateCustomerEmail'], true);
    }

    echo $this->Form->create(null, $formOptions);
    echo $this->Form->input('login', [
        'required' => true,
        'label' => __('Váš e-mail'),
        'placeholder' => '@',
        'templateVars' => [
            'class' => 'js-input-login',
            'holder' => 'is-email-input-holder'
        ]
    ]);

    echo $this->Form->input('password', [
        'required' => true,
        'label' => __('Heslo'),
        'placeholder' => __('Vaše heslo'),
        'templateVars' => [
            'class' => 'js-input-password',
            // 'labelAddon' => $forgottenPasswordLink,
            // 'inputAddon' => $this->Html->ico('eye', ['class' => 'js-toggle-password']),
            // 'inputClass' => 'has-addon'
        ]
    ]);

    echo $this->Form->button(__('Prihlásiť sa'), [
        'class' => 'c-btn c-btn--secondary has-full-width is-centered',
        'escape' => false
    ]);

    if ($isShopping) {
    ?>
        <div class="login-buttons">
            <div class="login-buttons__new">
                <button type="button" class="c-link c-link--primary js-trigger-reg-form">
                    <?= __('Chcete si vytvoriť nový účet?'); ?>
                </button>
            </div>
        </div>
    <?php
    } else {
        echo '<div class="c-login-links">';
        echo '<div class="c-login-links__item">';
        echo '<span class="sc-text">' . __('Nemáte konto?') . '</span>';
        echo $this->Html->link(
            __('Vytvoriť nové konto'),
            ['urlKey' => 'Eshop.registration'],
            ['escape' => false, 'class' => 'c-link']
        );
        echo '</div>';

        echo '<div class="c-login-links__item">';
        echo '<span class="sc-text">' . __('Zabudli ste heslo?') . '</span>';
        echo $this->Html->link(
            __('Reset hesla'),
            ['urlKey' => 'Eshop.lostPassword'],
            ['escape' => false, 'class' => 'c-link']
        );
        echo '</div>';
        echo '</div>';
    }
    echo $this->Form->end();
    ?>
</div>