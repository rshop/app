<?php
$type = $type ?? 'shipping';
$inputs = $inputs ?? [
    'address' => 'customer_addresses[0][address]',
    'city' => 'customer_addresses[0][city]',
    'post_code' => 'customer_addresses[0][post_code]',
    'country_id' => 'customer_addresses[0][country_id]'
];

// v modalnom okne (v mojom ucte) to zatial nefunguje, takze iba v nakupe zatial
if (!($this->request->params['controller'] == 'Steps' && $this->request->params['action'] == 'form')) {
    return;
}

// google places
if ($this->Configure->read('Eshop.googleAutocompleteActive')) {
?>
<div class="c-form__row">
    <?= $this->Form->googleAutocomplete([
        'id' => 'shipping-google-autocomplete-' . $type,
        'inputs' => $inputs,
        /*'label' => [
            'text' => '<span class="label-text">' . __('Vyhľadať adresu') . ':</span><small><a href="#" class="js-label-manual-address" data-type="billing">' . __('zadať adresu manuálne') . '</a></small>',
            'escape' => false
        ],*/
        'inputOptions' => [
            'templateVars' => [
                'labelClass' => 'is-google-autocomplete'
            ]
        ]
    ]); ?>
</div>
<?php
}

// smartform
if ($this->Configure->read('Eshop.smartformActive')) {
    $this->Html->script('tools/smartform', ['block' => true, 'data-cookieconsent' => 'ignore']);

    echo $this->Form->input('smartform_shipping_autocomplete', [
        'id' => 'shipping-smartform-autocomplete',
        'label' => __('Vyhľadať adresu') . ':',
        'placeholder' => __('Začnite písať názov ulice'),
        'templateVars' => [
            'class' => 'smartform-instance-' . $type . ' smartform-address-whole-address js-smartform-trigger',
            'inputAttrs' => 'data-type="' . $type . '"',
        ]
    ]);

    $this->Form->unlockField('smartform_shipping_autocomplete');
}
