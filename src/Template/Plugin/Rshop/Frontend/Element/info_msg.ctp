<div class="c-info c-info--msg js-info-message" data-cookie-value="<?= \md5($infoMsg); ?>">
    <div class="l-container">
        <div class="c-info__wrapper">
            <button type="button" class="c-info__btn c-btn c-btn--close js-info-message-close" aria-label="<?= __('Zatvoriť'); ?>" title="<?= __('Zatvoriť'); ?>">
                <?= $this->Html->ico('close'); ?>
            </button>

            <div class="c-info__text">
                <?= $infoMsg; ?>
            </div>
        </div>
    </div>
</div>
