<?php
$logoSrc = 'logos/rshop.svg';
$this->Html->preload('/img/' . $logoSrc, ['as' => 'image']);

$isLogged = $this->Auth->isLogged();

$topLinksBox = $this->cell('Rshop/Frontend.Box', ['header-top-links', ['limit' => 5]], [
    'customTemplate' => 'simple_links',
    'cache' => [
        'config' => 'rshop_boxes',
        'key' => 'top_header_links_' . $projectName
    ]
])->__toString();
?>

<header class="l-header js-page-header">
    <?= $this->element('Rshop/Frontend.Layout/header_messages'); ?>

    <div class="l-header__top js-header-navigation">
        <div class="l-container">
            <div class="c-header-top">
                <div class="c-header-top__msg">
                    <span class="c-indicator"></span>

                    <span class="js-admin-edit" data-url="<?= $this->Configure->getConfigurationUrl('text.header.openings'); ?>">
                        <?= $this->Configure->text('header.openings'); ?>
                    </span>

                    <?= $this->Html->link(
                        '<strong>' . $this->Configure->read('Rshop.store.phone') . '</strong>',
                        'tel:' . $this->Configure->read('Rshop.store.phone'),
                        ['class' => 'c-link c-link--secondary', 'escape' => false]
                    ); ?>
                </div>

                <?php
                if ($topLinksBox) {
                ?>
                <div class="c-header-top__links js-admin-edit" data-url="<?= $this->Configure->getBoxUrl('header-top-links'); ?>">
                    <div class="sc-list">
                        <?= $topLinksBox; ?>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="l-header__main">
        <div class="l-container">
            <div class="l-header-main <?= 'is-' . $this->Configure->read('Rshop.store.name'); ?>">
                <div class="l-header-main__logo">
                    <div class="c-header-logo">
                        <?= $this->Html->link(
                            $this->Html->image($logoSrc, ['class' => 'c-img', 'alt' => $this->Configure->read('Rshop.store.name')]),
                            $this->Url->build('/'),
                            [
                                'escape' => false,
                            ]
                        ); ?>

                        <span class="sc-text">
                            <?= __('Váš európsky partner pre lakovanie'); ?>
                        </span>
                    </div>
                </div>

                <div class="l-header-main__search" id="autocomplete">
                    <div class="c-search">
                        <div class="c-form">
                            <div class="c-form__item c-form__item--text">
                                <input class="c-form__input is-empty" readonly type="text" placeholder="<?= __('Napíšte názov produktu, kategóriu alebo výrobcu'); ?>" />
                            </div>

                            <button class="c-btn c-btn--transparent">
                                <i class="ico ico-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="l-header-main__actions">
                    <div class="c-header-actions">
                        <div class="c-header-actions__item is-search txs-is-visible">
                            <button class="c-link js-trigger-search">
                                <i class="ico ico-search" aria-hidden="true"></i>
                                <span class="is-text"><?= __('Vyhľadávanie'); ?></span>
                            </button>
                        </div>

                        <div class="c-header-actions__item is-account">
                            <?php
                            if ($isLogged) {
                                ?>
                                <button class="c-link" r-toggle-trigger="account-menu">
                                    <i class="ico ico-account" aria-hidden="true"></i>

                                    <span class="is-text sc-user-name">
                                        <?= $this->Auth->info('is_company') ? $this->Auth->info('company') : $this->Auth->info('first_name') . ' ' . $this->Auth->info('last_name'); ?>
                                    </span>
                                </button>
                                <?php
                            } else {
                                echo $this->Html->link(
                                    '<span class="is-text">' . __('Prihlásenie') . '</span>',
                                    '#',
                                    [
                                        'escape' => false,
                                        'class' => 'c-link js-login-button desktop-is-visible',
                                        'prepend' => $this->Html->ico('account'),
                                        'data-toggle' => 'modal',
                                        'data-target' => '#login-modal'
                                    ]
                                );
                                ?>

                                <button class="c-link mobile-is-visible" r-toggle-trigger="account-menu">
                                    <i class="ico ico-account" aria-hidden="true"></i>

                                    <span class="is-text <?= $isLogged ? 'sc-user-name' : ''; ?>">
                                        <?= __('Prihlásenie'); ?>
                                    </span>
                                </button>
                                <?php
                            }
                            ?>

                            <r-toggle class="c-header-actions__dropdown" id="account-menu" show-overlay="header" close-outside>
                                <div class="c-header-dropdown">
                                    <div class="c-header-dropdown__header">
                                        <div class="sc-text">
                                            <?= __('Pekný deň'); ?>
                                        </div>

                                        <div class="sc-name">
                                            <?php
                                            if ($isLogged) {
                                                echo $this->Auth->info('is_company') ? $this->Auth->info('company') : $this->Auth->info('first_name') . ' ' . $this->Auth->info('last_name');
                                            } else {
                                                echo __('Ste neprihlásený/á');
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="c-header-dropdown__list">
                                        <hr />

                                        <?php
                                        if ($isLogged) {
                                            foreach ($this->Configure->getCustomAccountItems() as $key => $accountItem) {
                                                if (!$accountItem['customShowInDropdown']) {
                                                    continue;
                                                }

                                                $linkName = $accountItem['name'];
                                                $linkClass = '';
                                                $linkIcon = '';

                                                if ($key == 'CustomSettings') {
                                                    $linkName = __('Môj účet');
                                                }

                                                if ($key == 'Logout') {
                                                    echo '<hr />';
                                                    $linkClass .= ' is-logout';
                                                    $linkIcon = $this->Html->ico('logout');
                                                }

                                                echo $this->Html->link(
                                                    $linkName,
                                                    $this->Url->build($accountItem['url'], true),
                                                    [
                                                        'class' => 'c-link' . $linkClass,
                                                        'escape' => false,
                                                        'append' => $linkIcon
                                                    ]
                                                );
                                            }
                                        } else {
                                            echo $this->Html->link(
                                                '<span class="is-text">' . __('Prihlásenie') . '</span>',
                                                '#',
                                                [
                                                    'escape' => false,
                                                    'class' => 'c-link js-login-button',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#login-modal'
                                                ]
                                            );

                                            echo $this->Html->link(
                                                '<span class="is-text">' . __('Registrácia') . '</span>',
                                                ['urlKey' => 'Eshop.registration'],
                                                [
                                                    'escape' => false,
                                                    'class' => 'c-link',
                                                ]
                                            );
                                        }
                                        ?>
                                    </div>
                                </div>
                            </r-toggle>
                        </div>

                        <div class="c-header-actions__item is-placeholder is-minicart" id="minicart">
                            <a class="c-link" title="<?= __('Košík'); ?>">
                                <i class="ico ico-minicart" aria-hidden="true"></i>
                                <span class="is-text"><?= __('Košík'); ?></span>
                                <div class="c-badge is-secondary" data-count="0"></div>
                            </a>
                        </div>

                        <div class="c-header-actions__item is-trigger mobile-is-visible">
                            <button type="button" class="c-link js-aside-trigger">
                                <i class="ico ico-menu" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="l-header-main__navigation js-header-navigation">
                    <?= $this->cell('Rshop/Frontend.Menu', [['getAll' => true]], [
                        'getAll' => true,
                        'cache' => [
                            'config' => 'rshop_categories',
                            'key' => 'main_menu'
                        ]
                    ])->__toString(); ?>
                </div>
            </div>
        </div>
    </div>
</header>