<?php
$isShopping = $isShopping ?? false;

$customMessages = \array_filter($customMessages ?: [], function ($customMessage) {
    $cookieKey = 'custom-msg-' . $customMessage['id'];

    return !isset($_COOKIE[$cookieKey]) || $_COOKIE[$cookieKey] !== \md5($customMessage['text']);
});

if ($customMessages) {
    ?>
    <div class="c-header-messages js-header-messages">
        <?php
        foreach ($customMessages as $customMessage) {
            if ($isShopping && !$customMessage['custom_show_shopping']) {
                continue;
            }

            echo $this->element('Rshop/Frontend.Layout/custom_info_msg', [
                'customMessage' => $customMessage
            ]);
        }
        ?>
    </div>
    <?php
}
