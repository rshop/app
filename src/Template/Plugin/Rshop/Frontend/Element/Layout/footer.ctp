<?php
$logoSrc = 'logos/rshop.svg';

$footerColumnsBox = $this->cell('Rshop/Frontend.Box', ['footer-columns'], [
    'customTemplate' => 'footer_columns',
    'cache' => [
        'config' => 'rshop_boxes',
        'key' => 'footer_columns'
    ]
])->__toString();
?>

<footer class="l-footer">
    <div class="l-footer__main">
        <div class="l-container">
            <div class="c-footer-main">
                <div class="c-footer-main__logo">
                    <?php
                    echo $this->Html->link(
                        $this->Html->image($logoSrc, ['class' => 'c-img', 'alt' => $this->Configure->read('Rshop.store.name')]),
                        $this->Url->build('/'),
                        ['escape' => false]
                    );
                    ?>
                </div>

                <div class="c-footer-main__social">
                    <div class="c-footer-social">
                        <div class="c-footer-social__title">
                            <?= __('Sledujte nás'); ?>
                        </div>

                        <div class="c-footer-social__list">
                            <?= $this->Html->link(
                                $this->Html->ico('social-facebook'),
                                $this->Configure->text('general.facebookLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>

                            <?= $this->Html->link(
                                $this->Html->ico('social-youtube'),
                                $this->Configure->text('general.youtubeLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>

                            <?= $this->Html->link(
                                $this->Html->ico('social-instagram'),
                                $this->Configure->text('general.instagramLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>

                            <?= $this->Html->link(
                                $this->Html->ico('social-linkedin'),
                                $this->Configure->text('general.linkedInLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>

                            <?= $this->Html->link(
                                $this->Html->ico('social-tiktok'),
                                $this->Configure->text('general.tikTokLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>

                            <?= $this->Html->link(
                                $this->Html->ico('social-discord'),
                                $this->Configure->text('general.discordLink'),
                                ['class' => 'c-link c-link--secondary-100', 'escape' => false, 'target' => '_blank']
                            ); ?>
                        </div>
                    </div>
                </div>

                <div class="c-footer-main__columns">
                    <?= $footerColumnsBox; ?>
                </div>

                <div class="c-footer-main__info">
                    <div class="c-footer-info">
                        <div class="c-footer-info__title">
                            <?= __('Objednávky a informácie o tovare'); ?>
                        </div>

                        <div class="c-footer-info__list">
                            <div class="c-footer-info__item">
                                <div class="sc-key js-admin-edit" data-url="<?= $this->Configure->getConfigurationUrl('text.footer.phoneText'); ?>">
                                    <?= $this->Configure->text('footer.phoneText'); ?>
                                </div>

                                <div class="sc-value">
                                    <?= $this->Html->link(
                                        $this->Configure->read('Rshop.store.phone'),
                                        'tel:' . $this->Configure->read('Rshop.store.phone'),
                                        ['class' => 'c-link c-link--secondary-100']
                                    ); ?>
                                </div>
                            </div>

                            <div class="c-footer-info__item">
                                <div class="sc-key js-admin-edit" data-url="<?= $this->Configure->getConfigurationUrl('text.footer.emailText'); ?>">
                                    <?= $this->Configure->text('footer.emailText'); ?>
                                </div>

                                <div class="sc-value">
                                    <?= $this->Html->link(
                                        $this->Configure->read('Rshop.store.email'),
                                        'mailto:' . $this->Configure->read('Rshop.store.email'),
                                        ['class' => 'c-link c-link--secondary-100']
                                    ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="c-footer-main__guarantee">
                    <?= ''//$this->element('Rshop/Frontend.guarantee'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="l-footer__middle">
        <div class="l-container">
            <div class="c-footer-middle">
                <div class="c-footer-middle__links">
                    <div class="sc-item">
                        <?= $this->Html->link(
                            __('Ochrana osobných údajov'),
                            $this->Url->build('ochrana-sukromia'),
                            ['class' => 'c-link']
                        ); ?>
                    </div>

                    <div class="sc-item">
                        <?= $this->Html->link(
                            __('Obchodné podmienky'),
                            ['urlKey' => 'Articles.ObchPodmienky'],
                            ['class' => 'c-link']
                        ); ?>
                    </div>

                    <div class="sc-item">
                        <?= $this->Html->link(
                            __('Nastavenia cookies'),
                            $this->Url->build('ochrana-sukromia'),
                            ['class' => 'c-link']
                        ); ?>
                    </div>
                </div>

                <div class="c-footer-middle__guarantee">
                    <?= $this->Html->lazyImage('/img/logos/payment-visa.svg', ['alt' => 'Visa', 'width' => 62, 'height' => 20]); ?>
                    <?= $this->Html->lazyImage('/img/logos/payment-mastercard.svg', ['alt' => 'Mastercard',  'width' => 150, 'height' => 26]); ?>
                    <?= $this->Html->lazyImage('/img/logos/payment-googlepay.svg', ['alt' => 'Google Pay',  'width' => 56, 'height' => 30]); ?>
                    <?= $this->Html->lazyImage('/img/logos/payment-applepay.svg', ['alt' => 'Apple Pay',  'width' => 47, 'height' => 30]); ?>
                    <?= $this->Html->lazyImage('/img/logos/shipping-dpd.svg', ['alt' => 'Dpd',  'width' => 61, 'height' => 28]); ?>
                    <?= $this->Html->lazyImage('/img/logos/shipping-alzabox.svg', ['alt' => 'Alza box',  'width' => 85, 'height' => 28]); ?>
                    <?= $this->Html->lazyImage('/img/logos/shipping-packeta.svg', ['alt' => 'Packeta',  'width' => 100, 'height' => 28]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="l-footer__bottom">
        <div class="l-container">
            <div class="c-footer-bottom">
                <div class="c-footer-bottom__copyright">
                    <div class="sc-copy">© <?= \date('Y') . ' ' . $this->Configure->read('Rshop.store.name'); ?></div>
                    <div class="sc-riesenia"><?= __('Tvorba výkonných internetových obchodov od {0}', '<a href="https://www.riesenia.com/" class="c-link c-link--inherit" target="_blank"><strong>RIESENIA</strong></a>'); ?></div>
                </div>

                <div class="c-footer-bottom__recaptcha">
                    <?php
                    $googlePrivacyLink = $this->Html->link(__('Pravidlá ochrany osobných údajov'), 'https://policies.google.com/privacy', ['class' => 'c-link c-link--inherit', 'target' => '_blank']);
                    $googleTermsLink = $this->Html->link(__(' Zmluvné podmienky'), 'https://policies.google.com/terms', ['class' => 'c-link c-link--inherit', 'target' => '_blank']);

                    echo __('Táto stránka je chránená pomocou reCAPTCHA a uplatňujú sa') . ' ' . $googlePrivacyLink . ' ' . __('spoločnosti Google a ich') . ' ' . $googleTermsLink . '.';
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>