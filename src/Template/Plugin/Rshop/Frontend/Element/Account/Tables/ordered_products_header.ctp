<div class="l-row">
    <div class="l-col" data-name="p-ordered-product-info">
        <?= __('Tovar'); ?>
    </div>

    <div class="l-col" data-name="p-ordered-product-model">
        <?= __('Model'); ?>
    </div>

    <div class="l-col" data-name="p-ordered-product-rating">
        <?= __('Hodnotenie'); ?>
    </div>

    <div class="l-col" data-name="p-ordered-product-price">
        <?= __('Cena'); ?>
    </div>
</div>