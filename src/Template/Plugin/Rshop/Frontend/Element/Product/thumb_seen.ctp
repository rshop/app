<?php
$cartQuantity = $this->Cart->getCartItemQuantity($data->id);
$thumbClasses = 'js-thumb' . ($cartQuantity > 0 ? ' is-in-cart' : '');
?>

<div class="c-seen-thumb <?= $thumbClasses; ?>" data-product-id="<?= $data->id; ?>">
    <?= $this->Html->link(
        $this->Html->lazyEntityThumbnail($data, ['format' => 'sm']),
        $data->link,
        ['class' => 'c-link c-seen-thumb__image', 'escape' => false]
    ); ?>

    <div class="c-seen-thumb__content">

        <?= $this->Html->link(
            $data->name,
            $data->link,
            ['class' => 'c-link c-seen-thumb__name']
        ); ?>

        <div class="c-seen-thumb__stars">
            <div class="is-stars js-stars" data-size="12" data-stars="<?= $data->rating_avg; ?>"></div>
            <span class="is-count">(<?= $data->rating_sum ?: 0; ?>)</span>
        </div>

        <div class="c-seen-thumb__availability">
            <?= $this->Product->customAvailability($data, true); ?>
        </div>

        <div class="c-seen-thumb__price">
            <!-- TODO: cena od -->
            <small class="is-from"><?= __('od'); ?></small>

            <span class="is-actual">
                <?= $this->Cart->productPrice($data, 'price'); ?>
            </span>
        </div>
    </div>
</div>
