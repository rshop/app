<?php
$isFavoriteListing = \in_array($this->request->params['controller'], ['CustomFavoriteProducts']) && \in_array($this->request->params['action'], ['listing']);
$isInFavorites = $this->Product->customIsFavorite($product);

$addText = $addText ?? __('Pridať do zoznamu');
$addTextShort = $addTextShort ?? __('Do zoznamu');
$removeText = $removeText ?? __('Odstrániť zo zoznamu');
$removeTextShort = $removeTextShort ?? __('Odstrániť');
?>

<r-favorite-trigger
    class="c-favorite-trigger <?= $class ?? ''; ?> <?= $isInFavorites ? 'is-in-favorites' : ''; ?>"
    data-add-url="<?= $this->Url->build(['controller' => 'CustomFavoriteProducts', 'action' => 'add', 'plugin' => null, $product->id]); ?>"
    data-remove-url="<?= $this->Url->build(['controller' => 'CustomFavoriteProducts', 'action' => 'remove', 'plugin' => null, $product->id]); ?>"
    data-add-text="<?= $addText; ?>"
    data-remove-text="<?= $removeText; ?>"
    data-count-selector=".js-favorite-products-count"
    tabindex="0"
    <?= $isFavoriteListing ? 'data-refresh-listing="true"' : ''; ?>
>
    <i class="ico <?= $isInFavorites ? 'ico-wishlist-saved' : 'ico-wishlist'; ?>"></i>
    <span class="cxs-is-hidden sc-text"><?= $isInFavorites ? $removeText : $addText; ?></span>
    <span class="cxs-is-visible sc-text-short"><?= $isInFavorites ? $removeTextShort : $addTextShort; ?></span>
</r-favorite-trigger>