<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

$status = $this->modelHelper('Product')->status($product);
$statusDescription = $this->modelHelper('Product')->statusDescription($product);

echo '<div class="sc-item is-' . ($product->is_available ? 'available' : 'unavailable') . '">' . $status . '</div>';
