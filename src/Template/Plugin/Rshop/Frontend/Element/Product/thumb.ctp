<?php
$cartQuantity = $this->Cart->getCartItemQuantity($data->id);
$thumbClasses = 'js-thumb is-' . $view . ($cartQuantity > 0 ? ' is-in-cart' : '');
?>

<div class="c-product-thumb <?= $thumbClasses; ?>" data-product-id="<?= $data->id; ?>">
    <?= $data->name; ?>
</div>