<?php
if (\strpos($params['notFoundArray'][0][0], 'Objednávkový kód') !== false) {
    \array_shift($params['notFoundArray']);
}

$notFoundCount = \count($params['notFoundArray']);
$notFoundArray = $params['notFoundArray'];

$wordingNotFoundProducts = $this->Format->plural(
    $notFoundCount . ' ' . __('produkt'),
    $notFoundCount . ' ' . __('produkty'),
    $notFoundCount . ' ' . __('produktov'),
    $notFoundCount
);

$wordingAdd = $this->Format->plural(
    __('sa nepridal'),
    __('sa nepridali'),
    __('sa nepridalo'),
    $notFoundCount
);
?>

<?php
if ($notFoundCount > 0) {
    ?>
<div class="b2b-csv-msg">
    <div class="b2b-csv-msg__holder">
        <button type="button" class="b2b-csv-msg__close btn js-close-csv-msg">
            <i class="ico ico-close"></i>
        </button>

        <div class="b2b-csv-msg__text">
            <?= __('Och, {0} z vášho zoznamu {1} do košíka! Produkty sa u nás možno už nenachádzajú, alebo potrebujete aktualizovať svoj zoznam. Produkty si môžete {2}zobraziť alebo stiahnuť{3} ich zoznam ako textový súbor.', '<strong>' . $wordingNotFoundProducts . '</strong>', '<strong>' . $wordingAdd . '</strong>', '<strong>', '</strong>'); ?>
        </div>

        <div class="b2b-csv-msg__links">
            <?= $this->Html->link(
                '<i class="ico ico-file-download"></i><span>' . __('Stiahnuť') . '</span>',
                '#',
                ['escape' => false, 'class' => 'b2b-csv-msg__link c-link js-notfound-download']
            ); ?>

            <?= $this->Html->link(
                '<i class="ico ico-caret-down"></i><span>' . __('Zobraziť') . '</span>',
                '#',
                ['escape' => false, 'class' => 'b2b-csv-msg__link c-link js-notfound-show']
            ); ?>
        </div>
    </div>

    <div class="b2b-csv-msg__notfound js-notfound">
        <strong><?= __('Zoznam nepridaných produktov') . ':'; ?></strong>

        <ul>
            <?php
            foreach ($notFoundArray as $product) {
                ?>
            <li>
                <?= $product[1] . 'x ' . $product[0]; ?>
            </li>
            <?php
            } ?>
        </ul>
    </div>
</div>
<?php
} else {
                ?>
<div class="b2b-csv-msg is-success">
    <div class="b2b-csv-msg__text">
        <?= h($message); ?>
    </div>
</div>
<?php
            }
?>

<?php
$this->Html->scriptBlock('
function download(csv, filename) {
    var csvFile,
        downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}
');
$this->Html->scriptBlock('
$(".js-notfound-show").on("click", function(event) {
    event.preventDefault();

    $(this).toggleClass("is-active");
    $(".js-notfound").toggle();
});

$(".js-notfound-download").on("click", function(event) {
    var csv = ' . \json_encode($notFoundArray) . ';

    download(csv.join("\n"), "export.csv");
});

$(".js-close-csv-msg").on("click", function(event) {
    $(".b2b-csv-msg").hide();
});
', ['ready' => true]);
