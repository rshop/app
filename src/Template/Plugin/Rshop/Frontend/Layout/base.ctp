<?php
$this->extend('Rshop/Frontend.core');

$layoutName = $this->App->getLayoutName();
$pageName = $this->App->getPageName($this);
$projectName = $this->Configure->read('Eshop.Project.name');

$this->assign('bodyClass', 'is-' . $projectName);

$this->start('notifications');
if (!isset($_COOKIE['cookies-allowed']) || $_COOKIE['cookies-allowed'] != 1) {
    echo $this->element('Rshop/Frontend.info_cookie');
}
$this->end();

$this->start('assets');
echo $this->Html->css('styles-common.css', ['block' => 'css']);
echo $this->Html->css('styles-layout-' . $layoutName . '.css', ['block' => 'layoutCss']);

if (\file_exists('./css/styles-lib.css')) {
    echo $this->Html->css('styles-lib.css', ['block' => 'libsCss']);
}

if (\file_exists('./css/styles-' . $pageName . '-dependencies.css')) {
    echo $this->Html->css('styles-' . $pageName . '-dependencies.css', ['block' => 'libsCss']);
}

if (\file_exists('./css/styles-' . $pageName . '.css')) {
    echo $this->Html->css('styles-' . $pageName . '.css', ['block' => 'pageCss']);
}
$this->end();

$this->start('assets_js_before');
// na poradi zalezi
$libsJs = [
    '/js/scripts-lib.min.js',
    '/js/scripts-' . $pageName . '-dependencies.min.js',
    '/js/scripts-frontend.min.js',
    '/js/scripts-' . $pageName . '-frontend.min.js',
];

foreach ($libsJs as $libJs) {
    if (\file_exists('.' . $libJs)) {
        echo $this->Html->script($libJs, ['block' => false]);
        $this->Html->preload($libJs, ['as' => 'script']);
    }
}
$this->end();

$this->start('assets_js');

if (\file_exists('./js/p-' . $pageName . '.min.js')) {
    $this->Html->script('/js/p-' . $pageName . '.min.js', ['block' => 'script', 'defer']);
}
$this->end();

// TODO: preload
// $this->Html->preload('/fonts/Jost/jost-cyrillic.woff2', ['as' => 'font', 'type' => 'font/woff2', 'crossorigin']);
// $this->Html->preload('/fonts/Jost/jost-latin.woff2', ['as' => 'font', 'type' => 'font/woff2', 'crossorigin']);
// $this->Html->preload('/fonts/Jost/jost-latin-ext.woff2', ['as' => 'font', 'type' => 'font/woff2', 'crossorigin']);
// $this->Html->preload('/fonts/icomoon/icomoon.ttf?1ytq9e', ['as' => 'font', 'crossorigin']);

echo $this->fetch('content');
