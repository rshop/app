<?php
$this->extend('Rshop/Frontend.Common/Layout/listing');

// title
$this->assign('title', __('Výsledky vyhľadávania'));

// settings
$this->set('settings', [
    'showSorts' => false
]);

echo $this->Listing->render($listingData);
