<?php
$this->extend('Rshop/Frontend.Common/Layout/listing');

// title
$this->assign('title', $category->heading);

// subcategories
if ($category->show_subcategories) {
    $subcategories = $this->cell('Rshop/Frontend.Category', [['parent_id' => $category->id, 'depth' => 0]], [
        // 'customTemplate' => 'subcategories',
        'cache' => [
            'config' => 'rshop_categories',
            'key' => 'listing_subcategories_' . $category->id
        ]
    ])->__toString();

    $this->assign('subcategories', $subcategories);
}

echo $this->Listing->render($listingData);
