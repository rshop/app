<?php
$this->extend('Rshop/Frontend.Common/Layout/listing');

// assign title
$this->assign('title', $config['title']);

// settings
$this->set('settings', [
    'showActionOption' => $config['type'] != 'action',
    'showNewOption' => $config['type'] != 'new',
]);

echo $this->Listing->render($listingData);
