<?php
$this->extend('Rshop/Frontend.Common/Base/content_template');

$this->assign('contentTag', 'article');
// layout setup
$this->assign('withoutWrapper', true);
$this->assign('extendLayout', '/Common/Layout/articles');

// default template definition
$this->start('template');
echo $this->element('Rshop/Frontend.Article/content');
$this->end();
