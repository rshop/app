<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

if (!$items) {
    return;
}

foreach ($items as $item) {
    echo $this->Html->link(
        $item->name,
        $item->link ?: $item->url,
        [
            'class' => 'c-link'
        ]
    );
}
