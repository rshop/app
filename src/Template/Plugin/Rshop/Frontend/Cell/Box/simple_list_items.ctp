<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

if (!$items) {
    return;
}

foreach ($items as $item) {
    echo '<li class="c-list__item">' . $item->name . '</li>';
}
