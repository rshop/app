<div class="c-modal__header">
    <div class="c-modal__name"><?= __('Sledovať produkt'); ?></div>

    <button class="c-btn c-btn--transparent is-sm" type="button" data-dismiss="modal">
        <i class="ico ico-close" aria-hidden="true"></i>
    </button>
</div>

<div class="c-modal__content">
    <div class="c-msg without-margin c-msg--error is-flash is-hidden js-watchdog-error"></div>
    <div class="c-msg without-margin c-msg--success is-flash is-hidden js-watchdog-success"></div>

    <div class="c-form js-watchdog-hide">
        <?php
        echo $this->Form->create($form, [
            'idPrefix' => 'watchdogForm',
            'name' => 'form-watchdog-modal',
            'url' => $this->Url->build(['urlKey' => 'Form.watchdog'], true),
            'class' => 'js-form-modal-watchdog',
        ]);
        echo $this->Form->input('customer_id', ['type' => 'hidden', 'value' => $this->Auth->info('id')]);

        echo $this->Form->input('customer_email', [
            'label' => __('Váš e-mail') . ':',
            'placeholder' => __('E-mail'),
            'required' => true,
            'type' => 'email',
            'default' => $this->Auth->info('email'),
        ]);

        $notificationOptions = [
            'become_cheaper' => __('Produkt je lacnejší než'),
            'change_price' => __('Zmena ceny produktu'),
        ];

        if (!$product->is_available) {
            $notificationOptions = [
                'available' => __('Produkt je dostupný'),
                'become_cheaper' => __('Produkt je lacnejší než'),
                'change_price' => __('Zmena ceny produktu'),
            ];
        }

        echo $this->Form->select('notification_type', $notificationOptions, [
            'required' => true,
            'label' => __('Typ notifikácie') . ':',
            'templateVars' => [
                'inputClass' => 'js-watchdog-notification-type',
            ],
        ]);

        echo '<div class="watchdog_price js-watchdog-price">';
        echo $this->Form->input('notification_price', [
            'type' => 'text',
            'required' => true,
            'label' => __('Cena nižšia než') . ':',
            'placeholder' => __('Nová cena'),
        ]);
        echo '</div>';

        echo $this->Form->input('product_id', ['type' => 'hidden', 'value' => $product->id]);
        echo $this->Form->input('product_price', ['type' => 'hidden', 'value' => $this->Cart->productPrice($product, 'price', 1, false, false)]);
        echo $this->Form->input('active', ['type' => 'hidden', 'value' => 1]);
        ?>

        <div class="form-group">
            <button type="submit" class="c-btn c-btn--primary is-bold"><?= __('Odoslať'); ?></button>
        </div>

        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>