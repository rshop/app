<?php
$isEdit = $action == 'edit';
$url = ['controller' => 'Users', 'action' => 'editShippingAddress', 'plugin' => null, $addressId];

if ($this->getRequest()->getQuery('updateOrder')) {
    $url['?'] = ['updateOrder' => 1];
}
?>

<div class="c-modal__header">
    <div class="c-modal__name">
        <?= $isEdit ? __('Úprava dodacej adresy') : __('Pridanie novej adresy'); ?>
    </div>

    <button class="c-btn c-btn--transparent is-sm" type="button" data-dismiss="modal">
        <i class="ico ico-cross" aria-hidden="true"></i>
    </button>
</div>

<div class="c-modal__content">
    <?php
    echo $this->Form->create($user, [
        'class' => 'c-form is-address js-form-address',
        'url' => $url,
        'data-locale' => $this->Format->locale()
    ]);
    echo '<div class="c-msg js-form-msg is-hidden"></div>';

    if ($isEdit) {
        echo $this->Form->input('customer_addresses.' . $addressId . '.id', ['type' => 'hidden']);
    }

    echo $this->Form->input('address_id', [
        'type' => 'hidden',
        'value' => 'shipping' . (isset($addressId) ? '-' . $addressId : ''),
        'templateVars' => [
            'class' => 'js-form-address-id',
        ],
    ]);

    echo $this->element('Rshop/Frontend.Form/shipping_address', \compact($addressId));

    echo '<div class="sc-submit">';
    echo $this->Form->button(__('Uložiť adresu'), [
        'class' => 'c-btn c-btn--secondary',
        'type' => 'submit',
    ]);
    echo '</div>';
    echo $this->Form->end();
    ?>
</div>