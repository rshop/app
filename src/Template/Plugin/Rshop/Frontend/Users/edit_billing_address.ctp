<div class="c-modal__header">
    <div class="c-modal__name">
        <?= __('Úprava fakturačnej adresy'); ?>
    </div>

    <button class="c-btn c-btn--transparent is-sm" type="button" data-dismiss="modal">
        <i class="ico ico-cross" aria-hidden="true"></i>
    </button>
</div>

<div class="c-modal__content">
    <?php
    echo $this->Form->create($entity, [
        'class' => 'c-form is-address js-form-address',
        'data-locale' => $this->Format->locale()
    ]);
    echo $this->Form->input('address_id', [
        'type' => 'hidden',
        'value' => 'billing-0',
        'templateVars' => [
            'class' => 'js-form-address-id',
        ],
    ]);

    echo '<div class="c-form-section">';
    echo $this->element('Rshop/Frontend.Form/billing_address', ['isEdit' => true, 'customerEntity' => $entity]);

    echo '<div class="sc-submit">';
    echo $this->Form->button(__('Uložiť adresu'), [
        'class' => 'c-btn c-btn--secondary',
        'type' => 'submit',
    ]);
    echo '</div>';
    echo '</div>';
    echo $this->Form->end();
    ?>
</div>