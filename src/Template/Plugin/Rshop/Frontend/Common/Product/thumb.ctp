<?php
$itemView = $_COOKIE['listingView'] ?? $this->Configure->read('Product.views.default');

echo $this->element('Rshop/Frontend.Product/thumb', ['view' => $itemView]);

echo $this->Form->input('ec-data', [
    'value' => $this->EcommerceTracking->encodeProductData($data),
    'type' => 'hidden',
    'data-ec-id' => $data->id,
    'data-ec-item' => 'Products'
]);

echo $this->Form->input('productViewTracking', [
    'type' => 'hidden',
    'data-ec-action' => 'productImpression',
    'data-ec-id' => $data->id,
    'data-ec-item' => 'Products'
]);
