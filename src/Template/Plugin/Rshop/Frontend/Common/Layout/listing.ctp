<?php
$this->extend('Rshop/Frontend.Common/Layout/Base/listing');

$title = $this->fetch('title') ?? '';
$subCategories = $this->fetch('subcategories') ?? false;
$manufacturers = $this->fetch('manufacturers') ?? false;
$settings = $settings ?? [];

$settings += [
    'showSorts' => true,
    'showViews' => true,
    'showLimit' => true,
    'showManufacturersFilter' => true,
    'showPriceFilter' => true,
    'showActionOption' => true,
    'showNewOption' => true,
];

if (!$this->exists('filter')) {
    $this->start('filter');
    echo $this->cell('Rshop/Frontend.Filter', [
        'id' => isset($category) ? $category->id : null,
        'options' => [
            'settings' => $settings
        ]
    ]);
    $this->end();
}

$filter = $this->fetch('filter') ?? false;

$this->start('layout');
?>
<div class="l-container">
    <div class="l-listing">
        <?= '{{{content}}}'; ?>
    </div>
</div>
<?php
$this->end();

echo $this->fetch('content');
