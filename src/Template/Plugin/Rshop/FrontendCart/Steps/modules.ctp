<?php
$this->assign('title', __('Doprava a platba'));

$isLogged = $this->Auth->isLogged();
$isB2b = $this->Configure->read('Custom.b2b');
?>

<div class="l-container">
    <div class="l-shopping">
        <div class="l-shopping__header">
            <div class="c-shopping-header">
                <div class="c-shopping-header__title">
                    <h1>
                        <?= $this->fetch('title'); ?>
                    </h1>
                </div>

                <div class="c-shopping-header__steps">
                    <?= $this->element('Rshop/FrontendCart.Layout/steps', ['actualStep' => 'third']); ?>
                </div>
            </div>
        </div>

        <div class="l-shopping__content">
            <div class="l-shopping-modules">
                <?= $this->Form->create($order, [
                    'id' => 'cart',
                    'class' => 'c-form js-form-cart-modules',
                    'data-update-action' => $this->Url->build(['urlKey' => 'Shopping.updateOrder']),
                    'location' => 'order-modules'
                ]); ?>
                <div class="c-form__messages">
                    <?= $this->Flash->render('Order.modules'); ?>
                </div>

                <?= $this->element('Rshop/FrontendCart.Modules/list'); ?>

                <div class="c-form__consents">
                    <?= $this->Form->consents([
                        'custom' => true
                    ]); ?>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>

        <div class="l-shopping__aside has-smaller-gap">
            <?= $this->element('Rshop/FrontendCart.Layout/recap'); ?>
        </div>

        <div class="l-shopping__buttons">
            <?= $this->element('Rshop/FrontendCart.Layout/buttons_third'); ?>
        </div>
    </div>
</div>