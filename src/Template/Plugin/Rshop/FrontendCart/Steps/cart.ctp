<?php
$this->assign('title', __('Prehľad košíka'));

$isB2b = $this->Configure->read('Custom.b2b');
$isEmptyCart = $this->Cart->isEmpty();
?>

<div class="l-container">
    <div class="l-shopping">
        <div class="l-shopping__header">
            <div class="c-shopping-header">
                <div class="c-shopping-header__title">
                    <h1>
                        <?= $this->fetch('title'); ?>
                    </h1>
                </div>

                <div class="c-shopping-header__steps">
                    <?= $this->element('Rshop/FrontendCart.Layout/steps', ['actualStep' => 'first']); ?>
                </div>
            </div>
        </div>

        <div class="l-shopping__messages">
            <?php
            if ($isEmptyCart) {
            ?>
                <div class="c-msg c-msg--info">
                    <?= __('V košíku nemáte žiadne produkty'); ?>
                </div>
            <?php
            }
            ?>

            <?= $this->Flash->render('Shopping.cart'); ?>
            <?= $this->Flash->render('Cart.voucher'); ?>
        </div>

        <div class="l-shopping__content">
            <div class="l-shopping-cart">
                <?php
                if (!$isEmptyCart) {
                ?>
                    <div class="l-shopping-cart__products">
                        <?= $this->element('Rshop/FrontendCart.Cart/products'); ?>
                    </div>
                <?php
                }
                ?>

                <div class="l-shopping-cart__info">
                    <div class="c-cart-info">
                        <div class="c-cart-info__partner">
                        </div>

                        <div class="c-cart-info__totals">
                            <?= $this->element('Rshop/FrontendCart.Cart/totals'); ?>
                        </div>

                        <?php
                        if (!$isEmptyCart) {
                        ?>
                            <div class="c-cart-info__voucher">
                                <?= $this->element('Rshop/FrontendCart.Cart/voucher'); ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="l-shopping__buttons">
            <?= $this->element('Rshop/FrontendCart.Layout/buttons_first'); ?>
        </div>
    </div>
</div>