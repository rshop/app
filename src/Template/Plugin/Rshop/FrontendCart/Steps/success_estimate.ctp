<?php
$this->assign('title', __('Dokončenie objednávky'));

$orderInfo = __('Váš dopyt číslo {0} bol úspešne odoslaný.{1}', '<strong>' . $order->document_number, '</strong>');

// survey forms
$this->assign('survey', '');

// main title
if (!$this->exists('shoppingTitle')) {
    $this->start('shoppingTitle'); ?>
    <div class="c-success-title">
        <div class="c-success-title__circle is-success">
            <i class="ico ico-tick" aria-hidden="true"></i>
        </div>

        <div class="c-success-title__text is-success">
            <?= __('Ďakujeme za váš dopyt.'); ?>
        </div>
    </div>
    <?php
    $this->end();
}
$successSurvey = $this->fetch('survey');
?>

<div class="l-container">
    <div class="l-shopping-success">
        <div class="l-shopping-success__main is-success">
            <div class="l-shopping-success__title">
                <?= $this->fetch('shoppingTitle'); ?>
            </div>

            <div class="l-shopping-success__order">
                <?= $orderInfo; ?>
            </div>

            <div class="l-shopping-success__divider"></div>

            <div class="l-shopping-success__text">
                <?php
                echo __('O postupe vybavenia dopytu {0}budete notifikovaný na vašu e-mailovú adresu.{1}', '<strong>', '</strong>') . '<br>';

            if ($this->Auth->isLogged()) {
                echo __('Históriu vašich objednávok a dopytov si môžete pozrieť cez {0}.', $this->Html->link(
                        __('váš účet'),
                        ['controller' => 'Users'],
                        ['class' => 'c-link']
                    ));
                echo '<br>';
            }

            echo __('V prípade, že ste potvrdzovací e-mail neobdržali, kontaktujte nás prosím e-mailom na {0}.', $this->Html->link(
                    $this->Configure->read('Rshop.store.email'),
                    'mailto:' . $this->Configure->read('Rshop.store.email'),
                    ['class' => 'c-link']
                )); ?>
            </div>

            <?php
            if (\trim($successSurvey)) {
                ?>
            <div class="l-shopping-success__survey">
                <?= $successSurvey; ?>
            </div>
            <?php
            } ?>
        </div>

        <div class="l-shopping-success__info">
            <div class="l-shopping-success__buttons is-success-step">
                <div class="cart-buttons is-success-step">
                    <div class="cart-buttons__item">
                        <?= $this->Html->link(
                            '<i class="ico ico-arrow-left"></i><span>' . __('Prejsť na úvodnú stránku') . '</span>',
                            $this->Url->build('/'),
                            ['class' => 'c-btn c-btn--secondary is-next', 'escape' => false]
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>