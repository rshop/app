<?php
$this->assign('title', __('Fakturačné a dodacie údaje'));

$isLogged = $this->Auth->isLogged();
$isB2b = $this->Configure->read('Custom.b2b');
?>

<div class="l-container">
    <div class="l-shopping">
        <div class="l-shopping__header">
            <div class="c-shopping-header">
                <div class="c-shopping-header__title">
                    <h1>
                        <?= $this->fetch('title'); ?>
                    </h1>
                </div>

                <div class="c-shopping-header__steps">
                    <?= $this->element('Rshop/FrontendCart.Layout/steps', ['actualStep' => 'second']); ?>
                </div>
            </div>
        </div>

        <div class="l-shopping__messages">
            <?= $this->Flash->render('Order.form'); ?>
        </div>

        <div class="l-shopping__content">
            <?php
            if ($isLogged) {
                echo $this->element('Rshop/FrontendCart.Form/logged_in');
            } else {
                echo $this->element('Rshop/FrontendCart.Form/not_logged_in');
            }
            ?>
        </div>

        <div class="l-shopping__aside">
            <?= $this->element('Rshop/FrontendCart.Layout/recap'); ?>
        </div>

        <div class="l-shopping__buttons">
            <?= $this->element('Rshop/FrontendCart.Layout/buttons_second'); ?>
        </div>
    </div>
</div>