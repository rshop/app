<?php
$successType = 'default';
$hasSurvey = false;

if ($isEstimate = $order->type == 'estimate') {
    $successType = 'estimate';
}

if ($isOmnipay = !$this->exists('moduleResponse') && $order->payment->is_omnipay) {
    $successType = 'omnipay';
}

if ($paymentAttempt = isset($payment)) {
    if ($order->paid) {
        $successType = 'paid';
    } else {
        $successType = 'unpaid';
    }
}

// TODO: if change payment page
if (isset($_GET['type'])) {
    // default, estimate, paid, unpaid, omnipay, change-payment
    $successType = $_GET['type'];
}
?>

<div class="l-container">
    <div class="l-shopping is-success">
        <div class="l-shopping__content">
            <div class="l-shopping-success">
                <div class="l-shopping-success__main">
                    <div class="c-success-main">
                        <div class="c-success-main__header">
                            <?= $this->element('Rshop/FrontendCart.Success/header', \compact('successType')); ?>
                        </div>

                        <div class="c-success-main__content">
                            <?= $this->element('Rshop/FrontendCart.Success/content', \compact('successType')); ?>
                        </div>

                        <div class="c-success-main__footer">
                            <?= $this->element('Rshop/FrontendCart.Success/footer', \compact('successType')); ?>
                        </div>
                    </div>
                </div>

                <?php
                if ($successType == 'default' && $hasSurvey) {
                ?>
                <div class="l-shopping-success__survey">
                    <?= $this->element('Rshop/FrontendCart.Success/survey'); ?>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

        <?php
        if ($successType == 'default') {
        ?>
        <div class="l-shopping__buttons">
            <?= $this->element('Rshop/FrontendCart.Layout/buttons_fourth'); ?>
        </div>
        <?php
        }
        ?>
    </div>
</div>