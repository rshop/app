<?php
$productGifts = $this->Cart->customProductGifts($product);
$productsCombidealInCart = $this->Cart->customProductCombideals($product);
$productCombideals = $this->Cart->customEligibleProductCombideals($product);

if ($productGifts || $productsCombidealInCart || $productCombideals) {
?>
<div class="c-cart-products__promotions">
    <?php
    if ($productGifts) {
        foreach ($productGifts as $productGift) {
        ?>
        <div class="c-product-promotion is-gift">
            <div class="c-product-promotion__image">
                <?= $this->Html->lazyEntityThumbnail($productGift, ['format' => 'shopping-cart']); ?>
            </div>

            <div class="c-product-promotion__info">
                <div class="sc-type">
                    <?= __('Darček'); ?>
                </div>

                <div class="sc-name">
                    <?= $productGift->getCartName(); ?>
                </div>
            </div>

            <div class="c-product-promotion__price">
                <div class="c-cart-products__price">
                    <div class="sc-actual is-free">
                        <?= __('Zadarmo'); ?>
                    </div>

                    <?php
                    if (isset($productGift->module_data['promotion']['original_price']) && $productGift->module_data['promotion']['original_price'] > 0) {
                    ?>
                    <div class="sc-unit">
                        <?= __('v hodnote {0}', $this->Format->price($productGift->module_data['promotion']['original_price'])); ?>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="c-product-promotion__actions"></div>
        </div>
        <?php
        }
    }

    if ($productsCombidealInCart) {
        foreach ($productsCombidealInCart as $productCombideal) {
            $isAction = $this->Cart->productPrice($productCombideal, 'old_price', 1, false) > $this->Cart->getItemPrice($productCombideal, false, 1, false, 'custom_combideal_price');
            ?>
            <div class="c-product-promotion is-combideal js-promotion-item" data-cart-id="<?= $productCombideal->getCartId(); ?>">
                <div class="c-product-promotion__image">
                    <?= $this->Html->lazyEntityThumbnail($productCombideal, ['format' => 'shopping-cart']); ?>
                </div>

                <div class="c-product-promotion__info">
                    <div class="sc-name">
                        <?= $productCombideal->getCartName(); ?>
                    </div>
                </div>

                <div class="c-product-promotion__availability">
                    <div class="c-cart-products__availability <?= $productCombideal->is_available ? 'is-available' : ''; ?>" style="color: <?= $this->modelHelper('Product')->statusColor($productCombideal); ?>">
                        <?= $this->element('Product/availability', [
                            'product' => $productCombideal,
                            'location' => 'cart-thumb',
                        ]); ?>
                    </div>
                </div>

                <div class="c-product-promotion__quantity">
                    <?php
                    // SVET-1859
                    if (false) {
                    ?>
                    <div class="c-cart-products__quantity">
                        <div class="c-form">
                            <?= $this->Cart->quantityInput($productCombideal, [
                                'max' => $this->Cart->getItemQuantity($product, false),
                                'templateVars' => [
                                    'wrapperClass' => 'js-promotion-quantity-spinner'
                                ]
                            ]); ?>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>

                <div class="c-product-promotion__price">
                    <div class="c-cart-products__price">
                        <?php
                        if ($isAction) {
                        ?>
                        <div class="sc-old">
                            <?= $this->Cart->getItemPrice($productCombideal, true, null, null, 'old_price'); ?>
                        </div>
                        <?php
                        } ?>

                        <div class="sc-actual <?= $isAction ? 'has-action' : ''; ?>">
                            <?= $this->Cart->getItemPrice($productCombideal, true, null, false, 'custom_combideal_price'); ?>
                        </div>

                        <div class="sc-unit">
                            <?= $this->Cart->getItemQuantity($productCombideal) . 'x' . $this->Cart->getItemPrice($productCombideal, true, 1, false, 'custom_combideal_price'); ?>
                        </div>
                    </div>
                </div>

                <div class="c-product-promotion__actions">
                    <div class="c-cart-products__actions">
                        <?php
                        if (!$productCombideal->is_not_saleable) {
                            echo $this->Html->link(
                                '<i class="ico ico-delete" aria-hidden="true"></i>',
                                ['urlKey' => 'Shopping.removeItem', $productCombideal->getCartId()],
                                [
                                    'title' => __('Odstrániť produkt z košíka?'),
                                    'escape' => false,
                                    'class' => 'c-btn c-btn--transparent',
                                    'onclick' => 'return Cart.removeItem(this);',
                                    'data-message' => __('Skutočne chcete odstrániť tento produkt?'),
                                    'data-confirm-btn' => __('Odstrániť'),
                                    'data-cancel-btn' => __('Zrušiť'),
                                ]
                            );
                        } ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    elseif ($productCombideals) {
    ?>
        <div class="c-product-promotion is-slider">
            <div class="sc-combideal">
                <div class="sc-combideal__title">
                    <?= __('K tomuto produktu viete získať výhodnejšie tieto produkty'); ?>
                </div>

                <div class="sc-combideal__content">
                    <?= $this->element('slider', [
                        'items' => $productCombideals,
                        'name' => 'r-slider-combideal-products',
                        'showButtons' => true,
                        'showPagination' => false,
                        'slide' => 'Product/thumb_combideal',
                        'linkedProduct' => $product,
                    ]); ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>
<?php
}
