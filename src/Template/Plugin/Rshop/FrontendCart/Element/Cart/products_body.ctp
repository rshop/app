<?php
$type = \reset($products)->product_type;
?>

<div class="c-cart-products__body" r-tab-target id="tab-<?= $type->id; ?>">
    <?php
    $key = 0;

    foreach ($products as $product) {
        $productPromotion = $product->module_data['promotion'] ?? false;
        $productName = $product->getCartName();
        $isAvailable = $product->is_available;
        $isAction = $this->Cart->productPrice($product, 'old_price', 1, false) > $this->Cart->getItemPrice($product, false, 1); ?>

        <div class="c-cart-products__row cart-product">
            <div class="c-cart-products__item is-image">
                <?= $this->Html->link(
                    $this->Html->lazyEntityThumbnail($product, [
                        'format' => 'shopping-cart',
                        'retina' => 'ret-shopping-cart'
                    ]),
                    $product->full_link,
                    ['escape' => false, 'class' => 'c-cart-products__img c-link', 'title' => __('Zobraziť detail produktu')]
                ); ?>
            </div>

            <div class="c-cart-products__item is-product">
                <div class="c-cart-products__name">
                    <div class="sc-name">
                        <?= $this->Html->link(
                            $productName,
                            $product->full_link,
                            ['escape' => false, 'class' => 'c-link', 'title' => __('Zobraziť detail produktu')]
                        ); ?>
                    </div>

                    <div class="sc-info">
                        <div class="sc-properties">
                            <div class="sc-properties__item">
                                <?= $product->model; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="c-cart-products__item is-availability">
                <div class="c-cart-products__availability <?= $isAvailable ? 'is-available' : ''; ?>" style="color: <?= $this->modelHelper('Product')->statusColor($product); ?>">
                    <?php
                    if (!$product->is_not_saleable) {
                        echo $this->element('Product/availability', [
                            'product' => $product,
                            'location' => 'cart-thumb',
                        ]);
                    } ?>
                </div>
            </div>

            <div class="c-cart-products__item is-quantity">
                <div class="c-cart-products__quantity">
                    <div class="c-form">
                        <?php
                        if ($product->is_not_saleable) {
                            echo $this->Cart->getItemQuantity($product);
                        } else {
                            echo $this->Cart->quantityInput($product, [
                                'templateVars' => [
                                    'wrapperClass' => 'js-product-quantity-spinner'
                                ]
                            ]);
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="c-cart-products__item is-price">
                <div class="c-cart-products__price">
                    <?php
                    if (!$product->is_not_saleable) {
                        if ($isAction) {
                            ?>
                        <div class="sc-old">
                            <?= $this->Cart->getItemPrice($product, true, null, null, 'old_price'); ?>
                        </div>
                        <?php
                        } ?>

                    <div class="sc-actual <?= $isAction ? 'has-action' : ''; ?>">
                        <?= $this->Cart->getItemPrice($product, ); ?>
                    </div>

                    <div class="sc-unit">
                        <?= $this->Cart->getItemQuantity($product) . 'x' . $this->Cart->getItemPrice($product, true, 1); ?>
                    </div>
                    <?php
                    } ?>
                </div>
            </div>

            <div class="c-cart-products__item is-actions">
                <div class="c-cart-products__actions">
                    <?php
                    if (!$product->is_not_saleable) {
                        echo $this->Html->link(
                            '<i class="ico ico-delete" aria-hidden="true"></i>',
                            ['urlKey' => 'Shopping.removeItem', $product->getCartId()],
                            [
                                'title' => __('Odstrániť produkt z košíka?'),
                                'escape' => false,
                                'class' => 'c-btn c-btn--transparent',
                                'onclick' => 'return Cart.removeItem(this);',
                                'data-message' => __('Skutočne chcete odstrániť tento produkt?'),
                                'data-confirm-btn' => __('Odstrániť'),
                                'data-cancel-btn' => __('Zrušiť'),
                            ]
                        );
                    } ?>
                </div>
            </div>

            <div class="c-cart-products__item is-promotion">
                <?= $this->element('Rshop/FrontendCart.Cart/product_promotions', ['product' => $product]); ?>
            </div>

            <?php
            if ($this->Format->entityName($product) == 'Products') {
                echo $this->Form->input('ec-data', [
                    'value' => $this->EcommerceTracking->encodeProductData($product),
                    'type' => 'hidden',
                    'data-ec-id' => $product->id,
                    'data-ec-item' => 'Products'
                ]);
            }
            ?>
        </div>
    <?php
    }
    ?>
</div>