<?php
$isEmptyCart = $this->Cart->isEmpty();
$savings = $this->Cart->getTotal(false, '~', 'savings');
$vouchers = $this->Cart->getTotal(false, 'voucher');
?>

<div class="c-cart-totals">
    <div class="c-cart-totals__list">
        <div class="c-cart-totals__item is-bigger">
            <div class="c-cart-totals__key">
                <?= __('Vaša cena') . ' ' . $this->Format->vatInfo(true); ?>
            </div>

            <div class="c-cart-totals__value">
                <?= $this->Cart->getTotal(true, $this->Configure->read('Cart.ProductItemsSelector'), 'total'); ?>
            </div>
        </div>

        <?php
        if ($savings) {
        ?>
        <div class="c-cart-totals__item is-red">
            <div class="c-cart-totals__key">
                <?= __('Ušetrili ste'); ?>
            </div>

            <div class="c-cart-totals__value">
                <r-span class="recap-totals__value" data-element="cart" data-type="savings">
                    <?= $this->Format->price($savings); ?>
                </r-span>
            </div>
        </div>
        <?php
        }

        if ($vouchers) {
        ?>
        <div class="c-cart-totals__item is-red">
            <div class="c-cart-totals__key">
                <?= __('Zľavový kupón'); ?>
            </div>

            <div class="c-cart-totals__value">
                <r-span class="recap-totals__value" data-element="cart" data-type="vouchers">
                    <?= $this->Format->price($vouchers); ?>
                </r-span>
            </div>
        </div>
        <?php
        }
        ?>

        <div class="c-cart-totals__item">
            <div class="c-cart-totals__key">
                <?= __('Cena') . ' ' . $this->Format->vatInfo(false); ?>
            </div>

            <div class="c-cart-totals__value">
            <?= $this->Cart->getTotal(true, $this->Configure->read('Cart.ProductItemsSelector'), 'subtotal'); ?>
            </div>
        </div>

        <?php
        if ($this->Configure->read('Rshop.pricesWithVat')) {
            foreach ($this->Cart->getTaxes(false, $this->Configure->read('Cart.ProductItemsSelector')) as $taxAmount => $tax) {
            ?>
            <div class="c-cart-totals__item">
                <div class="c-cart-totals__key">
                    <?= __('DPH {0}', $taxAmount . '%'); ?>
                </div>

                <div class="c-cart-totals__value">
                    <r-span class="recap-totals__value is-faded" data-element="cart" data-type="tax" data-id="<?= $taxAmount; ?>">
                        <?= $this->Format->price($tax->asFloat()); ?>
                    </r-span>
                </div>
            </div>
            <?php
            }
        }
        ?>
    </div>
</div>