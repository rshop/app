<?php
$isB2b = $this->Configure->read('Custom.b2b');
$type = \reset($products)->product_type;
$imgLimit = 3;

$totalQuantity = $this->Product->totalQuantity($products);
$totalPrice = $this->Product->totalPrice($products);
$totalAvailability = $this->Product->totalAvailability($products);
?>

<div class="c-cart-products__trigger js-type-trigger" r-tab-trigger data-tab-id="tab-<?= $type->id; ?>">
    <div class="c-cart-products__row">
        <div class="c-cart-products__item is-info">
            <div class="c-cart-products__info">
                <div class="sc-name">
                    <?= $type->name; ?>
                </div>

                <div class="sc-images">
                    <?php
                    $tmpCount = 0;

                    foreach ($products as $product) {
                        if (++$tmpCount > $imgLimit) {
                            break;
                        }

                        echo '<div class="sc-images__item">';
                        echo $this->Html->lazyEntityThumbnail($product, ['format' => 'shopping-cart']);
                        echo '</div>';
                    }

                    if (\count($products) > $imgLimit) {
                        echo '<div class="sc-images__item">';
                        echo '+' . (\count($products) - $imgLimit);
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="c-cart-products__item is-availability">
            <div class="c-cart-products__availability <?= $totalAvailability['isAvailable'] ? 'is-available' : 'is-unavailable'; ?>">
                <div class="availability-item is-bigger">
                    <?= $totalAvailability['text']; ?>
                </div>
            </div>
        </div>

        <div class="c-cart-products__item is-quantity">
            <div class="c-cart-products__quantity">
                <?= $totalQuantity . ' ' . __('ks'); ?>
            </div>
        </div>

        <div class="c-cart-products__item is-price">
            <div class="c-cart-products__price">
                <div class="sc-actual js-type-total">
                    <?= $this->Format->price($totalPrice); ?>
                </div>
            </div>
        </div>

        <div class="c-cart-products__item is-actions">
            <div class="c-cart-products__actions">
                <button type="button" class="c-btn c-btn--transparent">
                    <?= $this->Html->ico('chevron-down'); ?>
                </button>
            </div>
        </div>
    </div>
</div>