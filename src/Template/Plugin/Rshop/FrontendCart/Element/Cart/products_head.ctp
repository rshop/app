<div class="c-cart-products__head">
    <div class="c-cart-products__row">
        <div class="c-cart-products__item is-image">
            <?= __('Produkt'); ?>
        </div>

        <div class="c-cart-products__item is-product">
        </div>

        <div class="c-cart-products__item is-availability">
            <?= __('Stav'); ?>
        </div>

        <div class="c-cart-products__item is-quantity">
            <?= __('Množstvo'); ?>
        </div>

        <div class="c-cart-products__item is-price">
            <?= __('Cena') . ' ' . $this->Format->vatInfo($isB2b); ?>
        </div>

        <div class="c-cart-products__item is-delete">
        </div>
    </div>
</div>