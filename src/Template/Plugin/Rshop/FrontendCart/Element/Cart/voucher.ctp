<?php
$voucherCodes = $this->Cart->getItems('voucher');
?>

<div class="c-cart-voucher <?= $voucherCodes ? 'has-voucher' : ''; ?>">
    <div class="c-cart-voucher__title <?= $voucherCodes ? 'has-voucher' : 'is-empty'; ?>" <?= !$voucherCodes ? 'r-toggle-trigger="cart-vouchers"' : ''; ?>>
        <?php
        if (!$voucherCodes) {
            echo $this->Html->ico('percentage-circle');
            echo '<span class="sc-text">' . __('Máte zľavový kupón?') . '</span>';
            echo '<span class="sc-link">' . __('Zadajte kód') . '</span>';
        } else {
            echo $this->Html->ico('percentage-circle');
            echo '<span class="sc-text">' . __('Váš zľavový kupón') . ':</span>';
        }
        ?>
    </div>

    <r-toggle class="c-cart-voucher__form <?= $voucherCodes ? 'is-active' : ''; ?>" id="cart-vouchers">
        <?php
        if ($voucherCodes) {
            foreach ($voucherCodes as $voucherCode) {
                $voucher = $voucherCode->voucher;
            ?>
            <div class="c-cart-voucher__code">
                <span class="sc-code">
                    <?= $voucherCode->code; ?>

                    <small class="sc-code__value">
                        (-<?= $voucher->discount_type == 'percentage' ? $voucher->discount_value . '%' : $this->Format->price($voucher->discount_value); ?>)
                    </small>
                </span>

                <a href="<?= $this->Url->build(['urlKey' => 'Shopping.removeVoucher', $voucherCode->getCartId()]); ?>" class="c-btn c-btn--close remove-coupon" title="<?= __('Odstrániť zľavový kupón'); ?>">
                    <?= $this->Html->ico('cross'); ?>
                </a>
            </div>
            <?php
            }
        } else {
            echo $this->Form->create(null, [
                'url' => $this->Url->build(['urlKey' => 'Shopping.useVoucher'], true),
                'name' => 'vouchers',
                'ajax' => false,
                'cleanInputs' => true,
                'class' => 'c-form js-vouchers-form'
            ]); ?>
                <div class="c-form__item c-form__item--text">
                    <?= $this->Form->input('voucher', [
                        'label' => false,
                        'placeholder' => __('Napíšte kód'),
                        'templateVars' => ['class' => 'c-form__input'],
                        'required' => true,
                        'autocomplete' => 'off'
                    ]); ?>
                </div>

                <?= $this->Form->button(__('Použiť'), [
                    'type' => 'submit',
                    'class' => 'c-btn c-btn--primary'
                ]); ?>
            <?php
            echo $this->Form->end();
        }
        ?>
    </r-toggle>
</div>
