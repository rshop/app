<?php

use Cake\Utility\Hash;
use Riesenia\Cart\CartItemInterface;

$isB2b = $this->Configure->read('Custom.b2b');
$cartProducts = $this->Cart->getItems('product');

$cartProducts = \array_filter($cartProducts, function (CartItemInterface $item) {
    $giftsPerProduct = $this->Cart->cart()->giftProductOrigins;

    if ($giftsPerProduct) {
        foreach ($giftsPerProduct as $product => $gifts) {
            if (\in_array($item->getCartId(), $gifts)) {
                return false;
            }
        }
    }

    return !$item->getCombiDealParent();
});

$sortedProducts = false;
$productsLimit = 10;

if (\count($cartProducts) > $productsLimit || isset($_GET['b2b'])) {
    $sortedProducts = Hash::combine($cartProducts, '{n}.id', '{n}', '{n}.product_type_id');
}

// https://riesenia.atlassian.net/browse/SVET-1827
if (!isset($_GET['b2b'])) {
    $sortedProducts = false;
}
?>

<div class="c-cart-products">
    <?php
    if ($sortedProducts && \count($sortedProducts) > 1 || isset($_GET['b2b'])) {
    ?>
        <r-tabs allow-trigger-close>
            <?php
            foreach ($sortedProducts as $typesGroup) {
            ?>
                <div class="c-cart-products__box">
                    <?php
                    echo $this->element('Rshop/FrontendCart.Cart/products_trigger', ['products' => $typesGroup]);
                    echo $this->element('Rshop/FrontendCart.Cart/products_body', ['products' => $typesGroup]); ?>
                </div>
            <?php
            } ?>
        </r-tabs>
    <?php
    } else {
        echo $this->element('Rshop/FrontendCart.Cart/products_head');
        echo $this->element('Rshop/FrontendCart.Cart/products_body', ['products' => $cartProducts]);
    }
    ?>
</div>