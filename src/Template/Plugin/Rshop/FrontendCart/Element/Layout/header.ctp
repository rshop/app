<?php
$logoSrc = 'logos/' . $this->Configure->read('Eshop.Project.name') . '-lower.svg';
$this->Html->preload('/img/' . $logoSrc, ['as' => 'image']);
?>

<header class="l-header">
    <?= $this->element('Rshop/Frontend.Layout/header_messages', ['isShopping' => true]); ?>

    <div class="l-header__top">
        <div class="l-container">
            <div class="c-header-top">
                <div class="c-header-top__logo">
                    <a href="<?= $this->Url->build('/'); ?>" title="<?= __('Späť na nákup'); ?>" class="c-link">
                        <?= $this->Html->image($logoSrc, ['class' => 'c-img', 'alt' => $this->Configure->read('Rshop.store.name')]); ?>
                    </a>

                    <div class="cxs-is-hidden">
                        <i class="ico ico-arrow-left"></i>
                        <span class="sc-text"><?= __('Späť na nákup'); ?></span>
                    </div>
                </div>

                <div class="c-header-top__title">
                    <h1 class="sc-title">
                        <?= __('Nákupný košík'); ?>
                    </h1>

                    <div class="sc-subtitle">
                        <?= $this->Html->ico('shield'); ?>
                        <?= __('váš nákup je bezpečný'); ?>
                    </div>
                </div>

                <div class="c-header-top__info">
                    <div class="c-header-info">
                        <div class="c-header-info__title">
                            <?= __('Potrebujete poradiť s nákupom?'); ?>
                        </div>

                        <div class="c-header-info__contact">
                            <?= $this->Html->link(
                                $this->Configure->text('shopping.infoPhone'),
                                'tel:' . $this->Configure->text('shopping.infoPhone'),
                                ['class' => 'c-link c-link--secondary']
                            ); ?>

                            <div class="sc-text">
                                <?php //TODO: logika?>
                                <div class="c-indicator"></div>
                                <?= $this->Configure->text('shopping.infoPhoneText'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
