<div class="c-shopping-buttons is-fourth-step">
    <div class="c-shopping-buttons__item">
        <?= $this->Html->link(
            '<span class="sc-text">' . __('Prejsť na úvodnú stránku') . '</span>',
            $this->Url->build('/'),
            [
                'class' => 'c-btn c-btn--cart',
                'escape' => false,
                'append' => $this->Html->ico('chevron-right')
            ]
        ); ?>
    </div>
</div>