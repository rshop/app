<div class="c-shopping-buttons is-second-step">
    <div class="c-shopping-buttons__item">
        <?= $this->Html->link(
            '<span class="sc-text">' . __('Späť k nákupu') . '</span>',
            ['urlKey' => 'Shopping.cart'],
            [
                'class' => 'c-btn c-btn--ghost',
                'escape' => false,
                'prepend' => $this->Html->ico('chevron-left')
            ]
        ); ?>
    </div>

    <div class="c-shopping-buttons__item">
        <button type="submit" class="c-btn c-btn--cart js-shopping-form-submit hidden" onclick="$('.js-form-shopping').submit()">
            <span class="sc-text">
                <?= __('Pokračovať na dopravu a platbu'); ?>
            </span>

            <?= $this->Html->ico('chevron-right'); ?>
        </button>
    </div>
</div>