<?php
$isEmptyCart = $this->Cart->isEmpty();
?>

<div class="c-shopping-buttons is-first-step">
    <div class="c-shopping-buttons__item">
        <?= $this->Html->link(
            '<span class="sc-text">' . __('Pokračovať v nákupe') . '</span>',
            $this->Url->build('/'),
            [
                'class' => 'c-btn c-btn--ghost',
                'escape' => false,
                'prepend' => $this->Html->ico('chevron-left')
            ]
        ); ?>
    </div>

    <?php
    if (!$isEmptyCart) {
    ?>
    <div class="c-shopping-buttons__item">
        <?= $this->Form->create(null, ['id' => 'cart', 'class' => 'js-form-step-1']); ?>
            <button type="submit" class="c-btn c-btn--cart">
                <span class="sc-text">
                    <?= __('Pokračovať na fakturačné a dodacie údaje'); ?>
                </span>

                <?= $this->Html->ico('chevron-right'); ?>
            </button>

            <?= $this->Form->input('is_estimate', ['type' => 'hidden', 'value' => '0']); ?>
        <?= $this->Form->end(); ?>
    </div>
    <?php
    }
    ?>
</div>