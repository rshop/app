<div class="c-shopping-buttons is-third-step">
    <div class="c-shopping-buttons__item">
        <?= $this->Html->link(
            '<span class="sc-text">' . __('Späť k nákupu') . '</span>',
            ['urlKey' => 'Shopping.form'],
            [
                'class' => 'c-btn c-btn--ghost',
                'escape' => false,
                'prepend' => $this->Html->ico('chevron-left')
            ]
        ); ?>
    </div>

    <div class="c-shopping-buttons__item">
        <div class="sc-note">
            <?php
            $linkObchPodm = $this->Html->link(
                __('naše podmienky'),
                ['urlKey' => 'Articles.ObchPodmienky'],
                [
                    'class' => 'c-link',
                    'target' => '_blank'
                ]
            );

            $linkOsobneUdaje = $this->Html->link(
                __('spracovaním osobných údajov'),
                $this->Url->build('/ochrana-sukromia'),
                [
                    'class' => 'c-link',
                    'target' => '_blank'
                ]
            );

            echo __('Kliknutím na “dokončiť objednávku”, potrvzdujete že ste si prečítali {0} a súhlasíte so {1}', $linkObchPodm, $linkOsobneUdaje);
            ?>
        </div>

        <button type="submit" class="c-btn c-btn--cart js-shopping-modules-submit" onclick="$('.js-form-cart-modules').submit()">
            <span class="sc-text">
                <?= __('Dokončiť a objednať tovar'); ?>

                <small>
                    (<?= __('s povinnosťou platby'); ?>)
                </small>
            </span>
        </button>
    </div>
</div>