<?php
$selector = $this->Configure->read('Cart.ProductItemsSelector');
$savings = $this->Cart->getTotal(false, '~', 'savings');
$vouchers = $this->Cart->getTotal(false, 'voucher');
$products = $this->Cart->getItems('product,present');
$presentPacking = $this->Cart->customOrderPresentPacking();
$donation = $this->Cart->customOrderDonation();

$productsLimit = 5;
$moreText = '+' . (\count($products) - $productsLimit) . ' ' . $this->Format->plural(__('ďalši produkt'), __('ďalšie produkty'), __('ďalších produktov'), \count($products) - $productsLimit);
?>

<div class="c-recap">
    <div class="c-recap__trigger" r-toggle-trigger="recap-products">
        <div class="sc-text">
            <?= __('Prehľad tovaru v košíku'); ?>

            <?= $this->Html->ico('chevron-down'); ?>
        </div>

        <div class="sc-price">
            <?= $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(true, $selector . ',shipping,payment', 'total') : $this->Cart->getSubtotal(true, $selector . ',shipping,payment'); ?>
        </div>
    </div>

    <div class="c-recap__title">
        <?= __('Produkty, ktoré máte v košíku'); ?>
    </div>

    <r-more-items
        class="c-recap__products"
        data-more-text="<?= $moreText; ?>"
        data-less-text="<?= __('zobraziť menej'); ?>"
    >
        <r-toggle class="c-recap-products" query="cxs;xs" r-more-items-target id="recap-products">
            <div class="c-recap-products__list">
                <?php
                foreach ($products as $product) {
                    $productName = $product->getCartName();
                ?>
                <div class="c-recap-products__item">
                    <div class="c-recap-products__img">
                        <?= $this->Html->lazyEntityThumbnail($product, [
                            'format' => 'cxs',
                            'retina' => 'xs'
                        ]); ?>
                    </div>

                    <div class="c-recap-products__info">
                        <div class="sc-name">
                            <?= $productName; ?>
                        </div>

                        <div class="sc-info">
                            <div class="sc-properties">
                                <div class="sc-properties__item">
                                    <?= $product->model; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="c-recap-products__quantity">
                        <?= $this->Cart->getItemQuantity($product) . ' ' . __('ks'); ?>
                    </div>

                    <div class="c-recap-products__price">
                        <?php
                        if (!$product->is_not_saleable) {
                            if ($product->getCombiDealParent()) {
                                echo $this->Format->price($product->getCartQuantity() * $product->custom_combideal_price);
                            } else {
                                echo $this->Cart->getItemPrice($product);
                            }
                        } else {
                            echo '<span class="h-is-green">' . __('Zadarmo') . '</span>';
                        }
                        ?>
                    </div>

                </div>
                <?php
                }
                ?>
            </div>

            <?php
            if (\count($products) > $productsLimit) {
            ?>
            <div class="c-recap-products__more">
                <button type="button" class="c-btn c-btn--ghost" r-more-items-trigger>
                    <span class="sc-text" r-more-items-trigger-text>
                        <?= $moreText; ?>
                    </span>

                    <?= $this->Html->ico('chevron-down'); ?>
                </button>
            </div>
            <?php
            }
            ?>
        </r-toggle>
    </r-more-items>

    <div class="c-recap__summary">
        <div class="c-recap-summary">
            <div class="c-recap-summary__list">
                <div class="c-recap-summary__item js-cart-recap-packing-holder <?= $presentPacking ? '' : 'hidden'; ?>">
                    <div class="sc-key">
                        <span><?= __('Venovanie'); ?></span>
                    </div>

                    <div class="sc-value js-cart-recap-packing">
                        <?php
                        $presentPackingTotal = $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(false, 'present_packing') : $this->Cart->getSubtotal(false, 'present_packing');

                        echo $this->Format->price($presentPackingTotal);
                        ?>
                    </div>
                </div>

                <div class="c-recap-summary__item">
                    <div class="sc-key">
                        <?= __('Medzisúčet'); ?>
                    </div>

                    <div class="sc-value js-recap-totals">
                        <?= $this->Configure->read('Rshop.pricesWithVat') ? $this->Format->price($this->Cart->getTotal(false, 'product,service', 'total')) : $this->Format->price($this->Cart->getSubtotal(false, 'product,service')); ?>
                    </div>
                </div>

                <div class="c-recap-summary__item js-cart-recap-donation-holder <?= $donation ? '' : 'hidden'; ?>">
                    <div class="sc-key">
                        <span><?= __('Spoločne pomáhame'); ?></span>
                    </div>

                    <div class="sc-value js-cart-recap-donation">
                        <?php
                        $donationTotal = $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(false, 'donation') : $this->Cart->getSubtotal(false, 'donation');

                        echo $this->Format->price($donationTotal);
                        ?>
                    </div>
                </div>

                <?php
                if ($savings) {
                ?>
                <div class="c-recap-summary__item">
                    <div class="sc-key">
                        <?= __('Ušetrili ste'); ?>:
                    </div>

                    <div class="sc-value is-red">
                        <?= $this->Format->price($savings); ?>
                    </div>
                </div>
                <?php
                }

                if ($vouchers) {
                ?>
                <div class="c-recap-summary__item">
                    <div class="sc-key">
                        <?= __('Zľavový kupón'); ?>:
                    </div>

                    <div class="sc-value is-red">
                        <?= $this->Format->price($vouchers); ?>
                    </div>
                </div>
                <?php
                }
                ?>

                <div class="c-recap-summary__item js-cart-recap-shipping-holder <?= !isset($order->shipping_id) ? 'hidden' : ''; ?>">
                    <div class="sc-key">
                        <span><?= __('Doprava'); ?></span>
                        <small class="js-cart-recap-shipping-text"><?= '(' . $order->shipping_method . ')'; ?></small>
                    </div>

                    <div class="sc-value js-cart-recap-shipping <?= $this->Cart->getTotal(false, 'shipping') == 0 && isset($order->shipping_id) ? 'is-green' : ''; ?>" data-free="<?= __('Zadarmo'); ?>" data-empty="-">
                        <?php
                        $shippingTotal = $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(false, 'shipping') : $this->Cart->getSubtotal(false, 'shipping');
                        $isFreeText = isset($order->shipping_id) ? __('Zadarmo') : '-';

                        echo $shippingTotal == 0 ? $isFreeText : $this->Format->price($shippingTotal);
                        ?>
                    </div>
                </div>

                <div class="c-recap-summary__item js-cart-recap-payment-holder <?= !isset($order->payment_id) ? 'hidden' : ''; ?>">
                    <div class="sc-key">
                        <span><?= __('Platba'); ?></span>
                        <small class="js-cart-recap-payment-text"><?= '(' . $order->payment_method . ')'; ?></small>
                    </div>

                    <div class="sc-value js-cart-recap-payment <?= $this->Cart->getTotal(false, 'payment') == 0 && isset($order->payment_id) ? 'is-green' : ''; ?>" data-free="<?= __('Zadarmo'); ?>" data-empty="-">
                        <?php
                        $paymentTotal = $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(false, 'payment') : $this->Cart->getSubtotal(false, 'payment');
                        $isFreeText = isset($order->payment_id) ? __('Zadarmo') : '-';

                        echo $paymentTotal == 0 ? $isFreeText : $this->Format->price($paymentTotal);
                        ?>
                    </div>
                </div>
            </div>

            <div class="c-recap-summary__list">
                <div class="c-recap-summary__item is-bigger">
                    <div class="sc-key">
                        <?= __('Celková suma') . ' ' . $this->Format->vatInfo(); ?>
                    </div>

                    <div class="sc-value">
                        <?= $this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(true, $selector . ',shipping,payment,present_packing', 'total') : $this->Cart->getSubtotal(true, $selector . ',shipping,payment,present_packing'); ?>
                    </div>
                </div>

                <div class="c-recap-summary__item is-faded">
                    <div class="sc-key">
                        <?= !$this->Configure->read('Rshop.pricesWithVat') ? __('Suma s DPH') : __('Suma bez DPH'); ?>
                    </div>

                    <div class="sc-value">
                        <?= !$this->Configure->read('Rshop.pricesWithVat') ? $this->Cart->getTotal(true, $selector . ',shipping,payment,present_packing', 'total') : $this->Cart->getSubtotal(true, $selector . ',shipping,payment,present_packing'); ?>
                    </div>
                </div>

                <?php
                if ($this->Configure->read('Rshop.pricesWithVat')) {
                    foreach ($this->Cart->getTaxes(false, $this->Configure->read('Cart.ProductItemsSelector') . ',present_packing') as $taxAmount => $tax) {
                        ?>
                    <div class="c-recap-summary__item is-faded">
                        <div class="sc-key">
                            <?= __('DPH {0}', $taxAmount . '%'); ?>
                        </div>

                        <r-span class="sc-value" data-element="cart" data-type="tax" data-id="<?= $taxAmount; ?>">
                            <?= $this->Format->price($tax->asFloat()); ?>
                        </r-span>
                    </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>