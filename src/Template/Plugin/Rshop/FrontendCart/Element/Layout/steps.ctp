<?php
$steps = [
    [
        'class' => 'first',
        'url' => $this->Url->build(['urlKey' => 'Shopping.cart']),
        'title' => [
            'desktop' => __('Prehľad košíka'),
            'mobile' => __('Prehľad'),
        ]
    ], [
        'class' => 'second',
        'url' => $this->Url->build(['urlKey' => 'Shopping.form']),
        'title' => [
            'desktop' => __('Fakturačné a dodacie údaje'),
            'mobile' => __('Údaje'),
        ]
    ], [
        'class' => 'third',
        'url' => $this->Url->build(['urlKey' => 'Shopping.modules']),
        'title' => [
            'desktop' => __('Doprava a platba'),
            'mobile' => __('Doprava a platba'),
        ]
    ]
];
?>

<div class="c-steps c-steps--<?= $actualStep; ?>">
    <?php
    foreach ($steps as $key => $step) {
    ?>
    <div class="c-steps__item c-steps__item--<?= $step['class']; ?>">
        <a class="c-link" href="<?= $step['url']; ?>" title="<?= $step['title']['desktop']; ?>">
            <span class="sc-number">
                <span><?= $key + 1; ?></span>
            </span>

            <span class="sc-text mobile-is-hidden">
                <?= $step['title']['desktop']; ?>
            </span>

            <span class="sc-text desktop-is-hidden">
                <?= $step['title']['mobile']; ?>
            </span>
        </a>
    </div>
    <?php
    }
    ?>
</div>
