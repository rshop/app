<?php
$linksBox = $this->cell('Rshop/Frontend.Box', ['shopping-links'], [
    'customTemplate' => 'simple_links',
    'cache' => [
        'config' => 'rshop_boxes',
        'key' => 'shopping_links'
    ]
])->__toString();
?>

<footer class="l-footer">
    <?php
    if ($linksBox) {
    ?>
    <div class="l-footer__links">
        <div class="l-container">
            <div class="c-footer-links">
                <?= $linksBox; ?>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

    <div class="l-footer__bottom">
        <div class="l-container">
            <div class="c-footer-bottom">
                <div class="c-footer-bottom__copyright">
                    <div class="sc-copy">
                        Copyright © <?= $this->Configure->read('Rshop.store.name') . ' ' . \date('Y') . ' <span class="sc-copy__delimiter">|</span> ' . __('Všetky práva vyhradené'); ?>
                    </div>

                    <div class="sc-recaptcha">
                        <?php
                        $googlePrivacyLink = $this->Html->link(__('Pravidlá ochrany osobných údajov'), 'https://policies.google.com/privacy', ['class' => 'c-link c-link--inherit', 'target' => '_blank']);
                        $googleTermsLink = $this->Html->link(__(' Zmluvné podmienky'), 'https://policies.google.com/terms', ['class' => 'c-link c-link--inherit', 'target' => '_blank']);

                        echo __('Táto stránka je chránená pomocou reCAPTCHA a uplatňujú sa') . ' ' . $googlePrivacyLink . ' ' . __('spoločnosti Google a ich') . ' ' . $googleTermsLink . '.';
                        ?>
                    </div>
                </div>

                <div class="c-footer-bottom__created">
                    <?= __('Tvorba výkonných internetových obchodov od {0}', '<a href="https://www.riesenia.com?utm_source=rshop&utm_medium=footer" class="c-link c-link--secondary" target="_blank"><strong>RIESENIA</strong></a>'); ?>
                </div>
            </div>
        </div>
    </div>
</footer>