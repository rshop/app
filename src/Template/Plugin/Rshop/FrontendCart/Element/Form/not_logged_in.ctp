<?php
// TODO: podmienka kedy co zobrazit
// ak existuju data z odoslaneho formu (login, reg) tak konkretny form, inak default
// $activeSection -> default, login, registration
$activeSection = isset($customer['email']) ? 'registration' : 'default';
?>

<div class="l-shopping-form js-section-switcher" data-section="<?= $activeSection; ?>">
    <div class="l-shopping-form__section is-login">
        <div class="c-form-section without-bottom-margin">
            <div class="c-form-section__title">
                <?= __('Začnite zadaním vášho e-mailu'); ?>
            </div>

            <div class="c-form-section__text">
                <p>
                    <?= __('Ste už zaregistrovaný, alebo nakupujete u nás prvý krát? Zadajte najprv {0}váš e-mail a my to overíme{1}.', '<strong>', '</strong>'); ?>
                </p>

                <p>
                    <?php
                    $betterPricesLink = $this->Html->link(
                        '<strong>' . __('ešte lepšie ceny') . '</strong>',
                        ['urlKey' => 'Articles.VernostnyProgram'],
                        [
                            'class' => 'c-link',
                            'escape' => false,
                        ]
                    );
                    echo __('Ako registrovaný zákazník viete získať {0} {1}a veľa ďalších výhod{2}.', $betterPricesLink, '<strong>', '</strong>');
                    ?>
                </p>
            </div>

            <div class="c-form-section__messages">
                <div class="c-msg c-msg--success">
                    <?= $this->Html->ico('info-pin'); ?>

                    <span class="sc-text">
                        <?= __('Tento e-mail máme u nás už zaregistrovaný. Zadajte heslo, prihláste sa a pokračujte v objednávke.'); ?>
                    </span>
                </div>
            </div>
        </div>

        <?= $this->element('Rshop/Frontend.Form/login', ['isShopping' => true]); ?>
    </div>

    <div class="l-shopping-form__section is-registration">
        <div class="c-form-section without-bottom-margin">
            <div class="c-form-section__messages">
                <div class="c-msg c-msg--info">
                    <?= $this->Html->ico('info-pin'); ?>

                    <span class="sc-text">
                        <?= __('Tento e-mail ešte neregistrujeme. Vitajte u nás. Zadajte potrebné údaje a pokračujte v objednávke.'); ?>
                    </span>
                </div>
            </div>
        </div>

        <?= $this->element('Rshop/FrontendCart.Form/register'); ?>
    </div>
</div>