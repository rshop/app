<div class="c-info-box">
    <div class="c-info-box__item">
        <?php
        echo '<div><strong data-type="name">' . $order->{$type . '_name'} . '</strong></div>';
        echo '<div><span data-type="phone">' . ($type == 'shipping' ? $order->shipping_phone : $order->customer_phone) . '</span></div>';
        echo '<div>' . $order->customer_email . '</div>';
        ?>
    </div>

    <div class="c-info-box__item">
        <?php
        if ($type == 'shipping') {
            echo '<div><span data-type="company">' . ($order->shipping_company) . '</span></div>';
        }

        echo '<div><span data-type="address">' . $order->{$type . '_address'} . '</span></div>';
        echo '<div><span data-type="post_code">' . $order->{$type . '_post_code'} . '</span> <span data-type="city">' . $order->{$type . '_city'} . '</span></div>';
        echo '<div><span data-type="country">' . $order->{$type . '_country'}->name . '</span></div>';
        ?>
    </div>

    <?php
    if ($type == 'billing') {
    ?>
    <div class="c-info-box__item js-address-box-company <?= (!$order->billing_org_num && !$order->billing_tax_num && !$order->billing_vat_num) ? 'is-not-visible' : ''; ?>">
        <?php
        echo '<div>' . __('IČO') . ': <strong data-type="org_num">' . $order->billing_org_num . '</strong></div>';
        echo '<div>' . __('DIČ') . ': <span data-type="tax_num">' . $order->billing_tax_num . '</span></div>';
        echo '<div>' . __('IČ DPH') . ': <span data-type="vat_num">' . $order->billing_vat_num . '</span></div>';
        ?>
    </div>
    <?php
    }
    ?>
</div>