<?php
$benefitsBox = $this->cell('Box', ['shopping-benefits-registred'], [
    'customTemplate' => 'simple_list_items',
    'cache' => [
        'config' => 'rshop_boxes',
        'key' => 'shopping_benefits_registered'
    ]
])->__toString();

$formOptions = [
    'novalidate' => 'novalidate',
    'idPrefix' => 'customer',
    'data-formtype' => 'registration',
    'url' => $this->Url->build(['urlKey' => 'Shopping.registration'], true),
    'name' => 'customer-form',
    'class' => 'c-form js-form-shopping js-form-shopping-register',
    'location' => 'cart-order-form',
    'data-locale' => $this->Format->locale()
];

$registeredInputIsChecked = !isset($this->request->data['registered']) && $order->registered === null ? false : @$this->request->data['registered'] || $order->registered;
$billingInputIsChecked = !isset($this->request->data['billing_same_as_shipping']) && $order->billing_same_as_shipping === null ? true : @$this->request->data['billing_same_as_shipping'] || $order->billing_same_as_shipping;

echo $this->Form->create($customer, $formOptions);
?>
    <div class="c-form-section has-bottom-border">
        <?php
        echo $this->Form->input('email', [
            'placeholder' => false,
            'required' => true,
            'label' => __('Emailová adresa'),
            'templateVars' => [
                'class' => 'js-input-email',
                'inputAddon' => $this->Html->ico('cross-circle', ['class' => 'js-clear-input']),
                'inputClass' => 'has-addon'
            ],
        ]);
        ?>

        <div class="c-form__box">
            <?= $this->Form->input('registered', [
                'placeholder' => false,
                'label' => __('Chcem sa zaregistrovať a získať výhody'),
                'data-box' => 'registration_user_box',
                'type' => 'checkbox',
                'checked' => $registeredInputIsChecked,
                'default' => true,
                'custom' => true,
                'r-toggle-trigger' => 'shopping-registration',
            ]); ?>

            <?php
            if ($benefitsBox) {
            ?>
            <div class="c-list">
                <div class="c-list__title">
                    <?= __('Získajte výhody registrovaného zákazníka'); ?>
                </div>

                <ul class="c-list__list">
                    <?= $benefitsBox; ?>
                </ul>
            </div>
            <?php
            }
            ?>
        </div>

        <r-toggle class="c-form__toggle <?= $registeredInputIsChecked ? 'is-active' : ''; ?>" id="shopping-registration">
            <?= $this->Form->input('customer_logins.0.password', [
                'placeholder' => false,
                'label' => __('Heslo'),
                'type' => 'password',
                'required' => true,
                'templateVars' => [
                    'inputAddon' => '<span class="js-toggle-password">' . __('Zobraziť') . '</span>',
                    'inputClass' => 'has-long-addon',
                    'inputNote' => __('Odporúčame vám z hľadiska bezpečnosti, aby ste si zvolili {0}silné heslo, ktoré má aspoň 8 znakov a obsahuje číslo a znak{1} (ako napr. &,#,@ ...)', '<strong>', '</strong>')
                ]
            ]); ?>
        </r-toggle>

        <?= $this->element('Rshop/Frontend.Form/company_inputs', ['showFinstatField' => true]); ?>
    </div>

    <div class="c-form-section has-bottom-border">
        <div class="c-form-section__title has-subtitle">
            <?= __('Dodacie údaje'); ?>
        </div>

        <div class="c-form-section__subtitle">
            <?= __('Údaje potrebné k doručeniu tovaru (ďalšie adresy si môžete pridať neskôr)'); ?>
        </div>

        <?= $this->element('Rshop/Frontend.Form/shipping_address'); ?>
    </div>

    <div class="c-form-section has-bottom-border">
        <div class="c-form-section__title has-subtitle">
            <?= __('Fakturačné údaje'); ?>
        </div>

        <div class="c-form-section__subtitle">
            <?= __('Adresa, ktorá bude uvedená na daňovom doklade'); ?>
        </div>

        <div class="c-form__box">
            <?= $this->Form->input('billing_same_as_shipping', [
                'placeholder' => false,
                'label' => __('Fakturačná adresa je rovnaká ako dodacia'),
                'type' => 'checkbox',
                'checked' => $billingInputIsChecked,
                'custom' => true,
                'r-toggle-trigger' => 'shopping-billing',
            ]); ?>
        </div>

        <r-toggle class="c-form__toggle <?= !$billingInputIsChecked ? 'is-active' : ''; ?>" id="shopping-billing">
            <?= $this->element('Rshop/Frontend.Form/billing_address', ['showCompanyInputs' => false]); ?>
        </r-toggle>
    </div>

    <div class="c-form-section without-bottom-margin">
        <div class="c-form__item c-form__item--textarea">
            <div class="c-form__label">
                <span class="sc-text">
                    <?= $order->is_estimate ? __('Poznámka k dopytu') : __('Poznámka k objednávke'); ?>
                </span>

                <span class="sc-label-addon">
                    <?= __('nepovinné'); ?>
                </span>
            </div>

            <?= $this->Form->textarea('customer_note', [
                'label' => $order->is_estimate ? __('Poznámka k dopytu') : __('Poznámka k objednávke'),
                'placeholder' => false
            ]); ?>
        </div>
    </div>
<?php
echo $this->Form->end();
