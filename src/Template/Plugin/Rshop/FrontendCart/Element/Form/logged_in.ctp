<?php
$customerAddresses = $this->Auth->info('customer_addresses');
$pickedAddressId = $this->Cart->getOrder()->customer_address_id;

$addressIndex = null;

if ($customerAddresses) {
    foreach ($customerAddresses as $index => $address) {
        if ($address['id'] == $pickedAddressId) {
            $addressIndex = $index;

            break;
        }
    }
}
?>

<div class="l-shopping-form js-section-switcher" data-section="registration">
    <div class="l-shopping-form__section is-registration">
        <?= $this->Form->create($order, [
            'novalidate' => 'novalidate',
            'idPrefix' => 'order',
            'data-formtype' => 'order',
            'url' => $this->Url->build(['urlKey' => 'Shopping.form'], true),
            'name' => 'cart',
            'class' => 'c-form js-form-shopping js-form-shopping-logged',
            'location' => 'cart-order-form-logged',
            'data-locale' => $this->Format->locale(),
            'data-update-action' => $this->Url->build(['urlKey' => 'Shopping.updateOrder']),
        ]); ?>
        <div class="c-form-section">
            <div class="c-form-section__title has-subtitle">
                <?= __('Dodacie údaje'); ?>
            </div>

            <div class="c-form-section__subtitle">
                <?= __('Údaje a adresa potrebné k doručeniu tovaru'); ?>
            </div>

            <div class="c-form-section__box js-address-box" data-id="<?= 'shipping-' . $addressIndex; ?>">
                <?= $this->element('Rshop/Frontend.Form/info_box', ['entity' => $order, 'type' => 'shipping', 'addressId' => $addressIndex]); ?>

                <?php
                if ($addressIndex !== null) {
                ?>
                    <button type="button" class="c-link c-link--secondary js-address-box-edit" data-toggle="modal" data-target="#shipping-address-modal" data-href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'editShippingAddress', '?' => ['updateOrder' => 1], $addressIndex, 'plugin' => null]); ?>">
                        <?= $this->Html->ico('edit'); ?>

                        <span class="sc-text">
                            <?= __('Upraviť'); ?>
                        </span>
                    </button>
                <?php
                } else {
                ?>
                    <button type="button" class="c-link c-link--secondary js-address-box-edit" data-toggle="modal" data-target="#shipping-address-modal" data-href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'editShippingAddress', '?' => ['updateOrder' => 1], 'plugin' => null]); ?>">
                        <?= $this->Html->ico('edit'); ?>

                        <span class="sc-text">
                            <?= __('Upraviť'); ?>
                        </span>
                    </button>
                <?php
                } ?>
            </div>

            <?php
            if (\count($customerAddresses) > 0) {
            ?>
                <div class="c-form-section__link">
                    <button type="button" class="c-btn c-btn--ghost" data-toggle="modal" data-target="#select-shipping-modal" data-href="<?= $this->Url->build('/select-shipping-address'); ?>">
                        <span class="sc-text">
                            <?= __('Vybrať inú adresu'); ?>
                        </span>

                        <span class="sc-count">
                            (<?= \count($customerAddresses); ?>)
                        </span>
                    </button>
                </div>
            <?php
            }
            ?>
        </div>

        <div class="c-form-section has-bottom-border">
            <div class="c-form-section__title has-subtitle">
                <?= __('Fakturačné údaje'); ?>
            </div>

            <div class="c-form-section__subtitle">
                <?= __('Adresa a údaje, ktoré budú uvedené na daňovom doklade'); ?>
            </div>

            <div class="c-form-section__box js-address-box" data-id="billing-0">
                <?= $this->element('Rshop/Frontend.Form/info_box', ['entity' => $order, 'type' => 'billing']); ?>

                <button type="button" class="c-link c-link--secondary js-address-box-edit" data-toggle="modal" data-target="#billing-address-modal" data-href="<?= $this->Url->build('edit-order-billing-address'); ?>">
                    <?= $this->Html->ico('edit'); ?>

                    <span class="sc-text">
                        <?= __('Upraviť'); ?>
                    </span>
                </button>
            </div>
        </div>

        <div class="c-form-section without-bottom-margin">
            <div class="c-form__item c-form__item--textarea">
                <div class="c-form__label">
                    <span class="sc-text">
                        <?= $order->is_estimate ? __('Poznámka k dopytu') : __('Poznámka k objednávke'); ?>
                    </span>

                    <span class="sc-label-addon">
                        <?= __('nepovinné'); ?>
                    </span>
                </div>

                <?= $this->Form->textarea('customer_note', [
                    'label' => $order->is_estimate ? __('Poznámka k dopytu') : __('Poznámka k objednávke'),
                    'placeholder' => false
                ]); ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<?php
echo $this->Modal->create('shipping-address-modal', [
    'name' => __('Úprava dodacej adresy'),
    'templateVars' => [
        'containerClass' => 'c-modal--remote c-modal--address js-remote-modal',
    ]
])
    ->setCallback('window.addressModalLoaded($this)')
    ->remoteContent();

echo $this->Modal->create('billing-address-modal', [
    'name' => __('Úprava fakturačnej adresy'),
    'templateVars' => [
        'containerClass' => 'c-modal--remote c-modal--address js-remote-modal'
    ]
])
    ->setCallback('window.addressModalLoaded($this)')
    ->remoteContent();

if (\count($customerAddresses) > 0) {
    echo $this->Modal->create('select-shipping-modal', [
        'name' => __('Výber dodacej adresy'),
        'templateVars' => [
            'containerClass' => 'c-modal--remote c-modal--address js-remote-modal',
        ]
    ])
        ->setCallback('window.addressModalLoaded($this)')
        ->remoteContent();
}
