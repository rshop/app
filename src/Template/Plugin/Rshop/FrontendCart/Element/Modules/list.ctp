<?php
$orderModules = [
    'shipping' => [
        'title' => __('Vyberte si spôsob dopravy'),
        'errorMessage' => __('Nezvolili ste si spôsob dopravy.'),
        'items' => $shippings
    ],
    'payment' => [
        'title' => __('Vyberte si typ platby'),
        'errorMessage' => __('Nezvolili ste si spôsob platby.'),
        'items' => $payments
    ]
];

$customShippingsDates = $this->Cart->customShippingsDates();
$this->Configure->write('Custom.cartCustomShippingsDates', $customShippingsDates);
$isB2b = $this->Configure->read('Custom.b2b');
?>

<div class="c-modules">
    <?php
    // Lubos: fajn je, na zaciatku kosika definovat tie polia ako hidden s value="", nech ked su disabled, tak sa beru tie vrchne polia a teda nech sa posiela do requestu nulova hodnota ak zmenis nieco
    echo $this->Form->input('shipping_module_id', ['type' => 'hidden', 'value' => '']);

    foreach ($orderModules as $type => $orderModule) {
    ?>
        <div class="c-modules__section">
            <h2 class="c-modules__title">
                <?= $orderModule['title']; ?>
            </h2>

            <div class="c-modules__messages">
                <div class="c-msg c-msg--error is-flash is-hidden" id="error-<?= $type; ?>">
                    <?= $orderModule['errorMessage']; ?>
                </div>

                <?php
                if ($type == 'shipping') {
                ?>
                    <div class="c-msg c-msg--error is-flash is-hidden" id="error-branch">
                        <?= __('Nezvolili ste si predajňu.'); ?>
                    </div>
                <?php
                }
                ?>
            </div>

            <div class="c-modules__list">
                <?php
                foreach ($orderModule['items'] as $id => $module) {
                    echo $this->element('Rshop/FrontendCart.Modules/item', \compact('type', 'module', 'customShippingsDates'));
                }
                ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>