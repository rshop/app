<?php
$this->start('DpdParcelshopPickupModalContent');
?>
<div class="c-modal__content">
    <div class="c-map-pickup" id="dpd-parcelshop-map-pickup-el"></div>
</div>
<?php
$this->end();

echo $this->Modal->create('dpd-parcelshop-pickup-modal', [
    'name' => __('Výber pobočky'),
    'templateVars' => [
        'containerClass' => 'c-modal--branches',
        'loaderText' => __('Načítavam mapu')
    ],
])
    ->setContent($this->fetch('DpdParcelshopPickupModalContent'));

$this->Html->scriptBlock('
    var dpdParcelshopBranchesData = ' . ($pickups ? \json_encode($pickups) : '[]') . ';
', ['data-cookieconsent' => 'ignore']);

$selectedPickup = false;

if ($order[$pickupField]) {
    foreach ($pickups as $pickup) {
        if ($pickup['id'] == $order[$pickupField]) {
            $selectedPickup = $pickup;

            break;
        }
    }
}

echo $this->Form->input($pickupField, [
    'type' => 'hidden',
    'value' => ($selectedPickup ? $selectedPickup->id : ''),
    'r-dpd-parcelshop-input' => true
]);
?>


<div class="c-module-dependencies__pickup" r-dpd-parcelshop-holder>
    <div class="sc-empty <?= $selectedBranch ? 'hidden' : ''; ?>" r-dpd-parcelshop-empty>
        <?= __('Nie je zvolená žiadna pobočka'); ?>
    </div>

    <div class="sc-selected <?= $selectedBranch ? '' : 'hidden'; ?>" r-dpd-parcelshop-selected>
        <span class="sc-selected__title"><?= __('Vybrali ste pobočku') . ':'; ?></span>
        <strong class="sc-selected__branch" r-dpd-parcelshop-selected-branch>
            <?= $selectedBranch ? $selectedBranch['name'] : ''; ?>
        </strong>
        <span class="sc-selected__address" r-dpd-parcelshop-selected-address>
            <?= $selectedBranch ? $selectedBranch['address'] : ''; ?>
        </span>
    </div>

    <button class="c-btn c-btn--ghost" type="button" r-dpd-parcelshop-trigger>
        <?= __('Vybrať pobočku'); ?>
    </button>
</div>