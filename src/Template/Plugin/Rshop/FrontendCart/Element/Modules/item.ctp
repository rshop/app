<?php

$moduleDependencies = $this->cell('Rshop/Frontend.Modules::dependencies', [$module, $order])->__toString();
$moduleDescription = $this->Cart->customModuleDescription($module);
$moduleImage = $module->image;
$moduleTippy = '';
$moduleClass = $module->module->getModule();
$modulesItemClasses = '';

$isBranchPickup = \strpos($module->module['class'], 'BranchPickup') !== false;
$isFree = $this->Cart->getItemPrice($module, false) <= 0;

$isApplePay = $module->module_data['defaultPayment'] == 'APPLE_PAY';

if ($isApplePay) {
    $modulesItemClasses .= 'is-hidden js-apple-pay';

    echo $this->Html->scriptBlock('
        if (window.ApplePaySession && window.ApplePaySession.canMakePayments()) {
            document.querySelectorAll(".js-apple-pay.is-hidden").forEach(payment => {
                payment.classList.remove("is-hidden")
            });
        }
    ', ['ready' => false, 'data-cookieconsent' => 'ignore']);
}
?>

<div class="c-modules__item js-modules-item <?= $modulesItemClasses; ?>">
    <?= $this->Cart->moduleInput($module, [
        'disabled' => $moduleTippy ? 'disabled' : false
    ]); ?>

    <label class="c-modules__label js-module-label <?= $isBranchPickup ? 'has-caret' : ''; ?>" for="<?= $type . '-' . $module->id; ?>">
        <div class="c-modules__head">
            <div class="c-module-head">
                <div class="c-module-head__info">
                    <div class="sc-name">
                        <?= $module->name; ?>
                    </div>

                    <?php
                    if ($moduleDescription) {
                    ?>
                    <div class="sc-desc">
                        <?= $moduleDescription; ?>
                    </div>
                    <?php
                    }
                    ?>
                </div>

                <?php
                if ($moduleImage) {
                ?>
                <div class="c-module-head__image">
                    <?= $this->Html->lazyEntityThumbnail($module, ['format' => 'xs']); ?>
                </div>
                <?php
                }

                if ($type == 'shipping' && $customShippingsDates) {
                    $shippingDate = $isBranchPickup ? $customShippingsDates['pickup'] : $customShippingsDates['delivery'];

                    if ($isBranchPickup) {
                        $shippingDate = \min($shippingDate);
                    } else {
                        $shippingDate = $shippingDate[$module->id] ?? null;
                    }

                    if ($shippingDate) {
                    ?>
                    <div class="c-module-head__note">
                        <?= $this->Format->customShippingDateText($shippingDate); ?>
                    </div>
                    <?php
                    }
                }
                ?>

                <div class="c-module-head__price <?= $isFree ? 'is-free' : ''; ?>">
                    <?= $isFree ? __('Zadarmo') : $this->Cart->getItemPrice($module); ?>
                </div>
            </div>
        </div>

        <?php
        if ($moduleDependencies) {
            ?>
            <div class="c-modules__body">
                <div class="c-module-dependencies">
                    <?= $moduleDependencies; ?>
                </div>
            </div>
            <?php
        } ?>
    </label>
</div>
