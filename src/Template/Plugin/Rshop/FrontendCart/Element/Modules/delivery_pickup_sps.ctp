<?php
// resources/js/components/SpsParcelshopPickupMap.vue
$this->start('SpsParcelshopPickupModalContent');
?>
<div class="c-modal__content">
    <div class="c-map-pickup" id="sps-parcelshop-map-pickup-el"></div>
</div>
<?php
$this->end();

echo $this->Modal->create('sps-parcelshop-pickup-modal', [
    'name' => __('Výber pobočky'),
    'templateVars' => [
        'containerClass' => 'c-modal--branches',
        'loaderText' => __('Získavam lokáciu'),
    ],
])
    ->setContent($this->fetch('SpsParcelshopPickupModalContent'));

$this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . $this->Configure->read('Api.GoogleMap') . '&libraries=geometry', [
    'block' => true,
    'async' => false,
    'defer' => true,
    'data-cookieconsent' => 'ignore'
]);

$this->Html->scriptBlock('
    var spsParcelshopBranchesData = ' . ($pickups ? \json_encode($pickups) : '[]') . ';
', ['data-cookieconsent' => 'ignore']);

$selectedPickup = false;

if ($order[$pickupField]) {
    foreach ($pickups as $pickup) {
        if ($pickup['id'] == $order[$pickupField]) {
            $selectedPickup = $pickup;

            break;
        }
    }
}

echo $this->Form->input($pickupField, [
    'type' => 'hidden',
    'value' => ($selectedPickup ? $selectedPickup->id : ''),
    'r-sps-parcelshop-input' => true
]);
?>

<div class="c-module-dependencies__pickup" r-sps-parcelshop-holder>
    <div class="sc-empty <?= $selectedPickup ? 'hidden' : ''; ?>" r-sps-parcelshop-empty>
        <?= __('Nie je zvolená žiadna pobočka'); ?>
    </div>

    <div class="sc-selected <?= $selectedPickup ? '' : 'hidden'; ?>" r-sps-parcelshop-selected>
        <div class="sc-selected__title"><?= __('Vybrali ste pobočku') . ':'; ?></div>

        <div class="sc-selected__branch" r-sps-parcelshop-selected-branch>
            <?= $selectedPickup ? $selectedPickup['name'] : ''; ?>
        </div>

        <div class="sc-selected__address" r-sps-parcelshop-selected-address>
            <?= $selectedPickup ? $selectedPickup['address'] : ''; ?>
        </div>
    </div>

    <button class="c-btn c-btn--ghost" type="button" r-sps-parcelshop-trigger>
        <?= __('Vybrať pobočku'); ?>
    </button>
</div>