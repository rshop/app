<?php
$emailLink = $this->Html->link(
    '<strong>' . $this->Configure->read('Rshop.store.email') . '</strong>',
    'mailto:' . $this->Configure->read('Rshop.store.email'),
    ['class' => 'c-link c-link--secondary is-underlined', 'escape' => false]
);

$phoneLink = $this->Html->link(
    '<strong>' . $this->Configure->read('Rshop.store.phone') . '</strong>',
    'tel:' . $this->Configure->read('Rshop.store.phone'),
    ['class' => 'c-link c-link--secondary', 'escape' => false]
);
?>

<div class="c-success-content is-<?= $successType; ?>">
    <?php
    switch ($successType) {
        case 'estimate':
            $contentText = __('O postupe vybavenia dopytu budete notifikovaný na vašu e-mailovú adresu.');

            break;

        case 'omnipay':
            $contentText = false;

            break;

        case 'unpaid':
            $contentText = __('Mrzí nás táto situácia. Skúste uhradiť objednávku znovu tlačidlom {0}“uhradiť objednávku”{1}, nech vaše chuťové poháriky nezostanú nedotknuté.', '<strong>', '</strong>');

            break;

        case 'change-payment':
            $contentText = 'change';

            break;

        case 'paid':
        case 'default':
        default:
            $contentText = __('O postupe vybavenia objednávky {0}budete notifikovaný na vašu e-mailovú adresu.{1} V prípade, že ste potvrdzovací e-mail neobdržali, kontaktujte nás prosím e-mailom na {2}', '<strong>', '</strong>', $emailLink);

            break;
    }

    if ($contentText) {
        echo '<div class="c-success-content__text">';
            echo $contentText;
        echo '</div>';
    }
    ?>

    <div class="c-success-content__dependencies">
        <?php
        if (\in_array($successType, ['unpaid', 'omnipay'])) {
            echo $this->cell('Rshop/Frontend.Modules::omnipay', [$order]);
        }
        ?>

        <?= $this->cell('Rshop/Frontend.Modules::successDependencies', [$order, 'Payments', 'payment_id']); ?>
        <?= $this->cell('Rshop/Frontend.Modules::successDependencies', [$order, 'Shippings', 'shipping_id']); ?>
    </div>
</div>