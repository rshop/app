<?php
switch ($successType) {
    case 'estimate':
        $headerIcon = 'tick';
        $headerTitle = __('Ďakujeme, že ste u nás nakúpili');
        $headerSubtitle = __('Váš dopyt číslo {0} {1}bol úspešne odoslaný.{2}', '<strong>' . $order->document_number . '</strong>', '<strong>', '</strong>');

        break;

    case 'paid':
        $headerIcon = 'tick';
        $headerTitle = __('Ďakujeme, že ste u nás nakúpili');
        $headerSubtitle = __('Vaša objednávka číslo {0} {1}bola úspešne uhradená.{2}', '<strong>' . $order->document_number . '</strong>', '<strong>', '</strong>');

        break;

    case 'unpaid':
        $headerIcon = 'cross';
        $headerTitle = __('Ľutujeme, ale platbu sa nepodarilo zrealizovať');
        $headerSubtitle = __('Vašu objednávku {0} sme prijali, no nepodarilo sa ju uhradiť.', '<strong>' . $order->document_number) . '</strong>';

        break;

    case 'change-payment':
        $headerIcon = false;
        $headerTitle = __('Vyberte iný spôsob platby');
        $headerSubtitle = __('Napriek nezrealizovaní platby máte objednávku {0} aktívnu a môžete si tak vybrať iný spôsob jej úhrady..', '<strong>' . $order->document_number . '</strong>');

        break;

    case 'omnipay':
    case 'default':
    default:
        $headerIcon = 'tick';
        $headerTitle = __('Ďakujeme, že ste u nás nakúpili');
        $headerSubtitle = __('Vaša objednávka číslo {0} {1}bola úspešne odoslaná.{2}', '<strong>' . $order->document_number . '</strong>', '<strong>', '</strong>');

        break;
}
?>

<div class="c-success-header is-<?= $successType; ?>">
    <?php
    if ($headerIcon) {
    ?>
    <div class="c-success-header__icon">
        <?= $this->Html->ico($headerIcon); ?>
    </div>
    <?php
    }
    ?>

    <div class="c-success-header__title">
        <h1>
            <?= $headerTitle; ?>
        </h1>
    </div>

    <div class="c-success-header__subtitle">
        <?= $headerSubtitle; ?>
    </div>
</div>