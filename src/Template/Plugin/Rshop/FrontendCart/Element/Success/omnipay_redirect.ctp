<div class="c-dependencies__countdown">
    <svg class="js-counter" width="54px" height="54px" viewBox="0 0 54 54">
        <circle class="is-fill" stroke-width="4" cx="27" cy="27" r="25"></circle>
        <circle class="is-bg" stroke-width="4" cx="27" cy="27" r="25"></circle>
    </svg>

    <span class="js-omnipay-countdown">5s</span>
</div>

<div class="l-shopping-success__text h-margin-b-40">
    <?= __('Teraz budete automaticky vyzvaný, {0}aby ste zadali údaje o karte a dokončili tak objednávku{1}. Ak sa vám nezobrazí okno s platbou, skúste stlačiť tlačidlo {0}"uhradiť objednávku"{1}, nech vaše chuťové poháriky nezostanú nedotknuté.', '<strong>', '</strong>'); ?>
</div>