<?php
switch ($successType) {
    case 'unpaid':
        $footerText = __('Aj najpriek tejto nepríjemnosti dúfame, že nám zachováte vašu priazeň aj naďalej.') . '<br>' . __('S pozdravom') . ',';
        $footerLogo = true;

        break;

    case 'omnipay':
        $footerText = false;
        $footerLogo = true;

        break;

    case 'change-payment':
        $footerText = false;
        $footerLogo = false;

        break;

    case 'estimate':
    case 'paid':
    case 'default':
    default:
        $footerText = __('Tešíme sa na Vašu návštevu!') . '<br>' . __('Ďakujeme za nákup a veríme, že nám zachováte vašu priazeň aj naďalej.') . '<br>' . __('S pozdravom') . ',';
        $footerLogo = true;

        break;
}
?>

<div class="c-success-footer is-<?= $successType; ?>">
    <?php
    if ($footerText) {
    ?>
    <div class="c-success-footer__text">
        <?= $footerText; ?>
    </div>
    <?php
    }

    if ($footerLogo) {
    ?>
    <div class="c-success-footer__logo">
        <?= $this->Html->lazyImage('/img/logos/' . $this->Configure->read('Rshop.store.basicName') . '-lower-blue.svg'); ?>
    </div>
    <?php
    }
    ?>
</div>