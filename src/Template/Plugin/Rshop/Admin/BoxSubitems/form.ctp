<?php
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Rshop\Admin\Routing\RshopClassChecker;

$this->extend('/Common/form');

/**
 * Configure input fields per box.
 *
 * PLACE_NAME       : name of the box
 * INPUT_NAME       : name of the input
 * HTML_CONTENT     : html content to be echoed (instead of echoing input of options)
 *
 * ACTIVE_CONDITION : whether to show input (default is true)
 * INPUT_OPTIONS    : array of options (if extending default input, it will be merged)
 * HTML_CONTENT     : html content to be echoed (instead of echoing input of options)
 * FORCE_SHOW       : by default, inputs are echoed only if BoxItem is of type custom, but there
 *                    might be a situation, in which you would need input in other BoxItem types,
 *                    for example 'is_large' on product/category BoxItem type, and so on...
 *
 *   BOX_NAME => [
 *     INPUT_NAME => [
 *       'active' => ACTIVE_CONDITION
 *       'input_options' => INPUT_OPTIONS
 *       'html' => HTML_CONTENT
 *       'force_show' => FORCE_SHOW
 *     ]
 *   ]
 */
$boxInputs = [
    // 'footerColumns' => [
    //     'custom_is_underlined' => [
    //         'input_options' => [
    //             'type' => 'bool',
    //             'label' => __d('rshop', 'Podčiarknuť odkaz')
    //         ]
    //     ],
    // ]
];

/**
 * Default inputs, those are present in every subItem,
 * if not overwritten in specific input config.
 */
$defaultInputs = [
    'name' => [
        'input_options' => [
            'label' => __d('rshop', 'Názov')
        ]
    ],
    'url' => [
        'input_options' => [
            'label' => __d('rshop', 'Linka')
        ]
    ],
    'image' => [
        'active' => false,
        'input_options' => [
            'type' => 'upload', 'label' => __d('rshop', 'Obrázok')
        ]
    ],
];

/**
 * We need BoxItem to know ID of box that this subitem is going to be created in.
 */
$boxItem = null;

if (!$entity->isNew()) {
    $boxItem = $entity->box_item;
} elseif (isset($this->request->query['box_item_id'])) {
    $boxItem = RshopClassChecker::getTable('BoxItems')->find()
        ->where(['BoxItems.id' => $this->request->query['box_item_id']])
        ->first();
}

/**
 * This next loop will figure out current box and shop name.
 */
$currentBoxName = null;
$currentBoxId = $boxItem->box_id;

foreach (Configure::read('Boxes') as $boxConfigurations) {
    foreach ($boxConfigurations as $boxName => $boxId) {
        if ($currentBoxId == $boxId) {
            $currentBoxName = $boxName;

            break 2;
        }
    }
}

/*
 * Assignment and echoing of fields for current box
 */
echo $this->Form->create($entity);
    $formInputs = Hash::merge($defaultInputs, $boxInputs[$currentBoxName] ?? []);

    foreach ($formInputs as $inputName => $fieldOptions) {
        $showInput = $fieldOptions['active'] ?? true;

        if ($showInput) {
            $showInput = $fieldOptions['force_show'] ?? ($entity->isNew() || @$entity->box_item->subitems_type == 'custom');
        }

        if (!$showInput) {
            continue;
        }

        echo $this->Form->input($inputName, $fieldOptions['input_options'] ?? []);
    }

    echo $this->Form->input('box_item_id', ['type' => 'hidden', 'default' => $this->request->query['box_item_id'] ?? '']);
    echo $this->Form->input('active', ['type' => 'bool', 'label' => __d('rshop', 'Aktívna')]);
    echo $this->Form->submit(__d('rshop', 'Uložiť'));
echo $this->Form->end();
