<?php

/**
 * This file is part of rshop/lunys project.
 *
 * (c) RIESENIA.com
 */

use Cake\Core\Configure;
use Cake\Utility\Hash;

$this->extend('form_base');

/*
 * Form definition must be before everything's happening
 */
$this->start('form_definition');
echo $this->Form->create($entity);
$this->end();

/**
 * Configure input fields per banner place.
 *
 * BOX_KEY          : key of the banner place
 * BLOCK_NAME       : name of the block to be assigned to in form
 * INPUT_NAME       : name of the input
 *
 * ACTIVE_CONDITION : whether to show input
 * INPUT_OPTIONS    : array of options (if extending default input, it will be merged)
 * HTML_CONTENT     : html content to be echoed (instead of echoing input of options)
 *                    not working on images
 *
 *   BOX_KEY => [
 *     BLOCK_NAME => [
 *       INPUT_NAME => [
 *         'active' => ACTIVE_CONDITION
 *         'input_options' => INPUT_OPTIONS
 *         'html' => HTML_CONTENT
 *       ]
 *     ]
 *   ]
 */
$boxInputBlocks = [
    // 'index-main' => [
    //     'banner_place_item_main' => [
    //         'description' => ['active' => false]
    //     ],
    //     'banner_place_item_files' => [
    //         'desktop' => [
    //             'input_options' => [
    //                 'dimensions' => '1560 x 920px'
    //             ]
    //         ],
    //     ]
    // ],
];

/**
 * Default inputs, those are present in every item,
 * if not overwritten in specific item config.
 */
$defaultInputBlocks = [
    'banner_place_item_main' => [
        'name' => [
            'input_options' => [
                'label' => __d('rshop', 'Názov')
            ]
        ],
        'description' => [
            'input_options' => [
                'label' => __d('rshop', 'Popis'),
                'type' => 'textarea'
            ]
        ],
        'type' => [
            'active' => false,
            'input_options' => [
                'label' => __d('rshop', 'Typ bannera'),
                'options' => $entity->available_types
            ]
        ],
        'url' => [
            'input_options' => [
                'label' => __d('rshop', 'Linka')
            ]
        ],
        'target_blank' => [
            'input_options' => [
                'type' => 'bool',
                'label' => __d('rshop', 'Otvoriť linku v novom okne')
            ]
        ],
    ],

    // Restrictions block -> banner_place_item_restrictions
    'banner_place_item_restrictions' => [
        '_banner_place_item_restrictions_title' => [
            'html' => $this->Html->tag('h2', __d('rshop', 'Nastavenia'))
        ],
        'customer_selection' => [
            'active' => !$bannerPlace->products_listing_banner,
            'html' => $bannerPlace->products_listing_banner ? '' : $this->Form->customerSelection()
        ],
        'products_condition' => [
            'active' => $bannerPlace->select_product,
            'input_options' => [
                'type' => 'query',
                'label' => __d('rshop', 'Zobraziť na produktoch'),
                'config' => 'Rshop.productsQueryModule'
            ]
        ],
        'campaigns._ids' => [
            'active' => $bannerPlace->select_campaign,
            'input_options' => [
                'label' => __d('rshop', 'Zobraziť v kampaniach'),
                'remote' => $this->Url->build(['controller' => 'Campaigns', 'action' => 'kendoSelect']),
                'kendo' => ['dataTextField' => 'name']
            ]
        ],
        'categories._ids' => [
            'active' => $bannerPlace->select_category,
            'input_options' => [
                'label' => __d('rshop', 'Zobraziť na kategóriach'),
                'remote' => $this->Url->build(['controller' => 'Categories', 'action' => 'kendoSelect']),
                'kendo' => ['dataTextField' => 'name']
            ]
        ],
        'exclude_subcategories' => [
            'active' => $bannerPlace->select_category,
            'input_options' => [
                'type' => 'bool',
                'label' => __d('rshop', 'Zobraziť iba na zvolených kategóriách'),
                'help' => __d('rshop', 'Banner sa zobrazí iba na zvolených kategóriách. V opačnom prípade sa zobrazí aj na podkategóriách zvolených kategórií.')
            ]
        ],
    ],

    // Files block -> banner_place_item_files
    'banner_place_item_files' => [
        'desktop' => [
            'input_options' => [
                'label' => __d('rshop', 'Obrázok'),
            ]
        ],
        'tablet' => [
            'active' => false,
            'input_options' => [
                'label' => __d('rshop', 'Obrázok pre tablet')
            ]
        ],
        'phone' => [
            'active' => false,
            'input_options' => [
                'label' => __d('rshop', 'Obrázok pre telefón')
            ]
        ]
    ],

    // Video block -> banner_place_item_video
    'banner_place_item_video' => [
        'video' => [
            'active' => false,
            // 'active' => !$bannerPlace->products_listing_banner,
            'input_options' => [
                'label' => __d('rshop', 'Video')
            ]
        ],
    ],

    // Other block -> banner_place_item_other
    'banner_place_item_other' => [
        'listing_page' => [
            'active' => $bannerPlace->products_listing_banner,
            'input_options' => [
                'label' => __d('rshop', 'Listing - číslo strany')
            ]
        ],
        'listing_position' => [
            'active' => $bannerPlace->products_listing_banner,
            'input_options' => [
                'label' => __d('rshop', 'Listing - pozícia')
            ]
        ],
        'valid_from' => [
            'input_options' => [
                'label' => __d('rshop', 'Platný od'),
                'range_to' => 'valid-until'
            ]
        ],
        'valid_until' => [
            'input_options' => [
                'label' => __d('rshop', 'Platný do'),
                'range_from' => 'valid-from'
            ]
        ],
    ]
];

/**
 * Assignment and echoing of fields for current item.
 */
$bannerKey = $bannerPlace->banner_key;
$formBlocks = Hash::merge($defaultInputBlocks, $boxInputBlocks[$bannerKey] ?? []);

foreach ($formBlocks as $blockName => $blockInputs) {
    $this->assign($blockName, '');

    if ($blockName === 'banner_place_item_files') {
        $imageIndex = 0;

        foreach ($blockInputs as $imageVisibility => $fieldOptions) {
            $isActive = $fieldOptions['active'] ?? true;
            $inputOptions = \array_merge([
                'type' => 'upload',
                'dimensions' => Configure::read('Admin.images.banners.' . $bannerKey . '.' . $imageVisibility),
                'kendo' => [
                    'async' => [
                        'saveUrl' => $this->Url->build(['controller' => 'BannerPlaceItemFiles', 'action' => 'kendoHandleFile', 'image-' . $imageVisibility]),
                        'saveField' => 'image-' . $imageVisibility,
                        'autoUpload' => true,
                    ]
                ]
            ], $fieldOptions['input_options'] ?? []);

            $this->append($blockName, $this->Form->input('banner_place_item_files.' . $imageIndex . '.id'));
            $this->append($blockName, $this->Form->input('banner_place_item_files.' . $imageIndex . '.visibility', [
                'type' => 'hidden',
                'value' => $imageVisibility
            ]));

            /*
             * Images are displayed by index as well as visibility, so active flag here, will only hide it
             * In case that, one of other two images would be activated, and other deactivated, images
             * would mix up, one that was tablet would be phone and vice versa
             */
            if ($isActive) {
                $this->append($blockName, $this->Form->input('banner_place_item_files.' . $imageIndex . '.image', $inputOptions));
            }

            ++$imageIndex;
        }

        continue;
    }

    foreach ($blockInputs as $inputName => $fieldOptions) {
        $isActive = $fieldOptions['active'] ?? true;

        if (!$isActive) {
            continue;
        }

        if (isset($fieldOptions['html'])) {
            $this->append($blockName, $fieldOptions['html']);
        } else {
            $this->append($blockName, $this->Form->input($inputName, $fieldOptions['input_options'] ?? []));
        }
    }
}

$this->append('banner_place_item_other', $this->Form->input('banner_place_id', ['type' => 'hidden', 'default' => $bannerPlace->id]));
$this->append('banner_place_item_other', $this->Form->input('active', ['type' => 'bool', 'label' => __d('rshop', 'Aktívny')]));

echo $this->Form->input('banner_place_id', ['type' => 'hidden', 'default' => $bannerPlace->id]);
