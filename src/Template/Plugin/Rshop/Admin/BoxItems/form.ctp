<?php

use Cake\Core\Configure;
use Cake\Utility\Hash;

$this->extend('/Common/form');

/**
 * Configure input fields per box.
 *
 * BOX_KEY          : key of the box
 * INPUT_NAME       : name of the input
 * HTML_CONTENT     : html content to be echoed (instead of echoing input of options)
 *
 * ACTIVE_CONDITION : whether to show input (default is true)
 * INPUT_OPTIONS    : array of options (if extending default input, it will be merged)
 * HTML_CONTENT     : html content to be echoed (instead of echoing input of options)
 * FORCE_SHOW       : by default, inputs are echoed only if Box is of type custom, but there
 *                    might be a situation, in which you would need input in other Box types,
 *                    for example 'is_large' on product/category Box type, and so on...
 *
 *   BOX_KEY => [
 *     INPUT_NAME => [
 *       'active' => ACTIVE_CONDITION
 *       'input_options' => INPUT_OPTIONS
 *       'html' => HTML_CONTENT
 *       'force_show' => FORCE_SHOW
 *     ]
 *   ]
 */
$boxInputs = [
    // 'footer-columns' => [
    //     'url' => [
    //         'active' => false,
    //     ],
    //     'has_subitems' => [
    //         'input_options' => [
    //             'type' => 'hidden',
    //             'value' => true
    //         ]
    //     ],
    //     'subitems_type' => [
    //         'input_options' => [
    //             'type' => 'hidden',
    //             'value' => 'custom'
    //         ]
    //     ],
    // ],
];

/**
 * Default inputs, those are present in every item,
 * if not overwritten in specific input config.
 */
$defaultInputs = [
    'name' => [
        'input_options' => [
            'label' => __d('rshop', 'Názov')
        ]
    ],
    'url' => [
        'input_options' => [
            'label' => __d('rshop', 'Linka')
        ]
    ],
    'image' => [
        'active' => false,
        'input_options' => [
            'type' => 'upload',
            'label' => __d('rshop', 'Obrázok')
        ]
    ],
];

/**
 * This next loop will figure out current box and shop name.
 */
$boxId = $this->request->query['box_id'] ?? $entity->box->id;
$boxKey = $this->request->query['box_key'] ?? $entity->box->box_key;

/*
 * Assignment and echoing of fields for current item
 */
echo $this->Form->create($entity);
$formInputs = Hash::merge($defaultInputs, $boxInputs[$boxKey] ?? []);

foreach ($formInputs as $inputName => $fieldOptions) {
    $showInput = $fieldOptions['active'] ?? true;

    if ($showInput) {
        $showInput = $fieldOptions['force_show'] ?? ($entity->isNew() || @$entity->box->type == 'custom');
    }

    if (!$showInput) {
        continue;
    }

    echo $this->Form->input($inputName, $fieldOptions['input_options'] ?? []);
}

echo $this->Form->input('box_id', ['type' => 'hidden', 'default' => $this->request->query['box_id'] ?? '']);
echo $this->Form->input('active', ['type' => 'bool', 'label' => __d('rshop', 'Aktívna')]);
echo $this->Form->submit(__d('rshop', 'Uložiť'));
echo $this->Form->end();
