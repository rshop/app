<?php
namespace App\Console;

use Composer\Script\Event;

/**
 * Provides installation hooks for when this application is installed via composer.
 */
class Installer
{

    /**
     * Do some routine installation tasks
     *
     * @param \Composer\Script\Event composer event object
     * @return void
     */
    public static function postInstall(Event $event)
    {
        $io = $event->getIO();

        $rootDir = dirname(dirname(__DIR__));

        static::createAppConfig($rootDir, $io);

        // ask to set folder permissions and create symlinks
        if ($io->isInteractive()) {
            $validator = function ($arg) {
                if (in_array($arg, ['Y', 'y', 'N', 'n'])) {
                    return $arg;
                }
                throw new \Exception('This is not a valid answer. Please choose Y or n.');
            };

            $runUpdate = $io->askAndValidate('<info>Set folder permissions and create symlinks if needed? Run only with sufficient privileges</info> [<comment>Y,n</comment>]? ', $validator, 10, 'Y');

            if (strtolower($runUpdate) == 'y') {
                static::createRshopSymlinks($rootDir, $io);
                static::createWritableDirectories($rootDir, $io);
            }
        }

        // ask if the rshop console should run common tasks
        if ($io->isInteractive()) {
            $validator = function ($arg) {
                if (in_array($arg, ['Y', 'y', 'N', 'n'])) {
                    return $arg;
                }
                throw new \Exception('This is not a valid answer. Please choose Y or n.');
            };

            $runUpdate = $io->askAndValidate('<info>Run standard RSHOP update tasks?</info> [<comment>Y,n</comment>]? ', $validator, 10, 'Y');

            if (in_array($runUpdate, ['Y', 'y'])) {
                static::runRshopUpdate($rootDir, $io);
            }

            $runUpdate = $io->askAndValidate('<info>Run standard FRONTEND update tasks?</info> [<comment>Y,n</comment>]? ', $validator, 10, 'Y');

            if (in_array($runUpdate, ['Y', 'y'])) {
                static::runFrontendUpdate($rootDir, $io);
            }
        }

        // say congrats
        $io->write('<info>Congratulations Igor! You did it!</info>');
    }

    /**
     * Create the config/.env file if it does not exist
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function createAppConfig($dir, $io)
    {
        $appConfig = $dir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '.env';
        $defaultConfig = $dir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '.env.default';
        if (!file_exists($appConfig)) {
            copy($defaultConfig, $appConfig);
            $io->write('Created `config' . DIRECTORY_SEPARATOR . '.env` file');

            $name = $io->ask('<info>Set application name</info>: ');

            $content = file_get_contents($appConfig);
            $content = str_replace('__APP_NAME__', $name, $content, $count);
            $result = file_put_contents($appConfig, $content);

            static::setSecuritySalt($dir, $io);
            static::setDatabase($dir, $io);
        }
    }

    /**
     * Create the `files`, `logs` and `tmp` directories.
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function createWritableDirectories($dir, $io)
    {
        $paths = [
            'files',
            'logs',
            'tmp',
            'tmp' . DIRECTORY_SEPARATOR . 'cache',
            'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'models',
            'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'persistent',
            'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'rshop',
            'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'views',
            'tmp' . DIRECTORY_SEPARATOR . 'sessions',
            'tmp' . DIRECTORY_SEPARATOR . 'tests',
            'webroot' . DIRECTORY_SEPARATOR . 'files',
            'webroot' . DIRECTORY_SEPARATOR . 'images',
            'vendor' . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'ttfontdata'
        ];

        foreach ($paths as $path) {
            $path = $dir . DIRECTORY_SEPARATOR . $path;
            if (!file_exists($path)) {
                mkdir($path);
                $io->write('Created `' . $path . '` directory');
            }
            static::setFolderPermissions($path, $io);
        }
    }

    /**
     * Set globally writable permissions on the `files`, `logs` and `tmp` directory
     *
     * @param string path
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function setFolderPermissions($path, $io)
    {
        $res = chmod($path, 0777);

        if ($res) {
            $io->write('<info>Permissions set on ' . $path . '</info>');
        } else {
            $io->write('<error>Failed to set permissions on ' . $path . '</error>');
        }
    }

    /**
     * Set the security.salt value in the application's config file.
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function setSecuritySalt($dir, $io)
    {
        $config = $dir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '.env';
        $content = file_get_contents($config);

        $newKey = hash('sha256', $dir . php_uname() . microtime(true));
        $content = str_replace('__SALT__', $newKey, $content, $count);

        if ($count == 0) {
            $io->write('<error>No Security.salt placeholder to replace.</error>');
            return;
        }

        $result = file_put_contents($config, $content);
        if ($result) {
            $io->write('<info>Updated Security.salt value in config' . DIRECTORY_SEPARATOR . '.env</info>');
            return;
        }
        $io->write('<error>Unable to update Security.salt value.</error>');
    }

    /**
     * Set the database configuration in the application's config file.
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function setDatabase($dir, $io)
    {
        if (!$io->isInteractive()) {
            return;
        }

        $config = $dir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '.env';
        $content = file_get_contents($config);

        $database = [
            'host' => [
                'name' => 'host',
                'placeholder' => '__DB_HOST__',
                'default' => 'localhost'
            ],
            'port' => [
                'name' => 'port',
                'placeholder' => '__DB_PORT__',
                'default' => '3306'
            ],
            'user' => [
                'name' => 'user name',
                'placeholder' => '__DB_USER__',
                'default' => 'root'
            ],
            'pass' => [
                'name' => 'password',
                'placeholder' => '__DB_PASS__',
                'default' => ''
            ],
            'name' => [
                'name' => 'name',
                'placeholder' => '__DB_NAME__',
                'default' => null
            ]
        ];

        foreach ($database as &$param) {
            $param['value'] = $io->ask(
                '<info>Set database ' . $param['name'] . '</info>' . ($param['default'] !== null ? ' <comment>(Default to "' . $param['default'] . '")</comment>' : '') . ': ',
                $param['default']
            );

            $content = str_replace($param['placeholder'], $param['value'], $content, $count);

            if ($count == 0) {
                $io->write('<error>No placeholder ' . $param['placeholder'] . ' to replace.</error>');
                return;
            }
        }

        $result = file_put_contents($config, $content);
        if ($result) {
            $io->write('<info>Updated database config in config' . DIRECTORY_SEPARATOR . '.env</info>');

            // if database provided and does not exist, try to create one
            if ($database['user']['value'] == 'root' && trim($database['name']['value'])) {
                try {
                    $mysql = new \mysqli($database['host']['value'], $database['user']['value'], $database['pass']['value'], null, $database['port']['value']);
                } catch (\Exception $e) {
                    return;
                }

                if (!$mysql->select_db($database['name']['value']) && $mysql->query("CREATE DATABASE `" . trim($database['name']['value']) . "`")) {
                    $io->write('<info>Database `' . trim($database['name']['value']) . '` created.</info>');
                }
            }
            return;
        }
        $io->write('<error>Unable to update database config.</error>');
    }

    /**
     * Create rshop webroot symlinks
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function createRshopSymlinks($dir, $io)
    {
        if (!is_dir($dir . DIRECTORY_SEPARATOR . 'webroot' . DIRECTORY_SEPARATOR . 'rshop')) {
            exec('bin' . DIRECTORY_SEPARATOR . 'cake rshop install symlink', $output);
            $io->write('<info>Webroot symlinks created.</info>');
        }
    }

    /**
     * Run standard rshop update tasks
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function runRshopUpdate($dir, $io)
    {
        exec('bin' . DIRECTORY_SEPARATOR . 'cake rshop update', $output);
        $io->write($output);
    }

    /**
     * Run standard frontend update tasks
     *
     * @param string application's root directory
     * @param \Composer\IO\IOInterface IO interface to write to console
     * @return void
     */
    public static function runFrontendUpdate($dir, $io)
    {
        exec('bin' . DIRECTORY_SEPARATOR . 'cake frontend update', $output);
        $io->write($output);
    }
}
