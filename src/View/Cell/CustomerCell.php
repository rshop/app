<?php
namespace App\View\Cell;

use Rshop\Frontend\View\Cell\CustomerCell as BaseCustomerCell;

/**
 * @property \App\Model\Table\ProductsTable              $Products
 * @property \Rshop\Admin\Model\Table\OrderProductsTable $OrderProducts
 */
class CustomerCell extends BaseCustomerCell
{
    /**
     * Customer currently pending orders.
     *
     * @param int $customerId
     * @param array options
     * @param array $options
     *
     * @return void
     */
    public function customActiveOrders($customerId, array $options = [])
    {
        $this->loadModel('Orders');

        $orders = $this->Orders->find()
            ->matching('OrderStatuses')
            ->matching('Customers')
            ->where([
                'Orders.customer_id' => $customerId,
                'Orders.canceled' => 0,
                'Orders.resolved' => 0,
            ])
            ->order(['Orders.created' => 'DESC']);

        if ($this->request->session()->read('Auth.Customer.customer_branch_id') !== null) {
            $orders->andWhere(['Orders.customer_branch_id IS' => $this->request->session()->read('Auth.Customer.customer_branch_id')]);
        }

        if ($options['limit']) {
            $orders->limit($options['limit']);
        }

        $orders = $orders->toArray();

        $this->set(\compact('orders'));
    }

    /**
     * Customer unpaid invoices.
     *
     * @param int $customerId
     * @param array options
     * @param array $options
     *
     * @return void
     */
    public function customUnpaidInvoices($customerId, array $options = [])
    {
        $this->loadModel('Invoices');

        $invoices = $this->Invoices->find()
            ->matching('Customers')
            ->where([
                'Invoices.customer_id' => $customerId,
                'Invoices.canceled' => 0,
                'Invoices.paid' => 0,
            ])
            ->order(['Invoices.created' => 'DESC']);

        if ($this->request->session()->read('Auth.Customer.customer_branch_id') !== null) {
            $invoices->andWhere(['Invoices.customer_branch_id IS' => $this->request->session()->read('Auth.Customer.customer_branch_id')]);
        }

        if (@$options['limit']) {
            $invoices->limit($options['limit']);
        }

        $invoices = $invoices->toArray();

        $this->set(\compact('invoices'));
    }

    public function mostOrderedCategories($customerId, array $options = [])
    {
        $categories = $this->loadModel('Categories')->find()
            ->join([
                'CategoriesProducts' => [
                    'type' => 'INNER',
                    'table' => $this->Categories->CategoriesProducts->table(),
                    'conditions' => [
                        'CategoriesProducts.category_id = Categories.id'
                    ]
                ],
                'OrderProducts' => [
                    'type' => 'INNER',
                    'table' => $this->loadModel('OrderProducts')->table(),
                    'conditions' => [
                        'OrderProducts.product_id = CategoriesProducts.product_id'
                    ]
                ],
                'Orders' => [
                    'type' => 'INNER',
                    'table' => $this->loadModel('Orders')->table(),
                    'conditions' => [
                        'Orders.id = OrderProducts.order_id',
                        'Orders.type' => 'order',
                        'Orders.customer_id' => $customerId,
                        //? ak to chcu za poslednych 30 dni
                        // 'Orders.created >=' => 'CURRENT_DATE - INTERVAL 30 DAY'
                    ]
                ]
            ])
            ->group('Categories.id')
            ->limit($options['limit'])
            ->where([
                'Categories.active' => true,
                'Categories.has_active_path' => true
            ])
            ->toArray();

        $this->set(\compact('categories'));
    }
}
