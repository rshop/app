<?php
namespace App\View\Helper;

use Cake\Utility\Text;
use Rshop\Frontend\View\Helper\FormatHelper as FeFormatHelper;

class FormatHelper extends FeFormatHelper
{
    public function price($price, $currency = null, $calculate = false, $symbols = true, $round = false)
    {
        $price = parent::price($price, $currency, $calculate, $symbols, $round);

        if ($currency === null) {
            $currency = $this->request->session()->read('currency');
        }

        if (\is_object($currency)) {
            $currency = $currency->toArray();
        }

        if ($currency['decimals']) {
            $priceArray = \explode($currency['decimal_point'], $price);

            $price = $priceArray[0] . '<span class="is-small">' . $currency['decimal_point'] . $priceArray[1] . '</span>';
        }

        return $price;
    }

    public function customAverageReadingTime($text, array $options = [])
    {
        $options = \array_merge([
            'wpm' => 200
        ], $options);

        $text = \strip_tags($text);
        $text = Text::transliterate($text);
        $word_count = \str_word_count($text);

        return \ceil($word_count / $options['wpm']);
    }
}
