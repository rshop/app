<?php
namespace App\View\Helper;

use Rshop\Frontend\View\Helper\BannerHelper as BaseBannerHelper;

class BannerHelper extends BaseBannerHelper
{
    public function getSrc($banner)
    {
        $srcArray = [];

        foreach ($banner->banner_place_item_files as $file) {
            if ($file->image) {
                $srcArray[$file['visibility']] = $file->image;
            }
        }

        return $srcArray;
    }
}
