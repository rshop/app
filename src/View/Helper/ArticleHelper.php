<?php
namespace App\View\Helper;

use Rshop\Frontend\Routing\RshopClassChecker;
use Rshop\Frontend\View\Helper\ArticleHelper as BaseArticleHelper;

class ArticleHelper extends BaseArticleHelper
{
    public function customRandomArticle($sections = [], $exclude = [])
    {
        return RshopClassChecker::getTable('Articles')->find()
            ->where([
                'Articles.section_id IN' => $sections,
                'Articles.id NOT IN' => $exclude
            ])
            ->order('RAND()')
            ->first();
    }
}
