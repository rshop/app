<?php
namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\I18n\I18n;
use Rshop\Frontend\View\Helper\ConfigureHelper as BaseConfigureHelper;

class ConfigureHelper extends BaseConfigureHelper
{
    public $helpers = ['Url'];

    public function text($name)
    {
        return Configure::read('Rshop.text.' . $name);
    }

    public function box($name)
    {
        $locale = I18n::getLocale();

        return Configure::read('Boxes.' . $locale . '.' . $name);
    }

    public function banner($name)
    {
        $locale = I18n::getLocale();

        return Configure::read('Banners.' . $locale . '.' . $name);
    }

    public function getBoxUrl($boxKey, $itemId = null)
    {
        return $this->Url->build('/admin/boxes/');
    }

    public function getConfigurationUrl($configurationKey)
    {
        return $this->Url->build('/admin/text-groups/');
    }

    // tu sa najskor budu musiet bannery prerobit na sposob boxov
    public function getBannerUrl($bannerKey)
    {
        return $this->Url->build('/admin/banner-places/');
    }
}