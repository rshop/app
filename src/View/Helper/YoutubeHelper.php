<?php
namespace App\View\Helper;

use Rshop\Frontend\View\Helper\YoutubeHelper as FeYoutubeHelper;

class YoutubeHelper extends FeYoutubeHelper
{
    public function matchVideos($string)
    {
        if (!$string) {
            return [];
        }

        $lines = \explode(PHP_EOL, $string);
        $videos = [];

        foreach ($lines as $video) {
            if (!\preg_match('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', $video, $url)) {
                continue;
            }

            $ytUrl = $this->embedUrl($url[0]);

            if (!$ytUrl) {
                continue;
            }

            $videos[] = [
                'video' => $video,
                'embededUrl' => $ytUrl,
            ];
        }

        return $videos;
    }
}
