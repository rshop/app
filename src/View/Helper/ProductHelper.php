<?php
/**
 * This file is part of rshop/biutli project.
 *
 * (c) RIESENIA.com
 */

namespace App\View\Helper;

use Rshop\Frontend\Routing\RshopClassChecker;
use Rshop\Frontend\View\Helper\ProductHelper as BaseProductHelper;

/**
 * Class ProductHelper.
 *
 * @property FormatHelper $Format
 */
class ProductHelper extends BaseProductHelper
{
    /**
     * Check if product is favorite.
     *
     * @param Product $product
     *
     * @return bool
     */
    public function customIsFavorite($product)
    {
        $data = ['product_id' => $product->id];

        if ($this->Auth->user()) {
            $data['customer_id'] = $this->Auth->info('id');
        } elseif ($this->request->getCookie('favoriteProducts')) {
            $data['cookie'] = $this->request->getCookie('favoriteProducts');
        } else {
            return false;
        }

        return RshopClassChecker::getTable('CustomFavoriteProducts')->exists($data);
    }
}
