<?php
namespace App\View\Helper;

use Cake\Core\Configure;
use Rshop\Frontend\View\Helper\AppHelper as FeAppHelper;
use Rshop\FrontendUtility\Routing\Router;

class AppHelper extends FeAppHelper
{
    public function getLayoutName(array $options = [])
    {
        $urlKey = Router::getUrlKey($this->request);
        $urlKeyArray = \explode('.', $urlKey);
        $layoutName = 'default';

        if (Configure::read('Custom.layoutName')) {
            $layoutName = Configure::read('Custom.layoutName');
        } elseif ($urlKeyArray[0] == 'Shopping') {
            $layoutName = 'shopping';
        }

        return $layoutName;
    }

    public function getPageName($that, array $options = [])
    {
        $urlKey = Router::getUrlKey($this->request);
        $urlKeyArray = \explode('.', $urlKey);

        $controller = $that->request->params['controller'];
        $action = $that->request->params['action'];
        $statusCode = $that->response->getStatusCode();

        $isLogged = $that->Auth->isLogged();
        $pageName = 'default';

        if ($urlKeyArray[0] == 'Pages') {
            switch ($urlKeyArray[1]) {
                case 'index':
                    $pageName = 'index';

                    break;

                case 'contact':
                    $pageName = 'contact';

                    break;

                case '404':
                    $pageName = '404';

                    break;
            }
        } elseif (\in_array($urlKeyArray[0], ['Catalog'])) {
            switch ($urlKeyArray[1]) {
                case 'product':
                    $pageName = 'product';

                    break;

                case 'compare':
                    $pageName = 'compare';

                    break;

                default:
                    $pageName = 'catalog';

                    break;
            }
        } elseif ($urlKeyArray[0] == 'Eshop') {
            switch ($urlKeyArray[1]) {
                case 'registration':
                case 'userLogin':
                case 'lostPassword':
                    $pageName = 'default';

                    break;

                case 'claimForm':
                case 'returnForm':
                    if ($isLogged) {
                        $pageName = 'account';
                    } else {
                        $pageName = 'default';
                    }

                    break;

                default:
                    $pageName = 'account';

                    break;
            }
        } elseif (\in_array($urlKeyArray[0], ['Marks', 'Manufacturers'])) {
            switch ($urlKeyArray[1]) {
                case 'manufacturers':
                case 'brands':
                    $pageName = 'brands';

                    break;

                default:
                    $pageName = 'catalog';

                    break;
            }
        } elseif (\in_array($controller, ['Content', 'Consents'])) {
            switch ($action) {
                case 'listing':
                    $pageName = 'articles';

                    break;

                default:
                    $pageName = 'article';

                    break;
            }
        } elseif ($urlKeyArray[0] == 'Shopping') {
            switch ($urlKeyArray[1]) {
                case 'cart':
                    $pageName = 'shopping-cart';

                    break;

                case 'form':
                case 'registration':
                    $pageName = 'shopping-form';

                    break;

                case 'modules':
                    $pageName = 'shopping-modules';

                    break;

                case 'success':
                    $pageName = 'shopping-success';

                    break;

                default:
                    $pageName = 'shopping';

                    break;
            }
        } elseif (\in_array($controller, ['Customs']) && \in_array($action, ['favoriteProducts', 'userBoughtListing'])) {
            $pageName = 'catalog';
        } elseif (\in_array($controller, ['CustomFavoriteProducts']) && \in_array($action, ['listing'])) {
            $pageName = 'catalog';
        } elseif (\in_array($controller, ['CustomPromotions'])) {
            $pageName = 'promotions';
        } elseif (\in_array($controller, ['Customs']) && \in_array($action, ['b2bRegister', 'customerOrderedProducts'])) {
            $pageName = 'account';
        } elseif ($statusCode != '200') {
            $pageName = '404';
        }

        return $pageName;
    }
}
