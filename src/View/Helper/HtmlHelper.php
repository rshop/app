<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Rshop\Frontend\View\Helper\HtmlHelper as FeHtmlHelper;

class HtmlHelper extends FeHtmlHelper
{
    public function lazyImage($path, array $options = [])
    {
        $options += [
            'class' => '',
            'src' => 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
            'data-src' => $path
        ];

        if (isset($options['retina'])) {
            $options['data-srcset'] =
                $path . ' 1x,' .
                $options['retina'] . ' 2x';

            unset($options['retina']);
        }

        $options['class'] .= ' c-img js-lazy';

        if (!isset($options['alt'])) {
            $options['alt'] = '';
        }

        return $this->tag('img', '', $options);
    }

    public function lazyThumbnail($rawPath, array $options = [])
    {
        $path = $this->thumbnailSrc($rawPath, ['format' => $options['format'] ?? 'default']);

        if ($options['retina']) {
            $pathRetina = $this->thumbnailSrc($rawPath, ['format' => $options['retina']]);
            $options['retina'] = $pathRetina;
        }

        unset(
            $options['format'],
            $options['fullBase']
        );

        return $this->lazyImage($path, $options);
    }

    public function lazyEntityThumbnail($entity, array $options = [])
    {
        $path = $this->entityThumbnailSrc($entity, ['format' => $options['format'] ?? 'default']);

        if ($options['retina']) {
            $pathRetina = $this->entityThumbnailSrc($entity, ['format' => $options['retina']]);
            $options['retina'] = $pathRetina;
        }

        unset(
            $options['format'],
            $options['fullBase']
        );

        return $this->lazyImage($path, $options);
    }

    /**
     * Script block.
     *
     * @param string script
     * @param array options
     * @param mixed $script
     * @param array $options
     *
     * @return void
     */
    public function scriptBlock($script, array $options = [])
    {
        $options = \array_merge(['block' => true, 'ready' => false], $options);

        if ($options['ready']) {
            $script = \sprintf('document.addEventListener("DOMContentLoaded", function() { %s });', $script);
        }
        unset($options['ready']);

        return parent::scriptBlock($script, $options);
    }

    public function preload($url, array $options = [])
    {
        $options += ['rel' => 'preload', 'block' => true];

        if (isset($options['as']) && $options['as'] == 'script') {
            $url = $this->Url->script($url);

            if (Configure::check('App.subdomainAssets') && !isset($options['fullBase']) && \strpos($url, '//') === false) {
                $url = \preg_replace('@^/?@', '' . \rtrim(Configure::read('App.subdomainAssets'), '/') . '/', $url);
            }
        }

        $out = $this->formatTemplate('metalink', [
            'attrs' => $this->templater()->formatAttributes($options, ['block']),
            'url' => $url
        ]);

        if (empty($options['block'])) {
            return $out;
        }

        if ($options['block'] === true) {
            $options['block'] = 'preload';
        }
        $this->_View->append($options['block'], $out);
    }

    public function link($title, $url = null, array $options = [])
    {
        if (!isset($options['title'])) {
            $options['title'] = \strip_tags($title);
        }

        $options['class'] = $options['class'] ?? 'c-link';

        if (isset($options['wrapText'])) {
            if (\is_array($options['wrapText'])) {
                $title = $this->tag($options['wrapText']['tag'], $title, $options['wrapText']['options']);
            } else {
                $title = $this->tag($options['wrapText'], $title);
            }
            $options['escape'] = false;
            unset($options['wrapText']);
        }

        if (isset($options['append']) || isset($options['prepend'])) {
            $options['escape'] = false;
        }

        if ($url !== null && \is_array($url) && isset($url['urlKey'])) {
            if (Configure::check('Url.' . $url['urlKey'])) {
                $url += Configure::read('Url.' . $url['urlKey']);
            }

            unset($url['urlKey'], $url[1]);
        }

        if (\is_array($url) && !\array_key_exists('plugin', $url)) {
            $url['plugin'] = 'Rshop/Frontend';
        }

        return parent::link($title, $url, $options);
    }

    public function ico($name, array $options = [])
    {
        $icoClass = 'ico ico-' . $name;
        $attrs = '';

        if (isset($options['class'])) {
            $icoClass .= ' ' . $options['class'];
        }

        if (isset($options['tooltip'])) {
            $icoClass .= ' js-tippy';
            $attrs .= 'data-tippy-content="' . $options['tooltip'] . '"';
        }

        return '<i class="' . $icoClass . '" ' . $attrs . ' aria-hidden="true"></i>';
    }
}
