<?php
namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Rshop\Frontend\View\Helper\CartHelper as BaseCartHelper;

class CartHelper extends BaseCartHelper
{
    public function getTotal($format = true, $select = '~', $type = 'total')
    {
        if (!$this->cart()) {
            return '-';
        }

        $method = 'getTotal';

        if ($type == 'subtotal') {
            $method = 'getSubtotal';
        }

        if ($type == 'rounding') {
            $method = 'getRoundingAmount';
        }

        if ($type == 'savings') {
            $method = 'getSavings';
        }

        $total = $this->cart()->{$method}($select)->asFloat();

        if (!$format) {
            return $total;
        }

        return $this->Html->dataComponent($this->Format->price($total), [
            'data-element' => 'cart',
            'data-type' => $type,
            'data-test-id' => 'cart' . \ucfirst($type)
        ]);
    }

    /**
     * Check if module is besteron.
     *
     * @param string $module
     *
     * @return bool
     */
    public function customIsBesteron($module)
    {
        return \strpos($module->module['class'], 'Besteron') !== false && $module->id !== 5;
    }

    public function customShippingsDates()
    {
        $products = $this->getItemsByType('product');

        if (!$products) {
            return false;
        }

        $pickupAvailability = [];
        $deliveryAvailability = null;

        foreach ($products as $product) {
            if (!$product->is_available) {
                return false;
            }

            $productPickup = $this->getView()->Product->getPickupBranches($product) ?: [];

            foreach ($productPickup as $branchId => $pickup) {
                $pickupAvailability[$branchId][] = $pickup;
            }

            $deliveryDays = Configure::read('Rshop.products.deliveryDays');

            if (\time() > \strtotime(Configure::read('Rshop.products.deliveryCutoff'))) {
                ++$deliveryDays;
            }

            $productDelivery = $this->addBusinessDays(Time::parse(), $deliveryDays);

            if ($deliveryAvailability === null || $deliveryAvailability < $productDelivery) {
                $deliveryAvailability = $productDelivery;
            }
        }

        return [
            'pickup' => $pickupAvailability,
            'delivery' => $deliveryAvailability
        ];
    }
}
