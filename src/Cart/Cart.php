<?php
/**
 * This file is part of rshop/svetnapojov project.
 *
 * (c) RIESENIA.com
 */

namespace App\Cart;

use App\Model\Entity\Product;
use Cake\Core\Configure;
use Litipk\BigNumbers\Decimal;
use Rshop\Admin\Cart\Cart as RshopCart;

class Cart extends RshopCart
{
    /**
     * Get savings for all items.
     *
     * @param string $filter
     *
     * @return Decimal
     */
    public function getSavings($filter = '~'): Decimal
    {
        $savings = Decimal::fromInteger(0);

        foreach ($this->getItems($filter) as $item) {
            if ($item instanceof Product) {
                $savings = $savings->add($item->getSavings()->mul(Decimal::fromInteger($item->getCartQuantity())));
            }
        }

        $discountsTotal = Decimal::fromInteger(0);

        if ($filter == '~') {
            // discounts
            $discountsTotal = $this->getTotal(Configure::read('Cart.DiscountSelector'));
        }

        return $savings->add($discountsTotal);
    }
}
