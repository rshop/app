import "Layouts/shopping";

import 'Ui/more-items';
import 'Ui/toggle';

import './summary';
import "Common/finstat";
import "Components/form/shopping-login";
import "Components/form/shopping-registration";
import 'Components/modal/modal-remote';
import 'Components/modal/modal-address';

import { initAllSmartForms } from 'Common/smartform';
import { customInitGoogleAutocomplete } from 'Common/google-places';

import './shopping-form-jquery';

const regFormTrigger = document.querySelector('.js-trigger-reg-form');
const sectionSwitcher = document.querySelector('.js-section-switcher');

if (regFormTrigger && sectionSwitcher) {
    regFormTrigger.addEventListener('click', () => {
        sectionSwitcher.dataset.section = 'registration';
        document.querySelector('.js-input-email').value = document.querySelector('.js-input-login').value;
        rEvent.dispatch("ShoppingSwitcher.switched");
    });
}

function checkSubmitBtn() {
    const btn = document.querySelector('.js-shopping-form-submit');

    if (document.querySelector('.js-section-switcher').dataset.section == 'registration') {
        btn.classList.remove('hidden');
    } else {
        btn.classList.add('hidden');
    }
}

document.addEventListener('DOMContentLoaded', function() {
    checkSubmitBtn();
    initAllSmartForms();

    const googleAutocompleteTriggers = document.querySelectorAll('.js-google-autocomplete-trigger');

    if (googleAutocompleteTriggers.length > 0) {
        for (const trigger of googleAutocompleteTriggers) {
            trigger.addEventListener('focus', () => {
                customInitGoogleAutocomplete(trigger);
            });
        }
    }
});

rEvent.on("ShoppingSwitcher.switched", () => {
    console.log('Switch switched');
    checkSubmitBtn();
});
