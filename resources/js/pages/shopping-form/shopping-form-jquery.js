$(function () {
    $('.js-inscription-input').on('keyup', function () {
        const characterCount = $(this).val().length,
            max = $(this).attr('maxlength'),
            difference = parseInt(max) - parseInt(characterCount);

        $('.js-inscription-counter span').text(difference);
    });
});