let updateObjednavkyTimeout = false;
let updateObjednavkyRequest = false;

window.updateObjednavkyForm = () => {
    const $form = $(".js-form-shopping");

    if (updateObjednavkyTimeout) {
        clearTimeout(updateObjednavkyTimeout);
    }

    if (updateObjednavkyRequest) {
        updateObjednavkyRequest.abort();
    }

    updateObjednavkyTimeout = setTimeout(() => {
        updateObjednavkyRequest = $.post($form.data('update-action'), $form.serialize(), function (data) {
            let recapTotals = Eshop.options.pricesWithVat ? data.totals.total : data.totals.subtotal

            Cart.updates.totals(data.totals)

            for (const type of ['present_packing']) {
                const naming = {
                    'present_packing': {
                        'inputName': 'custom_present',
                        'jsClass': 'packing',
                    }
                };

                const isSelected = $(`input[name="${naming[type].inputName}"]:checked`).length > 0
                const value = data.totals[type]
                const $holder = $(`.js-cart-recap-${naming[type].jsClass}-holder`)
                const $valueEl = $(`.js-cart-recap-${naming[type].jsClass}`)

                $holder.toggleClass('hidden', !isSelected)

                if (value) {
                    $valueEl.html(Format.price(value))
                }

                recapTotals -= value
            }

            /*
            pravdepodobne netreba
            recapTotals -= data.totals.discount

            $('.js-recap-totals').html(Format.price(recapTotals))
            */
        }, "json");

        updateObjednavkyTimeout = false;
    }, 200);
}

$(function () {
    $('[name="custom_present"]').on('change', function () {
        window.updateObjednavkyForm();
    });
});