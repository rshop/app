import Swiper, { Pagination, Navigation } from 'swiper';

import "Layouts/shopping";

import "Ui/tabs";
import "Ui/toggle";
import 'Ui/slider';
import "Components/form/vouchers";

import "./shopping-cart-jquery";

rEvent.on('Cart.product.after_add', function(response) {
    if (response.status) {
        window.location.reload();
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const updateQuantity = (event) => {
        const rSpinner = event.detail.rSpinner
        const quantity = rSpinner.getParsedValue()
        const oldValue = rSpinner.getParsedOldValue()
        const max = rSpinner.max
        const spinDirection = rSpinner.spinDirection

        if (quantity == oldValue && quantity == max && spinDirection == 'up' || quantity == max && spinDirection === null) {
            Eshop.tippyAlert(rSpinner.input, rSpinner.dataset.maxMessage.replaceAll(':productStock', rSpinner.max), {
                theme: 'warning'
            }, 20 * 1000);
        }

        // promotion spinners
        if (rSpinner.classList.contains('js-product-quantity-spinner')) {
            const promotionSpinners = rSpinner.closest('.c-cart-products__row').querySelectorAll('.js-promotion-quantity-spinner');

            if (promotionSpinners && quantity != oldValue) {
                promotionSpinners.forEach(promotionSpinner => {
                    promotionSpinner.querySelector('.c-form__input').setAttribute('max', quantity);
                });
            }
        }
    }
    for (const rSpinner of document.querySelectorAll('r-spinner')) {
        rSpinner.addEventListener('stop', updateQuantity)
        rSpinner.addEventListener('changed', updateQuantity)
    }
});

rEvent.on('Cart.after.updateQuantity', function(input, response) {
    if (response) {
        const parentProductId = response.item.id;
        const products = response.products;

        // update promotion spinners
        const matchingProducts = Object.values(products).filter(product => {
            return product.id.endsWith('COMBIDEAL-' + parentProductId);
        });

        matchingProducts.forEach(product => {
            const promotionProduct = document.querySelector(`.js-promotion-item[data-cart-id="${product.id}"]`);

            if (promotionProduct) {
                const currentQuantity = promotionProduct.querySelector('.js-promotion-quantity-spinner').input.value;
                const newQuantity = product.quantity;

                if (currentQuantity != newQuantity) {
                    promotionProduct.querySelector('.js-promotion-quantity-spinner').input.value = newQuantity;
                    $(promotionProduct.querySelector('.js-promotion-quantity-spinner').input).change();
                }
            }
        });

        // update product type totals
        const typeId = products[parentProductId].product_type_id;
        const triggerElem = document.querySelector(`.js-type-trigger[data-tab-id="tab-${typeId}"]`);

        if (triggerElem) {
            const productsAsArray = Object.entries(products);
            const filteredProductsArray = productsAsArray.filter(([key, value]) => value.product_type_id === typeId);
            const typeFinalPrice = filteredProductsArray.reduce((acc, [key, value]) => acc + (value.price_vat * value.quantity), 0);
            const typeFinalPriceFormatted = Format.price(typeFinalPrice);

            triggerElem.querySelector('.js-type-total').innerHTML = typeFinalPriceFormatted;
            // aby bolo jasne, moze nastat situacia ked darcek bude mat iny typ produktu ako skupina v ktorej sa nachadza jeho parent (produkt ku ktoremu je darcek)
            // vtedy to tu nebude fungovat, ale to je ok, lebo to je edge case
        }

    }
});

rEvent.on('Cart.totals.after_updated', function (data) {
    if (data.rounding) {
        $('[data-element="cart"]')
            .filter('[data-type="rounding"]')
            .html(Format.price(data.rounding));
    }

    if (data.savings) {
        $('[data-element="cart"]')
            .filter('[data-type="savings"]')
            .html(Format.price(data.savings));
    }
});