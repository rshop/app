const formElem = document.querySelector('.js-form-step-1');

if (formElem) {
    $(formElem).validate({
        submitHandler: function (form) {
            $(form).find('button[type="submit"]')
                .prop('disabled', true)
                .addClass('is-loading');

            form.submit();
        }
    });
}
