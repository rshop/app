function toggleDisabledShippingModuleInputs() {
    $('input[name="shipping_id"]').each(function() {
        const isChecked = $(this).is(':checked');
        for (const inputName of ['branch_id', 'shipping_module_id', 'shipping_name', 'shipping_address', 'shipping_post_code', 'shipping_city', 'shipping_phone']) {
            $(this)
                .closest('.js-modules-item')
                .find('input[name="' + inputName + '"]')
                .attr('disabled', !isChecked);
        }
    });
}

$(function () {
    toggleDisabledShippingModuleInputs();

    $('input[name="shipping_id"]').on('change', function () {
        toggleDisabledShippingModuleInputs();
    });

    $('input[name="branch_id"]').on('change', function () {
        if ($(this).val() && $('#error-branch').is(':visible')) {
            $('#error-branch').hide();
        }
    });

    $('input[name="shipping_id"]').on('change', function () {
        const $form = $(".js-form-cart-modules");

        $('input[name="shipping_method"]').val($(this).data('name'));

        var payments = $(this).data('payments');
        $('input[name="payment_id"]').prop('disabled', false);
        $('input[name="payment_id"]').filter(function (index) {
            return _.indexOf(payments, parseInt($(this).val())) == -1;
        }).prop('disabled', true).prop('checked', false).trigger('change');

        if ($(this).val() != $('[name="branch_id"]').parents('.shopping-modules__item').find('[name="shipping_id"]').val()) {
            $('[name="branch_id"]:checked').prop('checked', false).trigger('change');
        }

        if ($(this).is(':checked')) {
            if ($('#error-shipping').is(':visible')) {
                $('#error-shipping').hide();
            }
            if ($('#error-branch').is(':visible')) {
                $('#error-branch').hide();
            }
        }
    });

    $('input[name="payment_id"]').on('change', function () {
        if ($(this).is(':checked')) {
            $('input[name="payment_method"]').val($(this).data('name'));

            var isInsideFakeInput = $(this).closest('.js-fake-payment-input').length;

            if (!isInsideFakeInput) {
                $('.js-fake-payment-input').removeClass('is-checked')
            }

            if ($('#error-payment').is(':visible')) {
                $('#error-payment').hide();
            }
        }
    });

    $('input[name="shipping_id"]:checked').trigger('change');
});