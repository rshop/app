import { Vue, i18n } from "Common/vue"

import 'Components/zasielkovna';

document.addEventListener('DOMContentLoaded', () => {
    if (document.querySelector('#dpd-parcelshop-map-pickup-el')) {
        import('Components/vue/mapPickup/index.vue').then((module) => {
            window.$_dpdParcelshop = new Vue({
                i18n,
                render: h => h(
                    module.default,
                    {
                        props: {
                            branchesData: window.dpdParcelshopBranchesData,
                            $modalInstance: $('#dpd-parcelshop-pickup-modal'),
                            mapTrigger: document.querySelector('[r-dpd-parcelshop-trigger]'),
                            pickupInput: document.querySelector('input[r-dpd-parcelshop-input]'),
                            branchEmptyEl: document.querySelector('[r-dpd-parcelshop-empty]'),
                            branchSelectedEl: document.querySelector('[r-dpd-parcelshop-selected]'),
                            branchNameEl: document.querySelector('[r-dpd-parcelshop-selected-branch]'),
                            branchAddressEl: document.querySelector('[r-dpd-parcelshop-selected-address]'),
                            markerIcons: {
                                your_location: {
                                    url: '/img/markers/your_location.svg',
                                    size: [50, 50],
                                    anchor: { left: 25, top: 25 }
                                },
                                normal: {
                                    url: '/img/markers/dpd_marker.svg',
                                    size: [56, 68],
                                    anchor: { left: 28, top: 54 }
                                },
                                selected: {
                                    url: '/img/markers/dpd_marker_selected.svg',
                                    size: [56, 68],
                                    anchor: { left: 28, top: 54 }
                                }
                            }
                        }
                    }
                )
            }).$mount("#dpd-parcelshop-map-pickup-el");
        })
    }
});