let updateObjednavkyTimeout = false;
let updateObjednavkyRequest = false;

window.updateObjednavky = () => {
    const $form = $(".js-form-cart-modules");

    if (updateObjednavkyTimeout) {
        clearTimeout(updateObjednavkyTimeout);
    }

    if (updateObjednavkyRequest) {
        updateObjednavkyRequest.abort();
    }

    updateObjednavkyTimeout = setTimeout(() => {
        updateObjednavkyRequest = $.post($form.data('update-action'), $form.serialize(), function (data) {
            let recapTotals = Eshop.options.pricesWithVat ? data.totals.total : data.totals.subtotal

            Cart.updates.totals(data.totals)

            for (const type of ['payment', 'shipping', 'donation']) {
                const naming = {
                    'payment': {
                        'inputName': 'payment_id',
                        'jsClass': 'payment',
                    },
                    'shipping': {
                        'inputName': 'shipping_id',
                        'jsClass': 'shipping',
                    },
                    'donation': {
                        'inputName': 'donation',
                        'jsClass': 'donation',
                    },
                };

                let isSelected = false

                if (type == 'donation') {
                    isSelected = $(`[name="${naming[type].inputName}"]`).val() > 0
                } else {
                    isSelected = $(`input[name="${naming[type].inputName}"]:checked`).length > 0
                }

                const value = data.totals[type]
                const $holder = $(`.js-cart-recap-${naming[type].jsClass}-holder`)
                const $text = $(`.js-cart-recap-${naming[type].jsClass}-text`)
                const $valueEl = $(`.js-cart-recap-${naming[type].jsClass}`)

                $holder.toggleClass('hidden', !isSelected)

                if (isSelected && type != 'donation') {
                    const name = $(`input[name="${naming[type].inputName}"]:checked`).data('name').toString().trim()
                    $text.text(`(${name})`)
                }

                if (value) {
                    $valueEl.removeClass('is-green').html(Format.price(value))
                } else {
                    $valueEl.html($valueEl.data('free'))
                    $valueEl.addClass('is-green')
                }

                recapTotals -= value
            }

            recapTotals -= data.totals.discount

            $('.js-recap-totals').html(Format.price(recapTotals))
        }, "json");

        updateObjednavkyTimeout = false;
    }, 200);
}

$('.js-form-cart-modules').on('submit', function (event) {
    event.preventDefault();
}).validate({
    errorPlacement: function (error, element) {
        if (element.attr("type").match("checkbox")) {
            error.insertAfter(element.siblings("label"));
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
        let result = true;

        $('.js-shopping-modules-submit')
            .prop('disabled', true)
            .addClass('is-loading');

        if (!$('[name="payment_id"]').is(':checked')) {
            $('#error-payment').fadeIn(500);
            result = false;
        } else {
            if ($('#error-payment').is(':visible')) { $('#error-payment').fadeOut(500); }
        }

        if (!$('[name="shipping_id"]').is(':checked')) {
            $('#error-shipping').fadeIn(500);
            result = false;
        } else {
            if ($('#error-shipping').is(':visible')) { $('#error-shipping').fadeOut(500); }

            // branch pickup || parcelshop
            if (
                !$('[name="branch_id"]').is(':checked') && $('[name="branch_id"]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')
                || !$('[r-sps-parcelshop-input]').val() && $('[r-sps-parcelshop-input]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')
                || !$('[r-gls-parcelshop-input]').val() && $('[r-gls-parcelshop-input]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')
                || !$('[r-zasielkovna-input]').val() && $('[r-zasielkovna-input]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')
            ) {
                $('#error-branch').fadeIn(500);

                result = false;
            }
            else {
                if ($('#error-branch').is(':visible')) { $('#error-branch').fadeOut(500); }
            }
        }

        if (result) {
            $('.js-form-cart-modules-submit')
                .prop('disabled', true)
                .addClass('is-loading');

            if (updateObjednavkyRequest) {
                updateObjednavkyRequest.abort();
            }

            // unset branch_id if not branch is selected
            if (!$('[name="branch_id"]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')) {
                $('[name="branch_id"]').val('')
            }
            if (!$('[r-sps-parcelshop-input]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')) {
                $('[r-sps-parcelshop-input]').val('')
            }
            if (!$('[r-zasielkovna-input]').parents('.js-modules-item').find('[name="shipping_id"]').is(':checked')) {
                $('[r-zasielkovna-input]').val('')
                $('[r-zasielkovna-additional-input]').val('')
            }

            form.submit();
        }
        else {
            window.scroll({
                top: $('.c-msg--error:visible').first().offset().top - 64,
                behavior: 'smooth'
            })

            $('.js-shopping-modules-submit')
                .prop('disabled', false)
                .removeClass('is-loading');
        }
    }
});

$(function () {
    $('.js-form-cart-modules').find('input[type="radio"]').on('change', function () {
        const checkedShippingInputEl = document.querySelector('[name="shipping_id"]:checked')
        const checkedShippingModuleInputEl = checkedShippingInputEl && checkedShippingInputEl.closest('.js-modules-item').querySelector('[name="shipping_module_id"]')

        if (checkedShippingModuleInputEl && !checkedShippingModuleInputEl.value) {
            return
        }

        console.log('module changed');

        if ($('.js-select-wrapper:has(.multiselect-native-select)').length > 0) {
            $('[name="donation"]')
                .val('')
                .multiselect('refresh')
        }

        window.updateObjednavky();
    });

    $('[name="donation"]').on('change', function () {
        window.updateObjednavky();
    });
});