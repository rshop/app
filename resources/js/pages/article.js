import Swiper, { Pagination, Navigation } from 'swiper';
import { updateLazy } from '../functions/lazy';

import "../layouts/default";

import '../ui/slider';

import '../common/social';
// import '../common/lightgallery';

Swiper.use([Pagination, Navigation]);

document.addEventListener('DOMContentLoaded', function() {
    updateLazy();
});