import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const scrollTo = (el, offset) => {
    if (!el) return

    const headerHeight = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) || 0

    window.scroll({
        top: el.offsetTop - headerHeight - offset,
        behavior: 'smooth'
    })
}

const initAnchors = () => {
    document.querySelectorAll('[r-anchor]').forEach(anchor => {
        if (anchor.classList.contains('r-anchor-initialized')) return

        anchor.addEventListener('click', event => {
            event.preventDefault()
            scrollTo(document.getElementById(event.currentTarget.getAttribute('r-anchor')), 40)
        })

        anchor.classList.add('r-anchor-initialized')
    })
}

initAnchors()

document.addEventListener('DOMContentLoaded', () => {
    initAnchors()
})

export { initAnchors, scrollTo }