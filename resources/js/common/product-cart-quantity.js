const cartQuantity = (products) => {
    document.querySelectorAll('.js-thumb.is-in-cart').forEach(thumb => {
        thumb.classList.remove('is-in-cart')
    })

    if (!products) {
        return
    }

    for (const productId in products) {
        const product = products[productId]

        if (product.quantity == 0) {
            return
        }

        document.querySelectorAll(`.js-thumb[data-product-cart-id="${product.id}"]`).forEach(thumb => {
            thumb.classList.add('is-in-cart')
        })
    }
}

Event.on('Cart.product.after_add', function (response) {
    if (!response || !response.status) {
        return
    }

    cartQuantity(response.products)
})

Event.on('Cart.product.removed', function (data) {
    if (!data || !data.response || !data.response.status) {
        return
    }

    cartQuantity(data.response.products)
})

Event.on('Cart.fetchedProducts', function (data) {
    if (!data) {
        return
    }

    const response = data.response

    if (!response || !response.status || !response.data) {
        return
    }

    cartQuantity(response.data.items)
})