const initNative = () => {
    for (const shareButtons of document.querySelectorAll('.js-share-buttons')) {
        shareButtons.classList.add('hidden')
    }

    for (const nativeShareButtons of document.querySelectorAll('.js-share-buttons-native')) {
        nativeShareButtons.classList.remove('hidden')
    }

    for (const button of document.querySelectorAll('.js-share')) {
        button.addEventListener('click', (event) => {
            event.preventDefault()

            const data = event.currentTarget.dataset

            navigator.share({
                title: data.text,
                url: data.link
            }).then(() => {})
            .catch(console.error)
        })
    }
}

const initFallback = () => {
    for (const button of document.querySelectorAll('.js-share-facebook')) {
        button.addEventListener('click', (event) => {
            event.preventDefault()

            const data = event.currentTarget.dataset

            window.open('http://www.facebook.com/sharer.php?u=' + data.link, '_blank', "width=540,height=450,menubar=0")
        })
    }
}

if (navigator.share) {
    initNative()
} else {
    initFallback()
}


