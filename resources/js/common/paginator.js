Listing.customCallbacks.beforeFetch = () => {
    Event.dispatch('Listing.customBeforeFetch');
}

Event.on('Listing.customBeforeFetch', function () {
    if ($('.listing__holder').length) {
        $('.listing__holder').addClass('is-loading');
    }
    if ($('.c-msg.is-empty.listing').length) {
        $('.c-msg.is-empty.listing').addClass('is-loading');
    }
});

Event.on('Listing.fetchedData', function (data) {
    requestAnimationFrame(() => {
        if ($('.listing__holder').length) {
            $('.listing__holder').removeClass('is-loading');
        }

        if ($('.pager__load').length) {
            $('.pager__load').removeClass('is-loading');
        }
    })
})

$(function () {
    $(document).on('click', '.pager__list a', function (event) {
        event.preventDefault();

        $.get($(this).attr('href'), {}, function (data) {
            Listing.callbacks.update(data);
        }, 'json');

        $(this).addClass('is-loading');

        if ($('.listing__holder').length) {
            $('.listing__holder').addClass('is-loading');
        }
    });

    $(document).on('click', '.pager__load', function (event) {
        $(this).addClass('is-loading');
    });
});
