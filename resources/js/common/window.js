import { calculateViewportUnits, calculateHeaderSize } from '../functions/window';

let resizeTimer = false

calculateViewportUnits()
calculateHeaderSize()

window.addEventListener('resize', event => {
    clearTimeout(resizeTimer);

    resizeTimer = setTimeout(function () {
        calculateViewportUnits()
        calculateHeaderSize()

        window.Event.dispatch('Window.resized');
    }, 250);
});
