document.querySelectorAll('.js-toggle-password').forEach(passToggle => {
    passToggle.addEventListener('click', event => {
        const input = event.currentTarget.parentNode.querySelector('input')
        const type = input.getAttribute('type')

        input.setAttribute('type', ( type == 'text' ? 'password' : 'text' ))
    })
})