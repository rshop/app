function customInitGoogleAutocomplete(input) {
    var id = input.getAttribute("id");
    activeAutocompleteId = id;
    if (autocomplete[id] !== undefined) {
        return;
    }

    autocomplete[id] = new google.maps.places.Autocomplete(input, { types: ["address"] });

    var form = input.form;
    var countrySelect = form.elements[autocompleteInputs[id].inputs.country_id];
    var autocompleteCountries = [];

    if (countrySelect) {
        var countryId = countrySelect.value;
        autocompleteCountries.push(Eshop.options.countries[countryId].iso2code);
    }

    autocomplete[id].setComponentRestrictions({ "country": autocompleteCountries.length > 0 ? autocompleteCountries : ["SK", "CZ"] });
    autocomplete[id].setFields(["address_component"]);
    autocomplete[id].addListener("place_changed", fillInAddress);
}

export { customInitGoogleAutocomplete }
