import Swal from 'sweetalert2';
import tippy from 'tippy.js';

window.Eshop.confirm = function(text, options, callback, thenCallback = null) {
    var settings = $.extend({
        title: text,
        denyButtonText: options.cancelButtonText || 'Zrušiť',
        confirmButtonText: options.confirmButtonText || 'Ok',
        buttonsStyling: false,
        showDenyButton: true,
        reverseButtons: true,
        icon: 'question',
        customClass: {
            confirmButton: 'c-btn is-confirm',
            denyButton: 'c-btn c-btn--ghost is-deny'
        },
        preConfirm: () => {
            return callback()
        }
    }, options);

    Swal.fire(settings).then(thenCallback);
}

window.Eshop.tippyAlert = (el, message, options = {}, delay = 3500) => {
    if (el._tippy) {
        el._tippy.destroy()
    }

    if (!options.content) {
        let icon = 'tick'

        if (options.theme === 'error') {
            icon = 'close'
        }

        icon = `<i class="ico ico-${icon}"></i>`

        options.content = `${icon}<div class="is-holder">${message}</div>`
    }

    const hideTimeout = (instance) => {
        if (instance.hideTimeout) {
            clearTimeout(instance.hideTimeout)
        }

        instance.hideTimeout = setTimeout(() => {
            instance.hide()
        }, delay)
    }

    const offsetTop = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) + 20

    const instance = tippy(el, Object.assign({
        animation: 'shift-away',
        trigger: 'manual',
        theme: 'success',
        placement: 'top-end',
        allowHTML: true,
        interactive: true,
        zIndex: 99,
        appendTo: () => document.body,
        onShow(instance) {
            hideTimeout(instance)

            instance.popper.addEventListener('mouseenter', e => {
                clearTimeout(instance.hideTimeout)
            })
            instance.popper.addEventListener('mouseleave', e => {
                hideTimeout(instance)
            })
        },
        onHidden(instance) {
            instance.destroy()
        },
        popperOptions: {
            modifiers: [
                {
                    name: 'flip',
                    options: {
                        fallbackPlacements: ['top', 'top-start', 'bottom', 'bottom-start', 'bottom-end'],
                        padding: {
                            top: offsetTop
                        }
                    },
                },
            ],
        },
    }, options))

    requestAnimationFrame(() => {
        instance.show()
    })
}
