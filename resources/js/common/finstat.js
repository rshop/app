const assignHiddenFields = (autocompleteField, data) => {
    const form = autocompleteField.closest('form')
    const billing = form.querySelectorAll('.js-b2b-reg-billing')

    if (billing) {
        billing.forEach((billingElem) => {
            billingElem.classList.remove('is-hidden')

            if (Object.values(data.result || []).length) {
                for (const input of billingElem.querySelectorAll('input, select')) {
                    $(input).valid()
                }
            }
        });

    }
};

rEvent.on('Finstat.select.after', (autocompleteField, data) => {
    assignHiddenFields(autocompleteField, data)
});