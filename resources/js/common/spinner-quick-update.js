import client from 'FrontendJsAssets/common/client'
import { CancelToken } from 'axios'
import { tippyAlert } from 'Functions/tippy'

const lang = document.querySelector('html').getAttribute('lang')
let localeSwal
let localeThumb
window.customInCart = {}

import(`Locale/${lang}.json`).then((module) => {
    localeSwal = module.Swal
    localeThumb = module.Thumb
})

window.quickUpdateSpinner = {
    updatingIds: {},
    updatingTimeouts: {},
    cancelTokens: {},
    initializeSpinners() {
        requestAnimationFrame(() => {
            document.querySelectorAll('r-spinner.is-quick-update').forEach(rSpinner => {
                if (!rSpinner.classList.contains('is-initialized')) {
                    rSpinner.addEventListener('spin', this.spinCallback.bind(this))
                    rSpinner.addEventListener('stop', this.afterChangeCallback.bind(this))
                    rSpinner.addEventListener('changed', this.afterChangeCallback.bind(this))
                    rSpinner.classList.add('is-initialized')
                    rSpinner.formEl = rSpinner.parentNode.querySelector('form')
                    rSpinner.formButtonEl = rSpinner.formEl ? rSpinner.formEl.querySelector('.products__buy') : null
                    rSpinner.lastValidQuantity = parseFloat(rSpinner.value)

                    // ? -> https://riesenia.atlassian.net/browse/SVET-1459?focusedCommentId=95269
                    rSpinner.input.addEventListener('keyup', (event) => {
                        if (event.currentTarget.value) {
                            rSpinner.updateQuantity(rSpinner.value)
                        }
                    })
                }
            })
        })
    },
    cancelUpdating(cartId) {
        const quantityCancelToken = this.cancelTokens[cartId]

        if (quantityCancelToken) {
            quantityCancelToken.cancel()
        }

        this.cancelTokens[cartId] = CancelToken.source()
    },
    spinCallback(event) {
        const rSpinner = event.detail.rSpinner
        const cartId = rSpinner.dataset.cartId

        this.updateCartItem(cartId, rSpinner.getParsedValue(), true)

        if (rSpinner.getParsedValue() == rSpinner.max && rSpinner.getParsedSpinningOldValue() == rSpinner.getParsedValue()) {
            this.showMaxQuantityMessage(rSpinner)
        }

        if (!this.updatingIds[cartId]) {
            return
        }

        this.updatingIds[cartId] = true
        this.cancelUpdating(cartId)
        rEvent.dispatch('Cart.customCancelFetchCart')

        if (this.updatingTimeouts[cartId]) {
            clearTimeout(this.updatingTimeouts[cartId])
        }
    },
    afterChangeCallback(event) {
        const rSpinner = event.detail.rSpinner
        const spinDirection = rSpinner.spinDirection
        const cartId = rSpinner.dataset.cartId
        const quantity = rSpinner.getParsedValue()
        const oldValue = rSpinner.getParsedOldValue()
        const isFirstUpdate = !this.updatingIds[cartId]

        if (rSpinner.input._tippy && !rSpinner.input._tippy.props.forbidHide) {
            rSpinner.input._tippy.hide()
        }
        if (rSpinner.formButtonEl && rSpinner.formButtonEl._tippy) {
            rSpinner.formButtonEl._tippy.hide()
        }

        if (quantity == oldValue && quantity == rSpinner.max && spinDirection == 'up' || quantity == rSpinner.max && spinDirection === null) {
            this.showMaxQuantityMessage(rSpinner)
        } else if (quantity == oldValue && quantity == rSpinner.min && spinDirection == 'down') {
            this.confirmCartRemove(rSpinner)

            return
        }

        this.updateCartItem(cartId, quantity)
        this.toggleLoading(cartId, true)

        this.updatingIds[cartId] = true
        this.cancelUpdating(cartId)

        if (this.updatingTimeouts[cartId]) {
            clearTimeout(this.updatingTimeouts[cartId])
        }

        this.updatingTimeouts[cartId] = setTimeout(() => {
            this.postNewQuantity(cartId, quantity, rSpinner)

            delete this.updatingTimeouts[cartId]
        }, isFirstUpdate ? 0 : 650)
    },
    showMaxQuantityMessage(rSpinner) {
        if (!rSpinner.dataset.maxMessage) return

        const options = {
            theme: rSpinner.dataset.maxMessageTheme || 'warning',
            forbidHide: true,
            zIndex: '200'
        }

        if (rSpinner.input._tippy && rSpinner.input._tippy.props.forbidHide) {
            if (rSpinner.input._tippy.state.isShown || rSpinner.input._tippy.state.isVisible) {
                return
            }

            options.duration = [50, 250];
        }

        tippyAlert(rSpinner.input, rSpinner.dataset.maxMessage.replaceAll(':productStock', rSpinner.max), options, 10 * 1000);
    },
    confirmCartRemove(rSpinner) {
        window.Eshop.confirm(
            localeSwal.DeleteProduct.title,
            {
                denyButtonText: localeSwal.DeleteProduct.deny,
                confirmButtonText: localeSwal.DeleteProduct.confirm,
                customClass: {
                    confirmButton: 'c-btn c-btn--remove is-confirm',
                    denyButton: 'c-btn c-btn--primary-transparent is-deny'
                },
            },
            () => {
                const cartId = rSpinner.dataset.cartId

                document.querySelector('.swal2-container .swal2-confirm').classList.add('is-loading')

                delete this.updatingIds[cartId]
                this.cancelUpdating(cartId)
                this.toggleLoading(cartId, false)

                return client.post(`/remove-item/${cartId}`).then((response) => {
                    window.rEvent.dispatch('Cart.product.removed', response.data)
                    window.rEvent.dispatch('Cart.customFetchCart')

                    if (response.data.cart) {
                        $('.cart').replaceWith(response.data.cart);
                    }

                    this.updateCartItem(cartId, 0)

                    if (rSpinner.formButtonEl) {
                        tippyAlert(rSpinner.formButtonEl, localeThumb.productRemoved, {
                            theme: 'success',
                            zIndex: '200'
                        });
                    }
                })
            }
        )
    },
    postNewQuantity(cartId, quantity, rSpinner) {
        rEvent.dispatch('Cart.customCancelFetchCart')

        client
            .post(`/change-item-quantity/${cartId}`, {
                quantity: quantity
            }, {
                cancelToken: this.cancelTokens[cartId].token,
            })
            .then((response) => {
                const data = response.data

                if (!data.status) {
                    this.postNewQuantityCallback(cartId, rSpinner.lastValidQuantity || 0)

                    tippyAlert(rSpinner.input, data.message || localeThumb.quantityUpdated.error, {
                        theme: 'error',
                        zIndex: '200'
                    });

                    return
                }

                if (data.cart) {
                    $('.cart').replaceWith(data.cart);
                }

                rEvent.dispatch('Custom.Cart.updatedItem', { response: data });
                this.postNewQuantityCallback(cartId, quantity)
                rSpinner.lastValidQuantity = quantity

                if (!rSpinner.input._tippy || rSpinner.input._tippy && !rSpinner.input._tippy.props.forbidHide) {
                    tippyAlert(rSpinner.input, localeThumb.quantityUpdated.success, {
                        theme: 'success',
                        zIndex: '200'
                    });
                }
            })
            .catch((error) => {
                if (!client.isCancel(error)) {
                    this.postNewQuantityCallback(cartId, rSpinner.lastValidQuantity)

                    tippyAlert(rSpinner.input, localeThumb.quantityUpdated.error, {
                        theme: 'error',
                        zIndex: '200'
                    });
                }
            })
    },
    postNewQuantityCallback(cartId, quantity) {
        if (!this.updatingTimeouts[cartId]) {
            delete this.updatingIds[cartId]

            this.updateCartItem(cartId, quantity)
            this.toggleLoading(cartId, false)

            rEvent.dispatch('Cart.customFetchCart')
        }
    },
    toggleLoading(cartId, bool) {
        document.querySelectorAll(`r-spinner.is-quick-update[data-cart-id="${cartId}"]`).forEach(rSpinner => {
            rSpinner.classList.toggle('is-loading', bool)
        })
    },
    updateCartItem(cartId, quantity, isSpinCallback = false) {
        if (this.updatingIds[cartId] && !isSpinCallback) {
            return
        }

        document.querySelectorAll(`r-spinner.is-quick-update[data-cart-id="${cartId}"]`).forEach(rSpinner => {
            this.updateInCartClasses(rSpinner, quantity > 0)

            if (rSpinner.input !== document.activeElement) {
                rSpinner.value = quantity
            }
        })
    },
    afterFetchedCallback(items) {
        document.querySelectorAll(`r-spinner.is-quick-update`).forEach(rSpinner => {
            if (!this.updatingIds[rSpinner.dataset.cartId]) {
                this.updateInCartClasses(rSpinner, false)
            }
        })

        for (const cartId in items) {
            this.updateCartItem(cartId, items[cartId].quantity)
        }
    },
    updateInCartClasses(rSpinner, value) {
        rSpinner.classList.toggle('is-in-cart', value)

        if (rSpinner.formButtonEl) {
            rSpinner.formButtonEl.classList.toggle('is-in-cart', value)
        }

        // custom pre napoje
        const buyWrapper = rSpinner.closest('.js-buy-wrapper');

        if (buyWrapper) {
            buyWrapper.classList.toggle('is-in-cart', value)
        }
    }
}

window.initializeSpinners = () => {
    quickUpdateSpinner.initializeSpinners()
}

window.initializeSpinners()

rEvent.on('Listing.fetchedData', () => {
    window.initializeSpinners()
})

rEvent.on('Cart.product.before_add', function (form) {
    const button = form.querySelector('[type="submit"]')

    if (button.removeClassTimeout) {
        clearTimeout(button.removeClassTimeout)
    }

    button.classList.remove('is-loaded')
    button.classList.remove('has-error')
    button.classList.add('is-loading')
    button.disabled = true
})

rEvent.on('Cart.product.after_add', function (response) {
    if (!response || !response.status) {
        return
    }

    window.customInCart[response.item.id] = response.item.quantity
    quickUpdateSpinner.updateCartItem(response.item.id, response.item.quantity)
})

rEvent.on('Cart.addedProduct', function (data) {
    const { response } = data

    window.customInCart[response.item.id] = response.item.quantity
})

rEvent.on('Custom.Cart.addedItem', function (data) {
    const { form, response } = data

    const button = form.querySelector('[type="submit"]')

    if (!response.status) {
        button.classList.add('has-error')
    }

    button.classList.add('is-loaded')
    button.classList.remove('is-loading')
    button.disabled = false

    button.removeClassTimeout = setTimeout(() => {
        button.classList.remove('is-loaded')
        button.removeClassTimeout = false
    }, 1000)

    if (response.item) {
        window.customInCart[response.item.id] = response.item.quantity
    }
})

rEvent.on('Cart.product.removed', function (data) {
    console.log(data);
    if (!data || !data.status) {
        return
    }

    delete window.customInCart[data.item.id]
    quickUpdateSpinner.updateCartItem(data.item.id, 0)
})

rEvent.on('Cart.fetchedProducts', function (data) {
    if (!data) {
        return
    }

    const response = data.response

    if (!response || !response.status || !response.data) {
        return
    }

    window.customInCart = {}
    for (const cartId in response.data.items) {
        window.customInCart[cartId] = response.data.items[cartId].quantity
    }

    quickUpdateSpinner.afterFetchedCallback(response.data.items)
})

Cart.callbacks.addedItem = function (form, response) {
    if (!response.status) {
        tippyAlert(form, response.message, {
            theme: 'error',
            zIndex: '200'
        });
        Eshop.preloader.hide();

        Cart.customCallbacks.addedItem(form, response);

        return;
    }

    var $product = $('.cart__product[data-id="' + response.item.id + '"]');

    $('.cart').replaceWith(response.cart);

    if (response.upsell.active && !$('#up-sell').is(':visible')) {
        $.get(response.upsell.url, {
            quantity: response.item.added_quantity
        }, function (html) {
            $('#up-sell').remove();
            $('body').append(html);
            $('#up-sell').modal();
            Eshop.preloader.hide();
            rEvent.dispatch('Cart.upsell.show');
        }, 'html');
    } else {
        let elementToAlert = form;

        if (form.nextElementSibling && form.nextElementSibling.input) {
            elementToAlert = form.nextElementSibling.input
        }

        tippyAlert(elementToAlert, response.message, {
            theme: 'success',
            zIndex: '200'
        });
        Eshop.preloader.hide();
    }

    if (response.item) {
        Cart.updates.product($product, response.item);
    }

    if (response.totals) {
        Cart.updates.totals(response.totals);
    }

    if (response.free_shipping) {
        Cart.updates.freeShipping(response.free_shipping);
    }

    if (typeof RshopApp !== 'undefined' && RshopApp._isMounted) {
        store.dispatch('fetchCart');
    }

    Cart.storeItems();
    Cart.customCallbacks.addedItem(form, response);
}
