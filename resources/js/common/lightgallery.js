import { checkResolution } from '../functions/helpers'
import { disableScroll, enableScroll } from '../functions/scroll-lock'
import "lightgallery/src/js/lightgallery.js"
import "lg-video/src/lg-video.js"

document.addEventListener('DOMContentLoaded', () => {
    const $ = window.jQuery;

    $('.js-lightbox-holder').lightGallery({
        autoplay: false,
        autoplayControls: false,
        download: false,
        share: false,
        fullScreen: false,
        videoMaxWidth: "1140px",
        iframeMaxWidth: "100%",
        enableDrag: true,
        enableTouch: true,
        zoom: false,
        thumbnail: true,
        selector: '.js-lightbox',
        exThumbImage: 'data-thumb',
        galleryId: 1
    })

    $('.js-lightbox-holder').on('onBeforeSlide.lg',function(event, index, fromTouch, fromThumb){
        disableScroll()
    });

    $('.js-lightbox-holder').on('onCloseAfter.lg',function(event, index, fromTouch, fromThumb){
        enableScroll()
    });

    $('.js-lightbox-trigger').on('click', function(event) {
        event.preventDefault();

        if ($(this).data('query') && !checkResolution($(this).data('query'))) {
            return
        }

        if ($(this).data('video')) {
            $('.js-lightbox-holder').find('.js-lightbox.is-video').click();
        }
        else {
            $('.js-lightbox-holder').find('.js-lightbox:not(.is-video)').eq(this.dataset.index).click();
        }
    });
})
