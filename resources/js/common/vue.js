import Vue from 'vue'
import VueI18n from 'vue-i18n'

// vue config
const devMode = process.env.NODE_ENV === 'development' || window.Eshop.options.debug

Vue.config.devtools = devMode
Vue.config.debug = devMode
Vue.config.silent = !devMode
Vue.config.productionTip = devMode
Vue.config.ignoredElements = ['r-span', 'r-slider', 'r-tabs', 'r-toggle', 'r-shorten', 'r-more-items', 'r-favorite-trigger']

Vue.use(VueI18n)

export const i18n = new VueI18n({
    locale: document.querySelector('html').getAttribute('lang'),
    fallbackLocale: 'sk',
    messages: {
        sk: require('../locale/sk.json')
    }
})

export { Vue }