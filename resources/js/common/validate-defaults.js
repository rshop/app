$(function() {
    jQuery.validator.setDefaults({
        errorPlacement: function(error, element) {
            if (element.attr('type').match('checkbox')) {
                error.insertAfter(element.siblings('label'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    requestAnimationFrame(function() {
        $.validator.addMethod("email", function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || value == '';
        });
    });
});