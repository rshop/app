jQuery.validator.addMethod("smartformEmail", function(value, element, param) {
    const classes = element.classList;
    var instance = null;

    for (const cl of classes) {
        if (cl.includes('smartform-instance')) {
            instance = cl;
            break;
        }
    }

    const lastResult = window.smartform.getInstance(instance).emailControl.getLastValidationResult();

    const acceptableResults = ['EXISTS', 'UNKNOWN'];

    if (!lastResult) {
        return true;
    }

    return lastResult.upToDate && acceptableResults.includes(lastResult.result.resultType);
}, jQuery.validator.format("Please enter a valid email address."));

// smartform email validation callback
window.emailValidationCallback = function(response) {
    const emailField = $('.smartform-email:visible')
    const form = emailField.parents('form:visible')

    if (form) {
        form.data('validator').element(emailField)
    }
}

// smartform init
const initAllSmartForms = () =>  {
    const smartformTriggers = document.querySelectorAll('.js-smartform-trigger')

    if (smartformTriggers.length > 0) {
        initSmartForm('kVt2jvyFqR', function() {
            for (const trigger of smartformTriggers) {
                const type = trigger.dataset.type

                initSmartFormInput(
                    `smartform-instance-${type}`,
                    document.querySelector(`[data-type="${type}-country"]`).value,
                    {
                        address: document.querySelector(`[data-type="${type}-address"]`).attributes.name.value,
                        city: document.querySelector(`[data-type="${type}-city"]`).attributes.name.value,
                        postCode: document.querySelector(`[data-type="${type}-post-code"]`).attributes.name.value,
                    }
                )
            }
        });
    }
}

window.initAllSmartForms = initAllSmartForms;

export { initAllSmartForms }