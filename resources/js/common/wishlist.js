$(function() {
    $(document).on('click', function (event) {
        const $popup = $('.js-wishlist-popup');

        if ($popup.is(':visible') && !$(event.target).closest('.js-wishlist-popup').size() && !$(event.target).closest('.js-wishlist-trigger').size()) {
            $popup.hide();
        }
    });

    // wishlist okienko
    $('.js-wishlist-trigger').on('click', function (event) {
        var $popup = $('.js-wishlist-popup');

        event.preventDefault();

        $popup.toggle();
    });

    $('.js-wishlist-new-trigger').on('click', function (event) {
        var $this = $(this),
            $form = $('.js-wishlist-new');

        event.preventDefault();

        $this.hide();
        $form.slideDown(200);
        $form.find('.js-wishlist-popup-new-input').focus();
    });

    $('.js-wishlist-popup-list').on('change', '.js-wishlist-popup-checkbox', function () {
        var $this = $(this),
            wishlistId = $this.val(),
            addUrl = $this.data('add-url'),
            deleteUrl = $this.data('delete-url'),
            productId = $this.data('product-id'),
            isChecked = $this.is(':checked'),
            url = isChecked ? addUrl : deleteUrl;

        $this.prop('disabled', true);
        $this.addClass('is-loading');

        $.post(url, { product_id: productId, customer_wishlist_id: wishlistId }, function (data) {
            Eshop.alert(data.message, { type: data.status ? 'success' : 'error' });
            $this.prop('disabled', false);
            $this.removeClass('is-loading');
        }, "json");
    });

    $('.js-form-wishlist-create').validate({
        submitHandler: function (form) {
            $(form).find('button[type="submit"]').prop('disabled', true).addClass('is-loading');

            Form.ajaxPost(form, function (data) {
                if (data.status) {
                    var $popup = $('.js-wishlist-popup'),
                        $list = $('.js-wishlist-popup-list'),
                        $input = $(form).find('.js-wishlist-popup-new-input'),
                        $form = $('.js-wishlist-new'),
                        $link = $('.js-wishlist-new-trigger'),
                        wishlist_name = $input.val(),
                        $item = $list.find('.js-wishlist-popup-item').eq(0);

                    if ($item.hasClass('is-empty')) {
                        $item.removeClass('is-empty');
                    } else {
                        $item = $item.clone();
                    }

                    if ($popup.hasClass('is-empty')) {
                        $popup.slideDown(200);
                        $popup.removeClass('is-empty');
                    }

                    $link.show();
                    $form.slideUp(200);

                    $input.val('');
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('is-loading');

                    $item
                        .find('.js-wishlist-popup-checkbox')
                        .prop('disabled', false)
                        .prop('checked', true)
                        .attr('id', 'wish_' + data.wishlist_id)
                        .val(data.wishlist_id)
                        .end()
                        .find('label')
                        .html(wishlist_name)
                        .attr('for', 'wish_' + data.wishlist_id)
                        .end()
                        .prependTo($list.find('form').first());

                    $list.animate({
                        scrollTop: 0
                    }, 200);

                    $('.js-wishlist-badge')
                        .html(data.wishlists_count)
                        .toggleClass('is-active', data.wishlists_count > 0);
                }

                Eshop.alert(data.message, { type: data.status ? 'success' : 'error' });
            }, 'json');
        }
    });
});
