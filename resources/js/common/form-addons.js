// toggle password
document.querySelectorAll('.js-toggle-password').forEach(passToggle => {
    passToggle.addEventListener('click', event => {
        const input = event.currentTarget.closest('.c-form__item').querySelector('.c-form__input')
        const type = input.getAttribute('type')

        input.setAttribute('type', ( type == 'text' ? 'password' : 'text' ))
    })
})

// clear input
document.querySelectorAll('.js-clear-input').forEach(addon => {
    addon.addEventListener('click', event => {
        const input = event.currentTarget.closest('.c-form__item').querySelector('.c-form__input')

        input.value = ''
    })
})