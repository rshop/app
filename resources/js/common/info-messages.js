import { setCookie } from 'Functions/cookie'
import { calculateHeaderSize } from 'Functions/window'

const cookieMessage = document.querySelector('.js-cookie-message')
const infoMessage = document.querySelector('.js-info-message')

if (cookieMessage) {
    cookieMessage.classList.add('is-loaded')

    cookieMessage.querySelector('.js-cookie-message-close').addEventListener('click', event => {
        event.preventDefault()

        setCookie("cookies-allowed", 1, 365)

        cookieMessage.classList.remove('is-loaded')
    })
}

if (infoMessage) {
    infoMessage.querySelector('.js-info-message-close').addEventListener('click', event => {
        event.preventDefault()

        const cookieValue = infoMessage.dataset.cookieValue

        setCookie("msg-text", cookieValue, 1);

        infoMessage.style.display = 'none'

        requestAnimationFrame(calculateHeaderSize)
    })
}
