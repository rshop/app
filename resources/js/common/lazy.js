import LazyLoad from 'vanilla-lazyload';

window.aLazyLoad = new LazyLoad({
    threshold: 125,
    elements_selector: '.js-lazy',
    callback_loaded: (el) => {
        const animationlength = window.getComputedStyle(el, null).getPropertyValue('animation-duration') || 0

        setTimeout(() => {
            el.classList.remove('js-lazy')
        }, parseFloat(animationlength) * 1000 + 50)
    }
})

window.bgLazyLoad = new LazyLoad({
    threshold: 125,
    elements_selector: '.js-lazy-bg',
    callback_loaded: (el) => {
        const animationlength = window.getComputedStyle(el, null).getPropertyValue('animation-duration') || 0

        setTimeout(() => {
            el.classList.remove('js-lazy-bg')
        }, parseFloat(animationlength) * 1000 + 50)
    }
})

window.fakeLazyLoad = new LazyLoad({
    threshold: 125,
    elements_selector: '.js-lazy-fake',
    callback_enter: (el) => {
        el.classList.add('is-lazy-fake-loaded')
        el.classList.remove('js-lazy-fake')

        window.fakeLazyLoad.update()
    }
})

window.sliderLazyLoad = new LazyLoad({
    threshold: 0,
    elements_selector: '[r-slider-lazy]',
    callback_enter: (sliderEl) => {
        sliderEl.removeAttribute('r-slider-lazy')

        requestAnimationFrame(() => {
            if (typeof sliderEl.handleSlider() === 'function') {
                sliderEl.handleSlider()

                window.sliderLazyLoad.update()
            }
        })
    }
})
