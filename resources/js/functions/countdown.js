import Timer from 'easytimer.js'

export const initCountdowns = () => {
    const lang = document.querySelector('html').getAttribute('lang')

    import(`../locale/${lang}`).then(module => {
        const locale = module.default.Countdown

        const getSeconds = date => {
            const difference = (date - new Date) / 1000

            return { seconds: difference }
        }

        const getHtml = (value, label) => {
            return `<span class="is-value">${value}${label}</span><span class="is-divider">:</span>`
        }

        const getString = timer => {
            const values = timer.getTimeValues(),
                days = values.days > 0 ? getHtml(values.days, locale.days) : '',
                hours = values.days > 0 || values.hours > 0 ? getHtml(values.hours, locale.hours) : '',
                minutes = values.days > 0 || values.hours > 0 || values.minutes > 0 ? getHtml(values.minutes, locale.minutes) : '',
                seconds = values.days > 0 || values.hours > 0 || values.seconds > 0 ? getHtml(values.seconds.toString().padStart(2, '0'), locale.seconds) : ''

            return `${days}${hours}${minutes}${seconds}`
        }

        for (const countdown of document.querySelectorAll('.js-countdown')) {
            if (countdown.classList.contains('is-countdown-initialized')) continue

            const date = new Date(countdown.dataset.time),
                timer = new Timer({ countdown: true })

            timer.start({ startValues: getSeconds(date) })

            timer.addEventListener('secondsUpdated', function (e) {
                countdown.innerHTML = getString(timer)
            })

            timer.addEventListener('targetAchieved', function (e) {
                countdown.innerHTML = ''
            })

            countdown.classList.add('is-countdown-initialized')
        }
    })
}
