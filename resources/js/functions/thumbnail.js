export const thumbnail = (imageSrc, format) => {
    if (typeof window.Eshop.options.imageSrc === 'undefined') {
        console.error('window.Eshop.options.imageSrc is not defined');

        return imageSrc;
    }

    return imageSrc ? window.Eshop.options.imageSrc.replace('%25s', imageSrc).replace('%s', imageSrc).replace('%format', format) : window.Eshop.options.fullUrl + 'img/noimage.svg';
}
