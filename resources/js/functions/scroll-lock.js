// Our beloved iOS oveflow:hidedn fix
import { checkIphone, getScrollbarWidth } from './helpers'

const body = document.body;
const isIphone = checkIphone();
const scrollbarWidth = getScrollbarWidth()
let scrollPosition = 0;

const disableScroll = () => {
    requestAnimationFrame(() => {
        scrollPosition = window.pageYOffset

        body.classList.add('is-overlayed')

        if (document.body.scrollHeight > window.innerHeight) {
            body.style.paddingRight = `${scrollbarWidth}px`
        }

        if (isIphone) {
            body.style.position = 'fixed';
            body.style.top = `-${scrollPosition}px`;
            body.style.width = '100%';
        }
    })
}

const enableScroll = () => {
    if (isIphone) {
        body.style.removeProperty('position');
        body.style.removeProperty('top');
        body.style.removeProperty('width');
    }

    body.style.removeProperty('padding-right');
    body.classList.remove('is-overlayed')

    window.scrollTo(0, scrollPosition);
}

export { disableScroll, enableScroll };