const checkResolution = (query) => {
    let active = false;

    if (!query) {
        return true;
    }

    for (let value of query.split(";")) {
        if (document.querySelector(`.c-var .${value}-is-visible`).offsetParent != null) {
            active = true;
        }
    }
    return active;
}

const plural = (singular, plural24, plural, count) => {
    if (count > 1 && count < 5) {
        return plural24;
    }

    return (count == 1 ? singular : plural);
};

const checkIphone = () => {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform) || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

const getScrollbarWidth = () => {
    // Creating invisible container
    const outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    const inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);

    return scrollbarWidth;
}

const getCssVar = (name) => {
    return getComputedStyle(document.documentElement).getPropertyValue(name);
}

export { checkResolution, plural, checkIphone, getScrollbarWidth, getCssVar };
