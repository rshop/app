import { disableScroll, enableScroll } from './scroll-lock'

window.overlayActiveElement = null

// TODO: NTH pri zobrazeni a skryti transition pekny (podla vue, tam tie transition el)
function baseShow(activeElement, overlay, allowScroll) {
    window.overlayActiveElement = activeElement

    requestAnimationFrame(() => {
        overlay.classList.add('is-active')

        if (!allowScroll) {
            disableScroll()
        }
    })
}

function baseHide(activeElement, overlay) {
    if (activeElement !== window.overlayActiveElement) {
        return
    }

    overlay.classList.remove('is-active')

    if (document.body.classList.contains('is-overlayed')) {
        enableScroll()
    }

    window.overlayActiveElement = null
}

function showOverlay(activeElement, allowScroll = false) {
    baseShow(activeElement, document.querySelector('.js-overlay'), allowScroll)
}

function hideOverlay(activeElement) {
    baseHide(activeElement, document.querySelector('.js-overlay'))
}

function showHeaderOverlay(activeElement, allowScroll = false) {
    baseShow(activeElement, document.querySelector('.js-header-overlay'), allowScroll)
}

function hideHeaderOverlay(activeElement) {
    baseHide(activeElement, document.querySelector('.js-header-overlay'))
}

export { showOverlay, hideOverlay, showHeaderOverlay, hideHeaderOverlay }
