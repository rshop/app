const calculateViewportUnits = () => {
    document.documentElement.style.setProperty('--vh', `${window.innerHeight * 0.01}px`)
}

const calculateHeaderSize = () => {
    const header = document.querySelector('.l-header')

    if (!header) {
        return
    }

    document.documentElement.style.setProperty('--header-height', `${header.offsetHeight}px`)
}

const scrollToEl = (el, offset) => {
    if (!el) return

    const headerHeight = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) || 0

    window.scrollTo({
        top: el.offsetTop - headerHeight - offset,
        behavior: 'smooth'
    })
}

export { calculateHeaderSize, calculateViewportUnits, scrollToEl }
