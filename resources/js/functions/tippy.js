import tippy, {createSingleton} from 'tippy.js';

export const initTippy = (selector = '.js-tippy', options = {}) => {
    const offsetTop = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) + 20

    for (const el of [...document.querySelectorAll(selector)].filter(el => !el._tippy)) {
        tippy(el, {...{
            content: el.dataset.tippyContent,
            animation: 'shift-away',
            theme: 'general',
            placement: 'top',
            allowHTML: true,
            interactive: true,
            zIndex: 99,
            interactiveBorder: 24,
            appendTo: () => document.body,
            popperOptions: {
                modifiers: [
                    {
                        name: 'flip',
                        options: {
                            fallbackPlacements: ['bottom'],
                            padding: {
                                top: offsetTop,
                                right: 10,
                                bottom: 10,
                                left: 10
                            }
                        },
                    },
                ],
            },
        }, ...options})
    }

    for (const holder of [...document.querySelectorAll('.js-singleton-holder')].filter(el => !el.classList.contains('is-singleton-initialized'))) {
        const singletonInstances = []

        for (const el of holder.querySelectorAll(selector)) {
            if (el._tippy) {
                singletonInstances.push(el._tippy)
            }
        }

        if (singletonInstances.length) {
            createSingleton(singletonInstances, {
                delay: [200, 0],
                overrides: [
                    'content',
                    'animation',
                    'theme',
                    'placement',
                    'allowHTML',
                    'interactive',
                    'zIndex',
                    'interactiveBorder',
                    'interactiveDebounce',
                    'appendTo',
                    'popperOptions',
                ]
            })
        }

        holder.classList.add('is-singleton-initialized')
    }
}

export const tippyAlert = (el, message, options = {}, delay = 3500) => {
    if (el._tippy) {
        el._tippy.destroy()
    }

    if (!options.content) {
        let icon = null

        if (options.icon) {
            icon = options.icon

            delete options.icon
        }
        else if (!options.theme || options.theme === 'success') {
            icon = 'tick'
        }
        else if (options.theme === 'error') {
            icon = 'close'
        }

        options.content = `<div class="is-holder">${message}</div>`

        if (icon) {
            options.content = `<i class="ico ico-${icon}"></i>${options.content}`
        }
    }

    const hideTimeout = (instance) => {
        if (instance.hideTimeout) {
            clearTimeout(instance.hideTimeout)
        }

        instance.hideTimeout = setTimeout(() => {
            instance.hide()
        }, delay)
    }

    const offsetTop = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) + 20
    const instance = tippy(el, Object.assign({
        animation: 'shift-away',
        trigger: 'manual',
        theme: 'success',
        placement: 'top-end',
        allowHTML: true,
        interactive: true,
        zIndex: 99,
        appendTo: () => document.body,
        onShow(instance) {
            hideTimeout(instance)

            instance.popper.addEventListener('mouseenter', e => {
                clearTimeout(instance.hideTimeout)
            })
            instance.popper.addEventListener('mouseleave', e => {
                hideTimeout(instance)
            })
        },
        onHidden(instance) {
            instance.destroy()
        },
        popperOptions: {
            modifiers: [
                {
                    name: 'flip',
                    options: {
                        fallbackPlacements: ['top', 'top-start', 'bottom', 'bottom-start', 'bottom-end'],
                        padding: {
                            top: offsetTop
                        }
                    },
                },
            ],
        },
    }, options))

    requestAnimationFrame(() => {
        instance.show()
    })
}

export const initTippyDropdown = (options = {}) => {
    const offsetTop = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) + 20

    for (const el of document.querySelectorAll('.js-tippy-dropdown:not(.is-initialized)')) {
        const triggerEl = el.querySelector('.c-dropdown__btn')
        const dropdownEl = el.querySelector('.c-dropdown__list')

        dropdownEl.style.display = null

        tippy(triggerEl, {...{
            content: dropdownEl,
            animation: 'shift-away',
            theme: 'dropdown',
            placement: 'top-start',
            allowHTML: true,
            interactive: true,
            zIndex: 299,
            maxWidth: 600,
            interactiveBorder: 24,
            trigger: 'click',
            popperOptions: {
                modifiers: [
                    {
                        name: 'flip',
                        options: {
                            fallbackPlacements: ['top-end', 'bottom-start', 'bottom-end'],
                            //? Only if header is sticky
                            // padding: {
                            //     top: offsetTop,
                            //     right: 10,
                            //     bottom: 10,
                            //     left: 10
                            // }
                        },
                    },
                ],
            },
        }, ...options})

        el.classList.add('is-initialized')
    }
}