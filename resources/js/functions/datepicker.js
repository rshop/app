import flatpickr from "flatpickr";
import { Slovak } from "flatpickr/dist/l10n/sk"

export const initDatepickers = () => {
    const elements = [...document.querySelectorAll('.datepicker')].filter(el => !el.classList.contains('is-datepicker-initialized'))

    for (const el of elements) {
        el.setAttribute('autocomplete', 'off')

        flatpickr(el, {
            disableMobile: true,
            allowInput: true,
            locale: Slovak,
            dateFormat: 'd.m.Y'
        })

        el.classList.add('is-datepicker.initialized')
    }
}
