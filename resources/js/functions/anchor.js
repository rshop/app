import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const scrollToEl = (el, offset) => {
    if (!el) return

    // const headerHeight = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height')) || 0
    // hlavicka nie je prilepena
    const headerHeight = 0
    const elOffsetTop = el.getBoundingClientRect().top + document.documentElement.scrollTop

    window.scroll({
        top: elOffsetTop - headerHeight - offset,
        behavior: 'smooth'
    })
}

const initAnchors = () => {
    document.querySelectorAll('[r-anchor]').forEach((anchor) => {
        if (anchor.classList.contains('r-anchor-initialized')) return

        anchor.addEventListener('click', (event) => {
            event.preventDefault()
            scrollToEl(document.getElementById(event.currentTarget.getAttribute('r-anchor')), 40)
        })

        anchor.classList.add('r-anchor-initialized')
    })
}

export { initAnchors, scrollToEl }