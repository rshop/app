import { checkResolution } from 'Functions/helpers';

export const rSwiperVisibleItems = ({ swiper, on }) => {
    swiper.rVisibleItems = {
        update() {
            const slides = swiper.slides
            const itemsCount = swiper.params.slidesPerView

            for (const slide of slides) {
                slide.classList.remove('is-visible')

                for (const tabbable of slide.querySelectorAll('a, button')) {
                    tabbable.tabIndex = checkResolution('cxs;xs') ? 0 : -1
                }
            }

            if (!checkResolution('cxs;xs')) {
                const tabbableSlides = [...slides].reduce((accumulator, current) => {
                    if (current.classList.contains('swiper-slide-active') || (accumulator.length && accumulator.length < itemsCount)) {
                        accumulator.push(current)
                    }

                    return accumulator
                }, [])

                for (const slide of tabbableSlides) {
                    slide.classList.add('is-visible')

                    for (const tabbable of slide.querySelectorAll('a, button')) {
                        tabbable.tabIndex = 0
                    }
                }
            }
        }
    }

    on('init', swiper.rVisibleItems.update)
    on('slideChangeTransitionStart', swiper.rVisibleItems.update)
    on('slideChangeTransitionEnd', swiper.rVisibleItems.update)
    on('touchStart', swiper.rVisibleItems.update)
    on('touchEnd', swiper.rVisibleItems.update)
    on('breakpoint', swiper.rVisibleItems.update)
}