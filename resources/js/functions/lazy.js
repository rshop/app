import '../common/lazy';

export const updateLazy = () => {
    window.aLazyLoad.update()
    window.bgLazyLoad.update()
    window.sliderLazyLoad.update()
};
