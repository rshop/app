/**
 * This swiper plugin fixes navigation on css swiper slider on mobile
 * when "scroll-padding-left" css property is used to have padding from left edge
 */

export const rSwiperCssNavFix = ({ swiper, on }) => {
    swiper.rCssNavFix = {
        init() {
            if (!swiper.params.cssMode) return
            if (!swiper.params.navigation.prevEl) return

            swiper.navigation.destroy()

            swiper.params.navigation.prevEl.addEventListener('click', swiper.rCssNavFix.onPrevClick)
            swiper.params.navigation.nextEl.addEventListener('click', swiper.rCssNavFix.onNextClick)
        },
        onPrevClick() {
            swiper.slideTo(swiper.activeIndex - 1)
        },
        onNextClick() {
            swiper.slideTo(swiper.activeIndex + 1)
        }
    }

    on('init', swiper.rCssNavFix.init)

    //? not including on breakpoint event,
    //? because after resize whole swiper is reinitialized
    // on('breakpoint', swiper.rCssNavFix.init)
}