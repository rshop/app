class rShorten extends HTMLElement {
    constructor() {
        super()

        this.isExpanded = false

        this.handleTrigger = this.handleTrigger.bind(this)
    }

    connectedCallback() {
        requestAnimationFrame(() => {
            this.moreText = this.dataset.moreText
            this.lessText = this.dataset.lessText

            this.trigger = this.querySelector('[r-shorten-trigger]')
            this.triggerText = this.querySelector('[r-shorten-trigger-text]')
            this.shortText = this.querySelector('[r-shorten-short]')
            this.fullText = this.querySelector('[r-shorten-full]')

            if (this.trigger && this.shortText && this.fullText) {
                this.trigger.addEventListener('click', this.handleTrigger)
            }
        })
    }

    handleTrigger() {
        this.isExpanded = !this.isExpanded

        this.shortText.classList.toggle('hidden', this.isExpanded)
        this.fullText.classList.toggle('hidden', !this.isExpanded)
        this.trigger.classList.toggle('is-expanded', this.isExpanded)

        if (this.triggerText) {
            this.triggerText.innerHTML = ( this.isExpanded ? this.lessText : this.moreText )
        }
    }

    disconnectedCallback() {
        if (this.trigger && this.shortText && this.fullText) {
            this.trigger.removeEventListener('click', this.handleTrigger)
        }
    }
}

customElements.define('r-shorten', rShorten)

export default rShorten;