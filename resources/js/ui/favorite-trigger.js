import client from 'FrontendJsAssets/common/client'
import { tippyAlert } from 'Functions/tippy'

const lang = document.querySelector('html').getAttribute('lang')
let generalError

import(`Locale/${lang}`).then((module) => {
    generalError = module.GeneralError
})

class rFavoriteTrigger extends HTMLElement {
    get postUrl() {
        return !this.isFavorite ? this.addUrl : this.removeUrl
    }

    constructor() {
        super()

        this.isLoading = false
        this.isFavorite = false

        this.handleTrigger = this.handleTrigger.bind(this)
    }

    connectedCallback() {
        requestAnimationFrame(() => {
            this.addUrl = this.dataset.addUrl
            this.removeUrl = this.dataset.removeUrl
            this.countSelector = this.dataset.countSelector
            this.refreshListing = this.dataset.refreshListing

            this.addIcon = 'ico-heart'
            this.addText = this.dataset.addText
            this.addTextShort = this.dataset.addTextShort
            this.removeIcon = 'ico-heart-full'
            this.removeText = this.dataset.removeText
            this.removeTextShort = this.dataset.removeTextShort

            this.icon = this.querySelector('.ico')
            this.text = this.querySelector('.sc-text')
            this.textShort = this.querySelector('.sc-text-short')
            this.thumb = this.closest('.js-thumb')

            if (this.classList.contains('is-in-favorites')) {
                this.isFavorite = true
            }

            this.addEventListener('click', this.handleTrigger)
            this.addEventListener('keypress', this.handleTrigger)
        })
    }

    toggleAttributes() {
        this.classList.toggle('is-in-favorites', this.isFavorite)

        if (this.icon) {
            this.icon.classList.toggle(this.addIcon, !this.isFavorite)
            this.icon.classList.toggle(this.removeIcon, this.isFavorite)
        }

        if (this.text) {
            this.text.innerHTML = this.isFavorite ? this.removeText : this.addText
        }

        if (this.textShort) {
            this.textShort.innerHTML = this.isFavorite ? this.removeTextShort : this.addTextShort
        }

        if (this.thumb) {
            this.thumb.classList.toggle('is-in-favorites', this.isFavorite)
        }
    }

    handleLoading(bool) {
        this.isLoading = bool

        if (this.classList.contains('c-btn')) {
            this.classList.toggle('is-loading', bool)
        } else {
            this.icon.classList.toggle('is-loading', bool)
        }
    }

    handleAlert(message, type) {
        tippyAlert(this.classList.contains('c-btn') ? this : this.icon, message, {
            theme: type,
            placement: 'top-start'
        })
    }

    handleTrigger(event) {
        event.preventDefault()

        if (event.type == 'keypress' && ![13,32].includes(event.charCode)) return

        this.handlePost()
    }

    handlePost() {
        if (this.isLoading)
            return

        this.handleLoading(true)

        client.post(this.postUrl)
            .then((response) => {
                const data = response.data

                if (data.status) {
                    this.isFavorite = !this.isFavorite

                    for (const favoriteCount of document.querySelectorAll(this.countSelector)) {
                        favoriteCount.dataset.count = data.count
                        favoriteCount.innerHTML = data.count
                        favoriteCount.classList.toggle('is-empty', data.count <= 0)
                    }

                    if (this.refreshListing) {
                        this.refreshCurrentListingPage()
                    }
                } else {
                    if (data.errorType === 'not_present') {
                        if (this.refreshListing) {
                            this.refreshCurrentListingPage()
                        } else {
                            this.isFavorite = false
                        }
                    }
                }

                this.handleAlert(data.message, data.status ? 'success' : 'error')
            })
            .catch((error) => {
                this.handleAlert(generalError, 'error')
                console.error(error)
            })
            .then(() => {
                this.handleLoading(false)
                this.toggleAttributes()
            })
    }

    refreshCurrentListingPage(tryPrevious = false) {
        let urlToFetch = window.location.href

        if (tryPrevious) {
            const pagerPrevEl = document.querySelector('.pager__list .c-btn.is-prev')

            if (!pagerPrevEl) {
                return
            }

            urlToFetch = pagerPrevEl.getAttribute('href')
        }

        for (const listingHolder of document.querySelectorAll('.listing__holder')) {
            listingHolder.classList.add('is-loading')
        }

        client.get(urlToFetch)
            .then((response) => {
                window.Listing.callbacks.update(response.data)
            })
            .catch((error) => {
                if (!tryPrevious) {
                    this.refreshCurrentListingPage(true)
                }
            })
    }

    disconnectedCallback() {
        this.removeEventListener('click', this.handleTrigger)
        this.removeEventListener('keypress', this.handleTrigger)
    }
}

customElements.define('r-favorite-trigger', rFavoriteTrigger)

export default rFavoriteTrigger