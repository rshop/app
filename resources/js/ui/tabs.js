import { checkResolution } from '../functions/helpers'

/**
 * Nasledujuci kod ti dovoli spravit custom element <r-tabs> ... </r-tabs>
 *
 * Na element ktory sa ma spravat ako trigger,
 * pridaj atribut 'r-tab-trigger' spolu s data atributom
 * data-tab-id="category-420"
 *
 * Na element ktory je tab
 * pridaj atribut 'r-tab-target' spolu s atributom id="category-420" (unikatny)
 *
 * Po zmene tabu sa na trigger a target s danym id pridavaju classy is-active
 *
 * r-tab-open, r-tab-close na tento element sa neaplikuje is-active a nemusi byt b <r-tabs> elemente
 * na tomto elemente zaroven musi byt tiez data atribut
 * data-tab-id="category-420"
 *
 *  r-tab-close-all zatvori vsetky taby
 *
 * 'r-tab-trigger', 'r-tab-target' a 'r-tab-close-all' musi byt v <r-tabs> elemente
 */

class rTabs extends HTMLElement {
    constructor() {
        super()

        this.getTabId = this.getTabId.bind(this)
        this.getTarget = this.getTarget.bind(this)
        this.closeAllTabs = this.closeAllTabs.bind(this)
        this.handleTrigger = this.handleTrigger.bind(this)
        this.handleOpen = this.handleOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.openTab = this.openTab.bind(this)
        this.closeTab = this.closeTab.bind(this)
        this.activeTabs = []
        this.eventsAttached = false
    }

    connectedCallback() {
        requestAnimationFrame(() => {
            this.query = this.hasAttribute('r-tabs-query') ? this.getAttribute('r-tabs-query') : false
            this.multiTab = this.hasAttribute('multi-tab')
            this.allowTriggerClose = this.hasAttribute('allow-trigger-close')

            this.openTriggers = document.querySelectorAll('[r-tab-open]')
            this.closeTriggers = document.querySelectorAll('[r-tab-close]')
            this.closeAllTriggers = this.querySelectorAll('[r-tab-close-all]')
            this.triggers = this.querySelectorAll('[r-tab-trigger]')
            this.targets = this.querySelectorAll('[r-tab-target]')

            if (this.query) {
                window.Event.on('Window.resized', () => {
                    this.handleTabs()
                })
            }

            this.handleTabs()
        })
    }

    getTabId(tab) {
        return tab.dataset.tabId
    }

    getTriggers(tabId) {
        return this.querySelectorAll(`[r-tab-trigger][data-tab-id="${tabId}"]`)
    }

    getTarget(tabId) {
        return this.querySelector(`[id="${tabId}"]`)
    }

    preventHandler(event) {
        const el = event.currentTarget

        if (typeof el.dataset.preventQuery !== 'undefined' && checkResolution(el.dataset.preventQuery)) {
            event.preventDefault()
        }
    }

    handleTrigger(event) {
        if (!this.triggerEvent('rTab.handleTrigger')) {
            return
        }

        this.preventHandler(event)

        const tabId = this.getTabId(event.currentTarget)

        if (this.allowTriggerClose && this.activeTabs.includes(tabId)) {
            this.closeTab(tabId)
        } else {
            this.openTab(tabId)
        }
    }

    handleOpen(event) {
        if (!this.triggerEvent('rTab.handleOpen')) {
            return
        }

        this.preventHandler(event)

        const tabId = this.getTabId(event.currentTarget)

        this.openTab(tabId)
    }

    handleClose(event) {
        if (!this.triggerEvent('rTab.handleClose')) {
            return
        }

        this.preventHandler(event)

        const tabId = this.getTabId(event.currentTarget)

        this.closeTab(tabId)
    }

    closeAllTabs() {
        if (!this.triggerEvent('rTab.before.closeAllTabs')) {
            return
        }

        this.triggers.forEach(trigger => {
            trigger.classList.remove('is-active')
        })

        this.targets.forEach(target => {
            target.classList.remove('is-active')
        })

        this.activeTabs = []

        this.triggerEvent('rTab.after.closeAllTabs')
    }

    closeTab(tabId) {
        const target = this.getTarget(tabId)
        const triggers = this.getTriggers(tabId)

        if (!target || !this.triggerEvent('rTab.before.closeTab')) {
            return
        }

        triggers.forEach(trigger => trigger.classList.remove('is-active'))
        target.classList.remove('is-active')

        if (this.activeTabs.includes(tabId)) {
            this.activeTabs.splice(this.activeTabs.indexOf(tabId), 1)
        }

        this.triggerEvent('rTab.after.closeTab')
    }

    openTab(tabId) {
        const target = this.getTarget(tabId)
        const triggers = this.getTriggers(tabId)

        if (!target || !this.triggerEvent('rTab.before.openTab')) {
            return
        }

        if (!this.multiTab) {
            this.closeAllTabs()
        }

        triggers.forEach(trigger => trigger.classList.add('is-active'))
        target.classList.add('is-active')

        if (!this.activeTabs.includes(tabId)) {
            this.activeTabs.push(tabId)
        }

        this.triggerEvent('rTab.after.openTab')
    }

    triggerEvent(name) {
        if (!name) {
            return false
        }

        const event = new CustomEvent(name, {
            detail: this,
            cancelable: true
        })

        return this.dispatchEvent(event)
    }

    handleTabs() {
        if (!this.query) {
            this.attachEventHandlers()

            return
        }

        if (checkResolution(this.query)) {
            this.attachEventHandlers()
        } else {
            this.detachEventHandlers()
        }
    }

    attachEventHandlers() {
        if (this.eventsAttached) {
            return
        }

        this.triggers.forEach(trigger => {
            trigger.addEventListener('click', this.handleTrigger)
        })

        this.openTriggers.forEach(openTrigger => {
            openTrigger.addEventListener('click', this.handleOpen)
        })

        this.closeTriggers.forEach(closeTrigger => {
            closeTrigger.addEventListener('click', this.handleClose)
        })

        this.closeAllTriggers.forEach(closeAllTrigger => {
            closeAllTrigger.addEventListener('click', this.closeAllTabs)
        })

        this.targets.forEach(target => {
            if (target.classList.contains('is-active')) {
                this.activeTabs.push(target.id)
            }

            target.open = () => {
                this.openTab(target.id)
            }
        })

        this.eventsAttached = true
        this.classList.add('is-initialized')
    }

    detachEventHandlers() {
        this.triggers.forEach(trigger => {
            trigger.removeEventListener('click', this.handleTrigger)
        })

        this.openTriggers.forEach(openTrigger => {
            openTrigger.removeEventListener('click', this.handleOpen)
        })

        this.closeTriggers.forEach(closeTrigger => {
            closeTrigger.removeEventListener('click', this.handleClose)
        })

        this.closeAllTriggers.forEach(closeAllTrigger => {
            closeAllTrigger.removeEventListener('click', this.closeAllTabs)
        })

        this.eventsAttached = false
        this.classList.remove('is-initialized')
    }

    disconnectedCallback() {
        this.detachEventHandlers()
    }
}

customElements.define('r-tabs', rTabs)

export default rTabs;
