import { checkResolution } from 'Functions/helpers'
import Swiper from 'swiper';
import { sliderOptions } from 'Config/slider/slider-options';

class rSlider extends HTMLElement {
    get lazy() {
        return this.hasAttribute('r-slider-lazy')
    }

    get query() {
        return this.hasAttribute('r-slider-query') ? this.getAttribute('r-slider-query') : false
    }

    get paginationEl() {
        return this.querySelector('[r-slider-pagination]')
    }

    get pageCountEl() {
        return this.querySelector('[r-slider-count]')
    }

    get prevEl() {
        return this.querySelector('[r-slider-prev]')
    }

    get nextEl() {
        return this.querySelector('[r-slider-next]')
    }

    set customOptions(val) {
        if (!val) {
            val = ''
        }

        this.setAttribute('r-slider-custom-options', JSON.stringify(val));
    }

    get customOptions() {
        return this.hasAttribute('r-slider-custom-options') ? JSON.parse(this.getAttribute('r-slider-custom-options')) : {}
    }

    get options() {
        return sliderOptions(this)
    }

    constructor() {
        super()

        this.handleSlider = this.handleSlider.bind(this)
        this.buildSlider = this.buildSlider.bind(this)
        this.destroySlider = this.destroySlider.bind(this)
    }

    connectedCallback() {
        window.requestAnimationFrame(() => {
            window.Event.on('Window.resized', () => {
                this.destroySlider()

                window.requestAnimationFrame(() => {
                    this.handleSlider()
                })
            })

            if (!this.lazy) {
                window.requestAnimationFrame(() => {
                    this.handleSlider()
                })
            }
        })
    }

    handleSlider() {
        if (this.lazy) {
            return
        }

        if (!this.query) {
            this.buildSlider()

            return
        }

        if (checkResolution(this.query)) {
            this.buildSlider()
        } else {
            this.destroySlider()
        }
    }

    destroySlider() {
        if (!this.swiper) {
            return
        }

        this.swiper.destroy()
        this.swiper = false
    }

    buildSlider() {
        if (this.swiper) {
            return
        }

        new Swiper(this, this.options)
    }
}

customElements.define('r-slider', rSlider)
