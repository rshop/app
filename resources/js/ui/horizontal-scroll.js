import { checkResolution } from '../functions/helpers'
import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

class rHorizontalScroll extends HTMLElement {
    get selectedEl() {
        for (const item of this.querySelectorAll('.is-selected')) {
            if (item.offsetWidth === 0 && item.offsetHeight === 0) continue

            return item
        }

        return null
    }

    get query() {
        return this.getAttribute('h-scroll-query')
    }

    get containerPadding() {
        return parseInt(getComputedStyle(document.documentElement).getPropertyValue('--container-padding')) || 20
    }

    // max possible scroll distance from left edge of viewport
    get maxScroll() {
        return this.scrollWidth - this.offsetWidth
    }

    connectedCallback() {
        requestAnimationFrame(() => {
            if (!this.selectedEl) return
            if (this.query && !checkResolution(this.query)) return

            // check if selected element is out of container, even partially
            if (this.selectedEl.offsetLeft + this.selectedEl.offsetWidth + this.containerPadding > this.offsetWidth) {
                this.doScroll()
            }
        })
    }

    doScroll() {
        // find center position of element within scroll container
        let initialNavScroll = Math.round(this.selectedEl.offsetLeft - this.offsetWidth + (this.selectedEl.offsetWidth / 2) + (this.offsetWidth / 2))

        if (initialNavScroll > this.maxScroll) {
            initialNavScroll = this.maxScroll
        }

        this.scroll({
            left: initialNavScroll,
            behavior: 'smooth'
        })
    }
}

customElements.define('r-hscroll', rHorizontalScroll)

export default rHorizontalScroll;