import { showOverlay, hideOverlay, showHeaderOverlay, hideHeaderOverlay } from '../functions/overlay'
import { checkResolution } from '../functions/helpers'

class rToggle extends HTMLElement {
    constructor() {
        super()

        this.isActive = false

        this.handleTrigger = this.handleTrigger.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.handleOverlay = this.handleOverlay.bind(this)
        this.handleClickOutside = this.handleClickOutside.bind(this)
    }

    connectedCallback() {
        requestAnimationFrame(() => {
            this.showOverlay = this.hasAttribute('show-overlay')
            this.headerOverlay = this.getAttribute('show-overlay') == 'header'
            this.query = this.getAttribute('query')
            this.closeOnClickOutside = this.hasAttribute('close-outside')
            this.initialActiveQuery = this.getAttribute('initial-active-query')

            this.triggers = document.querySelectorAll(`[r-toggle-trigger="${this.id}"]`)
            this.closeTriggers = document.querySelectorAll(`[r-toggle-close="${this.id}"]`)

            for (const trigger of this.triggers) {
                trigger.classList.add('is-initialized')
                trigger.addEventListener(trigger.type === 'checkbox' ? 'change' : 'click', this.handleTrigger)
            }

            for (const trigger of this.closeTriggers) {
                trigger.classList.add('is-initialized')
                trigger.addEventListener(trigger.type === 'checkbox' ? 'change' : 'click', this.handleClose)
            }

            if (this.classList.contains('is-active')) {
                this.isActive = true
            }

            if (this.initialActiveQuery && checkResolution(this.initialActiveQuery)) {
                this.handleToggle(true)
            }

            this.classList.add('is-initialized')
        })
    }

    handleTrigger(event) {
        if (checkResolution(this.query)) {
            event.preventDefault()
        }

        this.handleToggle(!this.isActive, event)
    }

    handleClose(event) {
        event.preventDefault()

        this.handleToggle(false)
    }

    handleToggle(value, event = null) {
        if (!checkResolution(this.query)) {
            value = false
        }

        this.classList.toggle('is-active', value)

        for (const trigger of this.triggers) {
            trigger.classList.toggle('is-active', value)
        }

        this.handleOverlay(value)

        this.isActive = value

        if (this.closeOnClickOutside) {
            if (value) {
                document.addEventListener('click', this.handleClickOutside)
            } else {
                document.removeEventListener('click', this.handleClickOutside)
            }
        }

        this.triggerEvent(value ? 'rToggle.afterOpen' : 'rToggle.afterClose', event)
    }

    handleClickOutside(event) {
        const clickedEl = event.target

        if (this.contains(clickedEl) || this == clickedEl) return

        for (const trigger of [...this.triggers, ...this.closeTriggers]) {
            if (trigger.contains(clickedEl) || trigger == clickedEl) return
        }

        this.handleToggle(false)
    }

    handleOverlay(value) {
        if (!this.showOverlay) {
            return
        }

        if (value) {
            this.headerOverlay ? showHeaderOverlay(this.id) : showOverlay(this.id)
        }
        else {
            this.headerOverlay ? hideHeaderOverlay(this.id) : hideOverlay(this.id)
        }
    }

    triggerEvent(name, origEvent = null) {
        if (!name) {
            return false
        }

        const event = new CustomEvent(name, {
            detail: {...this, ...{origEvent: origEvent}},
            cancelable: true,
        })

        return this.dispatchEvent(event)
    }

    disconnectedCallback() {
        for (const trigger of this.triggers) {
            trigger.removeEventListener('click', this.handleTrigger)
        }

        for (const trigger of this.closeTriggers) {
            trigger.removeEventListener('click', this.handleClose)
        }
    }
}

customElements.define('r-toggle', rToggle)

export default rToggle
