export const paginationOptions = (instance) => ({
    pagination: {
        el: instance.paginationEl,
        type: 'bullets',
        clickable: true,
        dynamicBullets: true,
        dynamicMainBullets: 10,
        hideOnClick: false,
    }
})

export const navigationOptions = (instance) => ({
    navigation: {
        prevEl: instance.prevEl,
        nextEl: instance.nextEl,
        disabledClass: 'is-disabled',
        hiddenClass: 'is-hidden'
    }
})