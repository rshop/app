export const productsSliderOptions = (instance, options) => {
    // const hasCartLayout = instance.classList.contains('has-cart-layout')

    let responsiveItemCounts = {
        1280: 5,
        1024: 4,
    }

    // if (hasCartLayout) {
    //     // nothing
    // } else {
    //     responsiveItemCounts = {...responsiveItemCounts, ...{
    //     }}
    // }

    let breakpoints = {}

    for (const resolution in responsiveItemCounts) {
        const count = responsiveItemCounts[resolution]

        breakpoints[resolution] = {
            slidesPerView: count,
            slidesPerGroup: count,
            spaceBetween: 30,
            cssMode: false,
            pagination: {
                dynamicBullets: false,
            }
        }
    }

    return {
        loop: false,
        slidesPerView: 'auto',
        slidesPerGroup: 1,
        spaceBetween: 20,
        cssMode: true,
        breakpoints: breakpoints
    }
}