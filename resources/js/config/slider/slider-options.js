import { paginationOptions, navigationOptions } from "Config/slider/default"
import { productsSliderOptions } from "Config/slider/product"

import { rSwiperVisibleItems } from 'Functions/swiper-visible-items'
import { rSwiperCssNavFix } from 'Functions/swiper-css-nav-fix'
import { Pagination, Navigation } from 'swiper';

export const sliderOptions = instance => {
    // default options
    let options = {
        simulateTouch: false,
        watchOverflow: true,
        preventInteractionOnTransition: true,
        roundLengths: true,
        modules: [Pagination, Navigation, rSwiperVisibleItems, rSwiperCssNavFix]
    }

    if (instance.paginationEl) {
        options = {...options, ...paginationOptions(instance)}
    }

    if (instance.prevEl && instance.nextEl) {
        options = {...options, ...navigationOptions(instance)}
    }

    if (instance.hasAttribute('r-slider-products')) {
        options = {...options, ...productsSliderOptions(instance, options)}
    }

    return {...options, ...instance.customOptions}
}