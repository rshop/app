import { checkResolution } from '../functions/helpers';

let activeMenuAim = false;

function activateSubmenu(row) {
    const $navItem = $(row),
        id = $navItem.data('id');

    $('.js-navigation-target').removeClass('is-active');
    $('.js-navigation-target[data-id="' + id + '"]').addClass('is-active');

    $navItem.addClass('is-active');
    $('.js-nav-holder').addClass('has-active-submenu');
}

function deactivateSubmenu(row) {
    const $navItem = $(row),
        id = $navItem.data('id');

    $('.js-navigation-target[data-id="' + id + '"]').removeClass('is-active');

    $navItem.removeClass('is-active');
    $('.js-nav-holder').removeClass('has-active-submenu');
}

function toggleMenuAim() {
    if (checkResolution('cxs;xs')) {
        if (activeMenuAim) {
            $('.js-navigation').menuAim('destroy');

            activeMenuAim = false;
        }

        deactivateSubmenu('.js-navigation-trigger.is-active');
    }
    else {
        if (!activeMenuAim) {
            $('.js-navigation').menuAim({
                activate: activateSubmenu,
                deactivate: deactivateSubmenu,
                rowSelector: '.js-navigation-trigger'
            });

            activeMenuAim = true;
        }
    }
}

$(function(){
    toggleMenuAim();

    Event.on('Window.resized', () => {
        toggleMenuAim();
    });

    if (document.querySelector('.js-nav-holder.is-index')) {
        document.querySelector('.js-nav-holder.is-index').addEventListener('mouseleave', () => {
            document.querySelector('.js-nav-holder.is-index').classList.remove('has-active-submenu');
            deactivateSubmenu('.js-navigation-trigger.is-active');
        });
    }

    document.querySelectorAll('.js-navigation-trigger').forEach((trigger) => {
        trigger.addEventListener('click', (event) => {
            if (checkResolution('cxs;xs')) {
                event.preventDefault();

                activateSubmenu(trigger);
            }
        });
    });

    document.querySelectorAll('.js-navigation-back').forEach((trigger) => {
        trigger.addEventListener('click', (event) => {
            if (checkResolution('cxs;xs')) {
                event.preventDefault();

                deactivateSubmenu('.js-navigation-trigger.is-active');
            }
        });
    });
});
