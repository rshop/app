for (const formElem of document.querySelectorAll('.js-form-login')) {
    $(formElem).validate({
        rules: {
            login: {
                email: true,
            }
        },
        submitHandler: function (form) {
            $(form).find('button[type="submit"]').addClass('is-loading');
            $('.js-login-error').slideUp(100);

            Form.ajaxPost(form, function (data) {
                if (!data.status) {
                    $('.js-login-error')
                        .text(data.message)
                        .slideDown(250);
                } else {
                    $('.js-login-success').addClass('is-visible');

                    if (window.location.hash == '#login-modal') {
                        history.pushState('', document.title, window.location.pathname)
                    }

                    window.location.reload();
                }

                $(form).find('button[type="submit"]').removeClass('is-loading');
            }, 'json');
        }
    });
}
