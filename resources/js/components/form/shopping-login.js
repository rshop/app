const formElem = document.querySelector('.js-form-login');

if (formElem) {
    const loginField = formElem.querySelector('[name="login"]');

    $(formElem).validate({
        onkeyup: function(element, event) {
            if (element.name == 'login') {
                const delay = 750;

                $(element).parent().addClass('is-loading');
                clearTimeout($(element).data('keyUpTimeout'));

                $(element).data('keyUpTimeout', setTimeout(function() {
                    $(element).valid();
                    $(element).parent().removeClass('is-loading');
                }, delay));
            }
        },
        rules: {
            login: {
                email: true,
                remote: {
                    url: formElem.dataset.emailVerificationUrl,
                    type: "post",
                    dataType: "json",
                    data: {
                        model: 'Customers',
                        registration: '1',
                        email: function() {
                            return $(loginField).val()
                        },
                    },
                    dataFilter: function(data) {
                        const json = JSON.parse(data);

                        return !json.status;
                    },
                    complete: function(data) {
                        const response = data.responseJSON;

                        const sectionSwitcher = document.querySelector('.js-section-switcher');

                        if (!document.querySelector('.js-input-password').offsetParent) {
                            if (response) {
                                // email existuje, zobraz login
                                sectionSwitcher.dataset.section = 'login';
                            } else {
                                // zobraz reg
                                sectionSwitcher.dataset.section = 'registration';
                                document.querySelector('.js-input-email').value = document.querySelector('.js-input-login').value;
                            }

                            rEvent.dispatch("ShoppingSwitcher.switched");
                        }
                    }
                }
            },
            password: {
                minlength: 8
            }
        },
        submitHandler: function (form) {
            $(form).find('button[type="submit"]').addClass('is-loading');
            $('.js-login-error').slideUp(100);

            Form.ajaxPost(form, function (data) {
                if (!data.status) {
                    $('.js-login-error')
                        .text(data.message)
                        .slideDown(250);
                } else {
                    $('.js-login-success').addClass('is-visible');

                    if (window.location.hash == '#login-modal') {
                        history.pushState('', document.title, window.location.pathname)
                    }

                    window.location.reload();
                }

                $(form).find('button[type="submit"]').removeClass('is-loading');
            }, 'json');
        }
    });
}
