const formElem = document.querySelector('.js-vouchers-form');

if (formElem) {
    $(formElem).on('submit', e => e.preventDefault()).validate({
        submitHandler: function (form) {
            var $form = $(form);

            $form.find('input[name="voucher"]').blur();
            $form.find('button[type="submit"]').addClass('is-loading');

            Form.ajaxPost(form, function (data) {
                Eshop.alert(data.message, { type: data.status ? 'success' : 'error' });

                if (data.status) {
                    location.reload();
                } else {
                    $form.find('button[type="submit"]').removeClass('is-loading');
                }
            }, "json");
        }
    });
}