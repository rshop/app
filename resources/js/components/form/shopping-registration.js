import { addRules, addRulesByClass } from 'Components/form/validate-methods';


const formElem = document.querySelector('.js-form-shopping');

if (formElem) {
    document.addEventListener('DOMContentLoaded', function() {
        $(formElem).validate({
            submitHandler: function(form) {
                $('.js-shopping-form-submit')
                    .prop('disabled', true)
                    .addClass('is-loading');

                form.submit();
            }
        });

        addRules(formElem, ['companyInputs', 'addressInputs']);

        // custom just for address
        $(formElem).find('.c-form__item[data-type="address"] .c-form__input').each(function() {
            $(this).rules('add', 'addressNumber');
        });
    });
}