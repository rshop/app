import { generalRules, languageDependentRules, setRules } from './validate-rules';

export const addRules = (formElem, rules = []) => {
    const locale = formElem.dataset.locale ?? 'sk_SK';
    const expandedRules = [];

    rules.forEach(rule => {
        if (setRules[rule]) {
            expandedRules.push(...setRules[rule]);
        } else {
            expandedRules.push(rule);
        }
    });

    expandedRules.forEach(rule => {
        if (languageDependentRules[locale] && languageDependentRules[locale][rule]) {
            $(formElem).find(`[name*="${rule}"]`).each(function() {
                $(this).rules('add', languageDependentRules[locale][rule]);
            });
        }
        else if (generalRules[rule]) {
            $(formElem).find(`[name*="${rule}"]`).each(function() {
                $(this).rules('add', generalRules[rule]);
            });
        }
    });
}