export const generalRules = {
    'phone': {
        phone: true,
    },
    'post_code': {
        digits: true,
        exactlength: 5,
    },
    'password': {
        minlength: 8
    },
    'tax_num': {
        digits: true,
        minlength: 10,
        maxlength: 10,
    },
    'address': {
        addressNumber: true,
    }
}

export const languageDependentRules = {
    'sk_SK': {
        'org_num': {
            number: true,
            exactlength: 8,
        },
        'vat_num': {
            skVatNum: true,
        },
    },
    'en_US': {
        'post_code': {
            digits: true,
        }
    }
}

export const setRules = {
    'companyInputs': ['tax_num', 'org_num', 'vat_num'],
    'loginInputs': ['email', 'password'],
    'addressInputs': ['phone', 'post_code'],
}