export const getDistanceInMeters = (
    { latitude: lat1, longitude: lon1 },
    { latitude: lat2, longitude: lon2 }
) => {
    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI/180; // φ, λ in radians
    const φ2 = lat2 * Math.PI/180;
    const Δφ = (lat2-lat1) * Math.PI/180;
    const Δλ = (lon2-lon1) * Math.PI/180;

    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    const d = R * c; // in metres

    return d;
}

const lowerWithoutDiacriticsMap = {}
export const lowerWithoutDiacritics = (str) => {
    if (lowerWithoutDiacriticsMap[str]) {
        return lowerWithoutDiacriticsMap[str]
    }

    const result = str.toLowerCase().normalize("NFD").replace(/\p{Diacritic}/gu, "")

    lowerWithoutDiacriticsMap[str] = result

    return result
}

export const loadScript = (FILE_URL) => {
    return new Promise((resolve, reject) => {
        try {
            if (document.querySelector(`script[src="${FILE_URL}"]`)) {
                resolve({ status: true });
                return
            }

            const scriptEle = document.createElement("script");
            scriptEle.src = FILE_URL;

            scriptEle.addEventListener("load", (ev) => {
                resolve({ status: true });
            });

            scriptEle.addEventListener("error", (ev) => {
                reject({
                    status: false,
                    message: `Failed to load the script ${FILE_URL}`
                });
            });

            document.body.appendChild(scriptEle);
        } catch (error) {
            reject(error);
        }
    });
};