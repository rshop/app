import client from '../../../vendor/rshop/frontend/assets/js/common/client'

const rZasielkovna = {
    apiKey: null,
    openModal() {
        Packeta.Widget.pick(
            this.apiKey,
            (data) => {
                this.processSelectedBranch(data)
            },
            {
                // ISO 639-1 code
                'language': Eshop.options.locale ? Eshop.options.locale.split('_')[0] : 'sk',
                'country': Eshop.options.locale ? Eshop.options.locale.split('_')[1].toLowerCase() : 'sk',
            }
        )
    },
    processSelectedBranch(data) {
        if (!data) {
            return
        }

        document.querySelector('[r-zasielkovna-holder]').classList.add('is-loading')
        document.querySelector('[r-zasielkovna-empty]').classList.remove('hidden')
        document.querySelector('[r-zasielkovna-selected]').classList.add('hidden')
        document.querySelector('input[name="shipping_module_id"]').value = null

        client.post('/create-zasielkovna-branch', data)
            .then((response) => {
                const responseData = response.data

                if (!responseData.status) {
                    this.failedCallback()

                    return false
                }

                data.id = responseData.data.id

                this.successCallback(data)
            })
            .catch((error) => {
                console.error(error)
                this.failedCallback()
            })
    },
    failedCallback() {
        document.querySelector('input[name="shipping_module_id"]').value = null
        document.querySelector('[r-zasielkovna-holder]').classList.remove('is-loading')
    },
    successCallback(data) {
        document.querySelector('input[name="shipping_module_id"]').value = data.id
        document.querySelector('input[name="shipping_name"]').value = data.name
        document.querySelector('input[name="shipping_address"]').value = data.street
        document.querySelector('input[name="shipping_post_code"]').value = data.zip
        document.querySelector('input[name="shipping_city"]').value = data.city

        document.querySelector('[r-zasielkovna-selected]').classList.remove('hidden')
        document.querySelector('[r-zasielkovna-selected-branch]').innerText = data.place
        document.querySelector('[r-zasielkovna-selected-address]').innerText = data.name
        document.querySelector('[r-zasielkovna-empty]').classList.add('hidden')
        document.querySelector('[r-zasielkovna-holder]').classList.remove('is-loading')

        document.getElementById('error-zasielkovna').style.display = 'none'
    },
    init(apiKey) {
        if (!apiKey) {
            console.error('apiKey is not defined')

            return
        }

        this.apiKey = apiKey

        setTimeout(() => {
            for (const shippingInput of document.querySelectorAll('input[name="shipping_id"]')) {
                shippingInput.addEventListener('change', (event) => {
                    if (event.target.dataset.zasielkovna == 'true') {
                        if (!document.querySelector('[r-zasielkovna-empty]').classList.contains('hidden')) {
                            this.openModal()
                        }
                    }
                })
            }
        }, 100)

        for (const trigger of document.querySelectorAll('[r-zasielkovna-trigger]')) {
            trigger.addEventListener('click', (event) => {
                this.openModal()
            })
        }
    }
}

 // TODO: zasielkovna id
// rZasielkovna.init('1234567890')