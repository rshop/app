import { Vue, i18n } from 'Common/vue';

// import components
import Autocomplete from './../components/vue/Autocomplete.vue';
import Minicart from '../components/vue/Minicart.vue';

import 'Ui/toggle';
import './navigation';

window.$_autocomplete = new Vue({
    i18n,
    render: h => h(Autocomplete)
}).$mount("#autocomplete");

window.$_minicart = new Vue({
    i18n,
    render: h => h(Minicart)
}).$mount("#minicart");

document.querySelector('.js-trigger-search').addEventListener('click', event => {
    event.preventDefault();

    Event.dispatch("Autocomplete.showForm");
});
