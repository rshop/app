import client from 'FrontendJsAssets/common/client'
import { updateLazy } from 'Functions/lazy';
import { buildStars } from 'Functions/rating';
import LazyLoad from 'vanilla-lazyload';

const seenLazyLoad = new LazyLoad({
    threshold: 125,
    elements_selector: '.js-product-detail-seen',
    callback_enter: (seenHolder) => {
        const seenProducts = JSON.parse(localStorage.getItem('seenProducts'))
        let currentProductId = null

        if (document.querySelector('.js-product-detail')) {
            currentProductId = document.querySelector('.js-product-detail').dataset.productId
        }

        if (seenProducts.includes(currentProductId)) {
            seenProducts.splice(seenProducts.indexOf(currentProductId), 1)
        }

        if (!seenProducts.length) {
            return
        }

        seenHolder.classList.add('is-loading')
        seenHolder.classList.remove('js-product-detail-seen')
        seenLazyLoad.update()

        client.post('/custom-seen-products', {
            product_ids: seenProducts.splice(0, 3)
        })
        .then((response) => {
            seenHolder.innerHTML = response.data

            buildStars()
            updateLazy()
        })
        .finally(() => {
            seenHolder.classList.remove('is-loading')
        })
    }
})

if (document.querySelector('.js-product-detail')) {
    const productId = document.querySelector('.js-product-detail').dataset.productId
    let seenProducts = window.localStorage.getItem('seenProducts')

    try {
        seenProducts = JSON.parse(seenProducts)
    } catch {
        seenProducts = []
    }

    if (!Array.isArray(seenProducts)) {
        seenProducts = []
    }

    if (seenProducts.includes(productId)) {
        seenProducts.splice(seenProducts.indexOf(productId), 1)
    }

    seenProducts.unshift(productId)
    seenProducts = seenProducts.slice(0, 4)

    window.localStorage.setItem('seenProducts', JSON.stringify(seenProducts))
}