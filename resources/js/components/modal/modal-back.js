const $ = window.jQuery

$('.c-modal').on('show.bs.modal', function() {
    var modal = this
    var hash = modal.id
    window.location.hash = hash
    window.onhashchange = function() {
        if (!window.location.hash){
            $(modal).modal('hide')
        }
    }
})

$('.c-modal').on('hide.bs.modal', function() {
    history.pushState('', document.title, window.location.pathname)
})