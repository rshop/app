const $ = window.jQuery

window.checkNotificationType = function(elem) {
    const $price_input = $('.js-watchdog-price');

    let isCheaper = $(elem).val() == "become_cheaper";

    if (isCheaper) {
        $price_input.slideDown();
    } else {
        $price_input.slideUp();
    }
}

window.watchdogModalLoaded = function($modal) {
    if (!$modal.hasClass('in')) {
        return;
    }

    $modal.addClass('is-loaded');

    $('.js-form-modal-watchdog').validate({
        rules: {
            notification_price: {
                number: true
            }
        },
        submitHandler: function (form) {
            $modal.find('[type="submit"]').prop('disabled', true).addClass('is-loading');;
            $('.js-watchdog-error').slideUp(100);
            $('.js-watchdog-success').slideUp(100);

            Form.ajaxPost(form, function (data) {
                $modal.find('[type="submit"]').prop('disabled', false).removeClass('is-loading');

                if (!data.status) {
                    $('.js-watchdog-error')
                        .text(data.message)
                        .slideDown(200);
                } else {
                    $('.js-watchdog-success')
                        .text(data.message)
                        .slideDown(200);
                        $('.js-watchdog-hide').slideUp(200);
                }
            }, 'json');
        }
    });

    $('.js-watchdog-notification-type').each(function(){
        window.checkNotificationType($(this));
    });

    $('body').on('change', '.js-watchdog-notification-type', function(){
        window.checkNotificationType($(this));
    });
}