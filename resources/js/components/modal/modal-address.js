import { addRules } from 'Components/form/validate-methods';
import { initAllSmartForms } from 'Common/smartform';

function updateAddress(address, addressId) {
    const addressBox = addressId == 'shipping-select' ? document.querySelector(`.js-address-box[data-id^="shipping"]`) : document.querySelector(`.js-address-box[data-id="${addressId}"]`);
    const fields = [
        {selectorName: 'name', fieldName: 'name'},
        {selectorName: 'company', fieldName: 'company'},
        {selectorName: 'phone', fieldName: 'phone'},
        {selectorName: 'address', fieldName: 'address'},
        {selectorName: 'city', fieldName: 'city'},
        {selectorName: 'post_code', fieldName: 'post_code'},
        {selectorName: 'country', fieldName: 'country_name'},
        {selectorName: 'org_num', fieldName: 'org_num'},
        {selectorName: 'tax_num', fieldName: 'tax_num'},
        {selectorName: 'vat_num', fieldName: 'vat_num'},
    ];

    if (addressBox && address) {
        fields.forEach(field => {
            if (addressBox.querySelector(`[data-type="${field.selectorName}"]`)) {
                addressBox.querySelector(`[data-type="${field.selectorName}"]`).innerHTML = address[field.fieldName];
            }
        });

        if (addressBox.querySelector('[data-type="formatted-address"]')) {
            addressBox.querySelector('[data-type="formatted-address"]').innerHTML = `${address.address}<br>${address.post_code} ${address.city}<br>${address.country_name}`;
        }

        if (addressBox.querySelector('.js-address-box-company') && (address.org_num || address.tax_num || address.vat_num)) {
            addressBox.querySelector('.js-address-box-company').classList.remove('is-not-visible');
        } else if (addressBox.querySelector('.js-address-box-company')) {
            addressBox.querySelector('.js-address-box-company').classList.add('is-not-visible');
        }

        if (address.index) {
            const editElem = addressBox.querySelector('.js-address-box-edit');

            addressBox.dataset.id = `shipping-${address.index}`;

            if (editElem) {
                editElem.dataset.href = editElem.dataset.href.replace(/\/edit-shipping-address\/\d+/, '/edit-shipping-address/' + address.index);
                // because v jadre je jQuery 🤣 a nebere zmenu data attributu
                $(editElem).data('href', editElem.dataset.href)
            }
        }
    }

}

window.addressModalLoaded = function($modal) {
    if (!$modal.hasClass('in')) {
        return;
    }

    $modal.addClass('is-loaded');

    const formElem = $modal[0].querySelector('.js-form-address');

    if (formElem) {
        window.initAllSmartForms();

        $(formElem).validate({
            submitHandler: function(form) {
                $(formElem).find('button[type="submit"]')
                    .addClass('is-loading');

                $(formElem).find('.js-form-msg')
                    .removeClass('c-msg--error c-msg--success')
                    .slideUp(100);

                Form.ajaxPost(form, function (data) {
                    if (!data.status) {
                        $(formElem).find('.js-form-msg')
                            .addClass('c-msg--error')
                            .text(data.message)
                            .slideDown(250);
                    } else {
                        const addressId = $(formElem).find('.js-form-address-id').val();

                        Eshop.alert(data.message);

                        if (data.actionType == 'create') {
                            location.reload();
                        } else {
                            updateAddress(data.address, addressId);
                        }
                        $modal.modal('hide');
                    }

                    $(form).find('button[type="submit"]')
                        .removeClass('is-loading');
                }, 'json');
            }
        });

        addRules(formElem, ['companyInputs', 'addressInputs']);

        // custom just for address
        $(formElem).find('.c-form__item[data-type="address"] .c-form__input').each(function() {
            $(this).rules('add', 'addressNumber');
        });
    }

    const selectFormElem = $modal[0].querySelector('.js-form-address-select');

    if (selectFormElem) {
        const rules = {
            'address-option': {
                required: true
            }
        };

        $(selectFormElem).validate({
            errorPlacement: function(error, element) {
                error.insertAfter(element.parents('.c-address-select__list'));
            },
            rules: rules,
            submitHandler: function(form) {
                Form.ajaxPost(form, function (data) {
                    if (!data.status) {
                        $(formElem).find('.js-form-msg')
                            .addClass('c-msg--error')
                            .text(data.message)
                            .slideDown(250);
                    } else {
                        const addressId = 'shipping-select';

                        Eshop.alert(data.message);
                        updateAddress(data.address, addressId);
                        $modal.modal('hide');
                    }

                    $(form).find('button[type="submit"]')
                        .removeClass('is-loading');
                }, 'json');
            }
        });
    }
}