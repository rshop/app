const openModalFromHash = () => {
    if (!window.location.hash) {
        return
    }

    const modalEl = document.querySelector(window.location.hash)
    if (!modalEl) {
        return
    }

    if (!modalEl.classList.contains('modal')) {
        return
    }

    if (!modalEl.dataset.hasOwnProperty('openFromHash')) {
        return
    }

    $(modalEl).modal('show')
}

document.addEventListener('DOMContentLoaded', () => {
    setTimeout(() => {
        openModalFromHash()
    }, 0)
});
