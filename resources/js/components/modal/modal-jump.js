import { disableScroll, enableScroll } from '../../functions/scroll-lock'

const $ = window.jQuery

$('.c-modal').on('show.bs.modal', function() {
    disableScroll()
})

$('.c-modal').on('hidden.bs.modal', function() {
    enableScroll()
})
