/**
 * functions
 */
import { updateLazy } from 'Functions/lazy';
import { initTippy } from 'Functions/tippy';
import smoothscroll from 'smoothscroll-polyfill';

/**
 * common & components
 */
import 'Common/validate-defaults';
import 'Common/eshop';
import 'Common/window';
import 'Common/info-messages';
import 'Common/form-addons';
import 'Common/toggle-password';

import 'Common/product-cart-quantity';
import 'Common/spinner-quick-update';

import 'Components/modal/modal-back';
import 'Components/modal/modal-jump';
import 'Components/modal/modal-open-from-hash';

 /**
 * script
 */
window.Cart.customCallbacks.addedItem = function(form, response) {
    window.Event.dispatch('Custom.Cart.addedItem', {form, response})
}
window.Cart.customCallbacks.updatedItem = function(input, response) {
    window.Event.dispatch('Custom.Cart.updatedItem', {input, response})
}
window.Cart.customCallbacks.removedItem = function(form, response) {
    window.Event.dispatch('Custom.Cart.removedItem', {form, response})
}

window.updateLazy = updateLazy();

document.addEventListener('DOMContentLoaded', () => {
    updateLazy();
    initTippy()
});

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.js-back').forEach(back => {
        back.addEventListener('click', event => {
            if (event.currentTarget.getAttribute('href') == '#') {
                event.preventDefault();

                window.history.back()
            }
        })
    })
})

smoothscroll.polyfill();