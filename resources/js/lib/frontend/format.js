var Format = {};

Format.price = function(price) {
    var currency = Eshop.get('currency'),
        options = {
            decimals: 2,
            decimal_point: ',',
            thousands_separator: ' ',
            symbol_left: '',
            symbol_right: '€'
        };

    if (currency != undefined) {
        options = $.extend(options, currency);
    }

    var n = typeof price === 'string' || price instanceof String ? parseFloat(price.replace(' ', '').replace(',', '.')) : price,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(options.decimals)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;

    var formattedPrice = sign + options.symbol_left + (j ? i.substr(0, j) + options.thousands_separator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + options.thousands_separator) + (options.decimals ? options.decimal_point + Math.abs(n - i).toFixed(options.decimals).slice(2) : "") + " " + options.symbol_right;

    if (currency['decimals']) {
        var priceArray = formattedPrice.split(currency['decimal_point']);

        formattedPrice = `${priceArray[0]}<span class="is-small">${currency['decimal_point']}${priceArray[1]}</span>`;
    }

    return formattedPrice;
};

Format.integer = function(value) {
    return parseInt(value);
};

Format.plural = function(singular, plural24, plural, count) {
    if (count > 1 && count < 5) {
        return plural24;
    }

    return (count == 1 ? singular : plural);
};
