$(function() {
    $('.js-custom-form-modal-login').validate({
        rules: {
            login: {
                email: true
            }
        },
        submitHandler: function (form) {
            $(form).find('button[type="submit"]').addClass('is-loading');
            $('.js-login-error').slideUp(100);

            Form.ajaxPost(form, function (data) {
                if (!data.status) {
                    $('.js-login-error')
                        .text(data.message)
                        .slideDown(250);
                } else {
                    $('.js-login-success').addClass('is-visible');

                    if (window.location.hash == '#login-modal') {
                        history.pushState('', document.title, window.location.pathname)
                    }

                    window.location.reload();
                }

                $(form).find('button[type="submit"]').removeClass('is-loading');
            }, 'json');
        }
    });

    requestAnimationFrame(function() {
        $.validator.addMethod("email", function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) || value == '';
        });

        $('[name*="post_code"]').each(function() {
            $(this).closest('form').validate().settings.rules[this.name] = {
                digitsWithSpaces: true
            }
        });
    });
});