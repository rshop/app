var Eshop = {
    options: {
    }
};

Eshop.extend = function(options) {
    this.options = $.extend(this.options, options);
}

Eshop.get = function(property) {
    return Eshop.options[property];
}

// generate lightbox
Eshop.lightbox = function(img, options) {
    var options = $.extend({
        title: 'image',
        'type': 'image'
    }, options);
    $.fancybox('<img src="' + img + '" alt="' + options.title + '" />', options);
};

Eshop.removeItem = function(elem) {
    var $elem = $(elem),
        $data = $elem.data();

    if (!confirm($data.confirm)) {
        return;
    }

    $.post($data.url, {}, function(result) {
        if (result) {
            $($data.remove).remove();
        }
    });
}

Eshop.toggle = function(target) {
    var $elem = $(target);

    $elem.toggle();

    return false;
}

Eshop.preloader = {
    show: function() {},
    hide: function() {}
};

Eshop.alert = function (msg, options) {
    const headerHeight = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'))

    var settings = $.extend({
        allow_dismiss: true,
        type: 'success',
        animate: {
            enter: 'fadeInRight',
            exit: 'fadeOutRight'
        },
        placement: {
            from: window.innerWidth < 1024 ? 'bottom' : 'top'
        },
        gap: 2,
        delay: 4500,
        mouse_over: 'pause',
        offset: {
            y: 24 + (window.innerWidth > 1024 ? headerHeight : 76)
        },
        z_index: 3003,
        template:
            '<div data-notify="container" class="notify custom {0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">' +
            '<i class="ico ico-close" aria-hidden="true"></i>' +
            '</button>' +
            '<span class="message" data-notify="message">{2}</span>' +
            '</div>'
    }, options);

    $.notify(msg, settings);
}

Eshop.linkConfirm = function(elem, options) {
    var $elem = $(elem),
        url = $elem.attr('href'),
        settings = $.extend({}, options);

    Eshop.confirm($elem.attr('title'), settings, function() {
        window.location = url;
    });

    return false;
};

Eshop.image = function(src, format) {
    return this.options.imageSrc.replace('%25s', src).replace('%s', src).replace('%format', format);
}
