<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

use App\Application;

$Application = new Application(\dirname(__DIR__) . '/config');
$Application->bootstrap();
$Application->pluginBootstrap();
